package ng.exelon.frankinator2_0.logs;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoggingInterceptor extends HandlerInterceptorAdapter
{

    private static final Log LOG = LogFactory.getLog(LoggingInterceptor.class);


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
    {
        StringBuilder logMessage = new StringBuilder();
        logMessage.append("\nRequest Details ...").append("\n");
        logMessage.append("method: ").append(request.getMethod()).append("\n");
        logMessage.append("uri: ").append(request.getRequestURI()).append("\n");
        logMessage.append("status: ").append(response.getStatus()).append("\n");
        logMessage.append("remoteAddress: ").append(request.getRemoteAddr()).append("\n");

        if (ex != null)
        {
            LoggingInterceptor.LOG.error(logMessage.toString(), ex);
        }
        else
        {
            LoggingInterceptor.LOG.info(logMessage.toString());
        }

    }

}
