package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class StateRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<State> findAll(Integer page) {
		logger.warn("Getting all States from repository...");
		TypedQuery<State> customQuery = em.createNamedQuery("State.findAll", State.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<State> ss = customQuery.getResultList();
		for(State s: ss)
			this.logit(s);
		return ss;
	}
	
	public State findById(Long id) {
		State s = em.find(State.class, id);
		this.logit(s);
		return s;
	}

	public State save(State State) {

		if (State.getId() == null) {
			em.persist(State);
		} else {
			em.merge(State);
		}

		return State;
	}

	public void deleteById(Long id) {
		State state = findById(id);
		em.remove(state);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(State state) {
		if(state != null) {
			logger.info("state --> {}", state);
			logger.info("state country --> {}", state.getCountry());
		}
	}

}
