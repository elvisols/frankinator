package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Assessment;
import ng.exelon.frankinator2_0.model.Training;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TrainingRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Training> findAll(Integer page) {
		logger.warn("Getting all Trainings from repository...");
		TypedQuery<Training> customQuery = em.createNamedQuery("Training.findAll", Training.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<Training> ts = customQuery.getResultList();
		for(Training t: ts)
			this.logit(t);
		return ts;
	}
	
	public Training findById(Long id) {
		Training t = em.find(Training.class, id);
		this.logit(t);
		return t;
	}

	public Training save(Training Training) {

		if (Training.getId() == null) {
			em.persist(Training);
		} else {
			em.merge(Training);
		}

		return Training;
	}
	public void deleteById(Long id) {
		Training training = findById(id);
		em.remove(training);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(Training training) {
		if(training != null) {
			logger.info("Training --> {}", training);
			logger.info("Trainer --> " + training.getTrainer().getId());
			logger.info("Approved by --> " + training.getApprovedBy().getId());
			logger.info("Training group --> " + training.getAudience().getCode());
			logger.info("Training country --> " + training.getCountry().getCode());
			logger.info("Training course --> " + training.getCourse().getId());
			logger.info("Training language --> " + training.getLanguage().getCode());
			logger.info("Training partner --> " + training.getPartner().getId());
			logger.info("Training scheduledBy --> " + training.getScheduledBy().getId());
			logger.info("Training tier --> " + training.getTier().getCode());
		}
	}

}
