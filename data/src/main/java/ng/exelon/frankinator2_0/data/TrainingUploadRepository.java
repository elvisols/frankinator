package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.TrainingStatus;
import ng.exelon.frankinator2_0.model.TrainingUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TrainingUploadRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<TrainingUpload> findAll(Integer page) {
		logger.warn("Getting all TrainingUploads from repository...");
		TypedQuery<TrainingUpload> customQuery = em.createNamedQuery("TrainingUpload.findAll", TrainingUpload.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<TrainingUpload> tups = customQuery.getResultList();
		for(TrainingUpload t: tups)
			this.logit(t);
		return tups;
	}
	
	public TrainingUpload findById(Long id) {
		TrainingUpload tup = em.find(TrainingUpload.class, id);
		this.logit(tup);
		return tup;
	}

	public TrainingUpload save(TrainingUpload TrainingUpload) {

		if (TrainingUpload.getId() == null) {
			em.persist(TrainingUpload);
		} else {
			em.merge(TrainingUpload);
		}

		return TrainingUpload;
	}

	public void deleteById(Long id) {
		TrainingUpload trainingUpload = findById(id);
		em.remove(trainingUpload);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(TrainingUpload tup) {
		if(tup != null) {
			logger.info("tup --> {}", tup);
			logger.info("tup media --> {}", tup.getMedia());
			logger.info("tup partner --> {}", tup.getPartner().getId());
			logger.info("tup trainees --> {}", tup.getTrainees());
			logger.info("tup trainer --> {}", tup.getTrainer().getId());
		}
	}

}
