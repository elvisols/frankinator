package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.KpiCategory;
import ng.exelon.frankinator2_0.model.KpiItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class KpiItemRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<KpiItem> findAll(Integer page) {
		logger.warn("Getting all KpiItems from repository...");
		TypedQuery<KpiItem> customQuery = em.createNamedQuery("KpiItem.findAll", KpiItem.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<KpiItem> kis = customQuery.getResultList();
		for(KpiItem k: kis)
			this.logit(k);
		return kis;
	}
	
	public KpiItem findById(Long id) {
		KpiItem ki = em.find(KpiItem.class, id);
		this.logit(ki);
		return ki;
	}

	public KpiItem save(KpiItem KpiItem) {

		if (KpiItem.getId() == null) {
			em.persist(KpiItem);
		} else {
			em.merge(KpiItem);
		}

		return KpiItem;
	}

	public void deleteById(Long id) {
		KpiItem kpiItem = findById(id);
		em.remove(kpiItem);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(KpiItem kpiItem) {
		if(kpiItem != null) {
			logger.info("kpiItem --> {}", kpiItem);
			logger.info("kpi id --> {}", kpiItem.getKpi().getId());
		}
	}

}
