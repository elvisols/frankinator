package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Assessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class AssessorRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Assessor> findAll(Integer page) {
		logger.warn("Getting all Assessors from repository...");
		TypedQuery<Assessor> customQuery = em.createNamedQuery("Assessor.findAll", Assessor.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
//		Query queryTotal = em.createQuery("Select count(u.id) from Assessor u"); 
//		long countResult = (long)queryTotal.getSingleResult(); // Getting total count
//		int pageSize = 10;
//		int pageNumber = (int) ((countResult / pageSize) + 1); // Calculating the last page
		return customQuery.getResultList();
	}
	
	public Assessor findById(Long id) {
		return em.find(Assessor.class, id);
	}

	public Assessor save(Assessor Assessor) {

		if (Assessor.getId() == null) {
			em.persist(Assessor);
		} else {
			em.merge(Assessor);
		}

		return Assessor;
	}

	public void deleteById(Long id) {
		Assessor Assessor = findById(id);
		em.remove(Assessor);
	}

}
