package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Target;
import ng.exelon.frankinator2_0.model.Trainee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TraineeRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Trainee> findAll(Integer page) {
		logger.warn("Getting all Trainees from repository...");
		TypedQuery<Trainee> customQuery = em.createNamedQuery("Trainee.findAll", Trainee.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<Trainee> ts = customQuery.getResultList();
		for(Trainee t: ts)
			this.logit(t);
		return ts;
	}
	
	public Trainee findById(Long id) {
		Trainee t = em.find(Trainee.class, id);
		this.logit(t);
		return t;
	}

	public Trainee save(Trainee Trainee) {

		if (Trainee.getId() == null) {
			em.persist(Trainee);
		} else {
			em.merge(Trainee);
		}

		return Trainee;
	}

	public void deleteById(Long id) {
		Trainee trainee = findById(id);
		em.remove(trainee);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(Trainee trainee) {
		if(trainee != null) {
			logger.info("trainee --> {}", trainee);
			logger.info("trainee training --> {}", trainee.getTraining().getId());
		}
	}

}
