package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.TrainingStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TrainingStatusRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<TrainingStatus> findAll(Integer page) {
		logger.warn("Getting all TrainingStatuss from repository...");
		TypedQuery<TrainingStatus> customQuery = em.createNamedQuery("TrainingStatus.findAll", TrainingStatus.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<TrainingStatus> tss = customQuery.getResultList();
		for(TrainingStatus t: tss)
			this.logit(t);
		return tss;
		
	}
	
	public TrainingStatus findById(Long id) {
		TrainingStatus ts = em.find(TrainingStatus.class, id);
		this.logit(ts);
		return ts;
	}

	public TrainingStatus save(TrainingStatus TrainingStatus) {

		if (TrainingStatus.getId() == null) {
			em.persist(TrainingStatus);
		} else {
			em.merge(TrainingStatus);
		}

		return TrainingStatus;
	}

	public void deleteById(Long id) {
		TrainingStatus trainingStatus = findById(id);
		em.remove(trainingStatus);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(TrainingStatus ts) {
		if(ts != null) {
			logger.info("ts --> {}", ts);
			logger.info("ts status type --> {}", ts.getStatusType());
			logger.info("ts training --> {}", ts.getTraining().getId());
			logger.info("ts user --> {}", ts.getUser().getId());
		}
	}

}
