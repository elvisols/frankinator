package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.KpiItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class KpiRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Kpi> findAll(Integer page) {
		logger.warn("Getting all Kpis from repository...");
		TypedQuery<Kpi> customQuery = em.createNamedQuery("Kpi.findAll", Kpi.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<Kpi> ks = customQuery.getResultList();
		for(Kpi k: ks)
			this.logit(k);
		return ks;
	}
	
	public Kpi findById(Long id) {
		Kpi k = em.find(Kpi.class, id);
		this.logit(k);
		return k;
	}

	public Kpi save(Kpi Kpi) {

		if (Kpi.getId() == null) {
			em.persist(Kpi);
		} else {
			em.merge(Kpi);
		}

		return Kpi;
	}

	public void deleteById(Long id) {
		Kpi kpi = findById(id);
		em.remove(kpi);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(Kpi kpi) {
		if(kpi != null) {
			logger.info("kpi --> {}", kpi);
			logger.info("kpi category id --> {}", kpi.getCategory().getId());
			logger.info("kpi input type --> {}", kpi.getInputType());
		}
	}

}
