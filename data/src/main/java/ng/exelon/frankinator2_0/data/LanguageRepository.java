package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class LanguageRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Language> findAll(Integer page) {
		logger.warn("Getting all Languages from repository...");
		TypedQuery<Language> customQuery = em.createNamedQuery("Language.findAll", Language.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		return customQuery.getResultList();
	}
	
	public Language findById(String id) {
		return em.find(Language.class, id);
	}

	public Language save(Language Language) {

		if (Language.getCode() == null) {
			em.persist(Language);
		} else {
			em.merge(Language);
		}

		return Language;
	}

	public void deleteById(String id) {
		Language language = findById(id);
		em.remove(language);
	}

}
