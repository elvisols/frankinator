package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.KpiCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class KpiCategoryRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<KpiCategory> findAll(Integer page) {
		logger.warn("Getting all KpiCategorys from repository...");
		TypedQuery<KpiCategory> customQuery = em.createNamedQuery("KpiCategory.findAll", KpiCategory.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<KpiCategory> kc = customQuery.getResultList();
		for(KpiCategory k: kc)
			this.logit(k);
		return kc;
	}
	
	public KpiCategory findById(String id) {
		KpiCategory kc = em.find(KpiCategory.class, id);
		this.logit(kc);
		return kc;
	}

	public KpiCategory save(KpiCategory KpiCategory) {

		if (KpiCategory.getId() == null) {
			em.persist(KpiCategory);
		} else {
			em.merge(KpiCategory);
		}

		return KpiCategory;
	}

	public void deleteById(String id) {
		KpiCategory kpiCategory = findById(id);
		em.remove(kpiCategory);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(KpiCategory kpiCategory) {
		if(kpiCategory != null) {
			logger.info("kpiCategory --> {}", kpiCategory);
			logger.info("kpiCategory Kpis --> {}", kpiCategory.getKpis());
		}
	}

}
