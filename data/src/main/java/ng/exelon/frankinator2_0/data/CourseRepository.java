package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.CourseTierDeleted;
import ng.exelon.frankinator2_0.model.value.RoleEnum;
import ng.exelon.frankinator2_0.model.value.TierEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class CourseRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EntityManager em;

	public List<Course> findAll() {
		logger.warn("Getting all Courses from repository...");
		TypedQuery<Course> customQuery = em.createNamedQuery("Course.findAll", Course.class);
		return customQuery.getResultList();
	}
	
	public List<Course> findCoursesByTier(TierEnum te) {
		logger.warn("Getting all Courses by tier from repository...");
		TypedQuery<Course> customQuery = em.createQuery("select u from Course u", Course.class);
		return customQuery.getResultList();
	}
	
	public Course findById(Long id) {
		return em.find(Course.class, id);
	}

	public Course save(Course Course) {

		if (Course.getId() == null) {
			em.persist(Course);
		} else {
			em.merge(Course);
		}

		return Course;
	}

	public void deleteById(Long id) {
		Course course = findById(id);
		em.remove(course);
	}
	
}
