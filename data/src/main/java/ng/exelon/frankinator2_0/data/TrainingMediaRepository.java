package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.TrainingMedia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TrainingMediaRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<TrainingMedia> findAll(Integer page) {
		logger.warn("Getting all TrainingMedias from repository...");
		TypedQuery<TrainingMedia> customQuery = em.createNamedQuery("TrainingMedia.findAll", TrainingMedia.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<TrainingMedia> tms = customQuery.getResultList();
		for(TrainingMedia t: tms)
			this.logit(t);
		return tms;
	}
	
	public TrainingMedia findById(Long id) {
		TrainingMedia tm = em.find(TrainingMedia.class, id);
		this.logit(tm);
		return tm;
	}

	public TrainingMedia save(TrainingMedia TrainingMedia) {

		if (TrainingMedia.getId() == null) {
			em.persist(TrainingMedia);
		} else {
			em.merge(TrainingMedia);
		}

		return TrainingMedia;
	}

	public void deleteById(Long id) {
		TrainingMedia trainingMedia = findById(id);
		em.remove(trainingMedia);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(TrainingMedia tm) {
		if(tm != null) {
			logger.info("tm --> {}", tm);
			logger.info("tm training --> {}", tm.getTraining().getId());
			logger.info("tm media type --> {}", tm.getMediaType());
		}
	}

}
