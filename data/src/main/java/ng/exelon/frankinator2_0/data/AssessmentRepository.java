package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Assessment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class AssessmentRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Assessment> findAll(Integer page) {
		logger.warn("Getting all Assessments from repository...");
		TypedQuery<Assessment> customQuery = em.createNamedQuery("Assessment.findAll", Assessment.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<Assessment> assessments = customQuery.getResultList();
		// Fetch relation in a single transaction
		for(Assessment a: assessments)
			this.logit(a);
//			this.logit(assessments.isEmpty() ? null : assessments.get(0));
		return assessments;
	}
	
	public Assessment findById(Long id) {
		Assessment assessment = em.find(Assessment.class, id);
		// Fetch relation in a single transaction
		this.logit(assessment);
		return assessment;
	}

	public Assessment save(Assessment Assessment) {

		if (Assessment.getId() == null) {
			em.persist(Assessment);
		} else {
			em.merge(Assessment);
		}

		return Assessment;
	}

	public void deleteById(Long id) {
		Assessment assessment = findById(id);
		em.remove(assessment);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(Assessment assessment) {
		if(assessment != null) {
			logger.info("Assessment --> {}", assessment);
			logger.info("Assessor --> {}", assessment.getAssessor().getId());
			logger.info("Trainer --> " + assessment.getTrainer().getId());
			logger.info("Training --> " + assessment.getTraining().getId());
			logger.info("CreatedBy --> " + assessment.getCreatedBy().getId());
//			logger.info("CreatedBy Roles --> " + assessment.getCreatedBy().getRoles());
			logger.info("Kpis --> " + assessment.getKpis());
		}
	}

}
