package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Target;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TraineeGroupRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<TraineeGroup> findAll(Integer page) {
		logger.warn("Getting all TraineeGroups from repository...");
		TypedQuery<TraineeGroup> customQuery = em.createNamedQuery("TraineeGroup.findAll", TraineeGroup.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		return customQuery.getResultList();
	}
	
	public TraineeGroup findById(String id) {
		return em.find(TraineeGroup.class, id);
	}

	public TraineeGroup save(TraineeGroup TraineeGroup) {

		if (TraineeGroup.getCode() == null) {
			em.persist(TraineeGroup);
		} else {
			em.merge(TraineeGroup);
		}

		return TraineeGroup;
	}

	public void deleteById(String id) {
		TraineeGroup traineeGroup = findById(id);
		em.remove(traineeGroup);
	}
	

}
