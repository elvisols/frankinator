package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.State;
import ng.exelon.frankinator2_0.model.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TargetRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Target> findAll(Integer page) {
		logger.warn("Getting all Targets from repository...");
		TypedQuery<Target> customQuery = em.createNamedQuery("Target.findAll", Target.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<Target> ts = customQuery.getResultList();
		for(Target t: ts)
			this.logit(t);
		return ts;
	}
	
	public Target findById(Long id) {
		Target t = em.find(Target.class, id);
		this.logit(t);
		return t;
	}

	public Target save(Target Target) {

		if (Target.getId() == null) {
			em.persist(Target);
		} else {
			em.merge(Target);
		}

		return Target;
	}

	public void deleteById(Long id) {
		Target target = findById(id);
		em.remove(target);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(Target target) {
		if(target != null) {
			logger.info("target --> {}", target);
			logger.info("target country --> {}", target.getCountry().getCode());
			logger.info("target course --> {}", target.getCourse().getId());
			logger.info("target group --> {}", target.getGroup().getCode());
			logger.info("target partner --> {}", target.getPartner().getId());
			logger.info("target quarter --> {}", target.getQuarter());
			logger.info("target tier --> {}", target.getTier().getCode());
		}
	}

}
