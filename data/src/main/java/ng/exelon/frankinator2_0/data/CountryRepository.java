package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class CountryRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Country> findAll(Integer page) {
		logger.warn("Getting all Countries from repository...");
		TypedQuery<Country> customQuery = em.createNamedQuery("Country.findAll", Country.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		return customQuery.getResultList();
	}
	
	public Country findById(String id) {
		return em.find(Country.class, id);
	}

	public Country save(Country Country) {

		if (Country.getCode() == null) {
			em.persist(Country);
		} else {
			em.merge(Country);
		}

		return Country;
	}

	public void deleteById(String id) {
		Country country = findById(id);
		em.remove(country);
	}

}
