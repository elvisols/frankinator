package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Assessment;
import ng.exelon.frankinator2_0.model.User;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class UserRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<User> findAll(Integer page) {
		logger.warn("Getting all users from repository...");
		TypedQuery<User> customQuery = em.createNamedQuery("User.findAll", User.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
//		Query queryTotal = em.createQuery("Select count(u.id) from User u"); 
//		long countResult = (long)queryTotal.getSingleResult(); // Getting total count
//		int pageSize = 10;
//		int pageNumber = (int) ((countResult / pageSize) + 1); // Calculating the last page
		List<User> us = customQuery.getResultList();
		for(User u: us)
			this.logit(u);
		return us;
	}
	
	public List<User> findRoleUsers(RoleEnum role) {
		logger.warn("Getting all users from repository...");
		TypedQuery<User> customQuery = em.createQuery("select u from User u", User.class);
		return customQuery.getResultList();
	}
	
	public User findById(Long id) {
		User u = em.find(User.class, id);
		this.logit(u);
		return u;
	}

	public User save(User user) {

		if (user.getId() == null) {
//			em.persist(user.getRoles().get(0));
			em.persist(user);
		} else {
			em.merge(user);
		}

		return user;
	}

	public void deleteById(Long id) {
		User user = findById(id);
		em.remove(user);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(User user) {
		if(user != null) {
			logger.info("User --> {}", user);
			logger.info("User roles --> {}", user.getRoles());
		}
	}

}
