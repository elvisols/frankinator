package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.PartnerEmployee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PartnerEmployeeRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<PartnerEmployee> findAll(Integer page) {
		logger.warn("Getting all PartnerEmployees from repository...");
		TypedQuery<PartnerEmployee> customQuery = em.createNamedQuery("PartnerEmployee.findAll", PartnerEmployee.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<PartnerEmployee> pes = customQuery.getResultList();
		for(PartnerEmployee p: pes)
			this.logit(p);
		return pes;
	}
	
	public PartnerEmployee findById(Long id) {
		PartnerEmployee pe = em.find(PartnerEmployee.class, id);
		this.logit(pe);
		return pe;
	}

	public PartnerEmployee save(PartnerEmployee PartnerEmployee) {

		if (PartnerEmployee.getId() == null) {
			em.persist(PartnerEmployee);
		} else {
			em.merge(PartnerEmployee);
		}

		return PartnerEmployee;
	}

	public void deleteById(Long id) {
		PartnerEmployee partnerEmployee = findById(id);
		em.remove(partnerEmployee);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(PartnerEmployee pe) {
		if(pe != null) {
			logger.info("pe --> {}", pe);
			logger.info("pe partner id --> {}", pe.getPartner().getId());
		}
	}

}
