package ng.exelon.frankinator2_0.data.dbconfig;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
//@ImportResource("classpath:jpa-repos.xml")
//@ComponentScan(basePackages={"ng.exelon.frankinator2_0.data"})
@EnableJpaRepositories(basePackages={"ng.exelon.frankinator2_0.data"})
@PropertySource(value = {"classpath:application.properties"})
public class JpaHibernateConfig {

	@Autowired
	private Environment environment;
		@Bean
	public HikariDataSource getDataSource() {
		HikariDataSource dataSource =  new HikariDataSource();
		dataSource.setDataSourceClassName(environment.getRequiredProperty("jdbc.driverClassname"));
		dataSource.addDataSourceProperty("databaseName",  environment.getRequiredProperty("jdbc.dbName"));
		dataSource.addDataSourceProperty("user", environment.getRequiredProperty("jdbc.username"));
		dataSource.addDataSourceProperty("password", environment.getRequiredProperty("jdbc.password"));
		dataSource.addDataSourceProperty("portNumber", environment.getRequiredProperty("jdbc.port"));
		dataSource.addDataSourceProperty("serverName", environment.getRequiredProperty("jdbc.serverName"));
		return dataSource;
	}
	
	@Bean
    @Autowired
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        JpaDialect jpaDialect = new HibernateJpaDialect();
        txManager.setEntityManagerFactory(entityManagerFactory);
        txManager.setJpaDialect(jpaDialect);
        return txManager;
    }
	
	@Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }
	
	@Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(true);
        jpaVendorAdapter.setGenerateDdl(true);
        jpaVendorAdapter.setDatabase(Database.POSTGRESQL);
        jpaVendorAdapter.setDatabasePlatform(environment.getRequiredProperty("hibernate.dialect"));
        return jpaVendorAdapter;
    }

    @Bean
    @Autowired
    public EntityManagerFactory entityManagerFactory(HikariDataSource dataSource) {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(jpaVendorAdapter());
        factory.setPackagesToScan("ng.exelon.frankinator2_0.model");
        factory.setDataSource(dataSource);

        Properties properties = new Properties();
        properties.setProperty("hibernate.cache.use_second_level_cache", environment.getRequiredProperty("hibernate.cache.use_second_level_cache"));
        properties.setProperty("hibernate.cache.region.factory_class", environment.getRequiredProperty("hibernate.cache.region.factory_class"));
        properties.setProperty("hibernate.cache.use_query_cache", environment.getRequiredProperty("hibernate.cache.use_query_cache"));
        properties.setProperty("hibernate.generate_statistics", environment.getRequiredProperty("hibernate.generate_statistics"));
        properties.setProperty("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.setProperty("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));

        factory.setJpaProperties(properties);

        factory.afterPropertiesSet();

        return factory.getObject();
    }
	
}
