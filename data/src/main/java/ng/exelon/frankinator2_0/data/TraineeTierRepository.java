package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.TraineeTier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TraineeTierRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<TraineeTier> findAll(Integer page) {
		logger.warn("Getting all TraineeTiers from repository...");
		TypedQuery<TraineeTier> customQuery = em.createNamedQuery("TraineeTier.findAll", TraineeTier.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		return customQuery.getResultList();
	}
	
	public TraineeTier findById(String id) {
		return em.find(TraineeTier.class, id);
	}

	public TraineeTier save(TraineeTier TraineeTier) {

		if (TraineeTier.getCode() == null) {
			em.persist(TraineeTier);
		} else {
			em.merge(TraineeTier);
		}

		return TraineeTier;
	}

	public void deleteById(String id) {
		TraineeTier traineetierTier = findById(id);
		em.remove(traineetierTier);
	}

}
