package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.Partner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PartnerRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<Partner> findAll(Integer page) {
		logger.warn("Getting all Partners from repository...");
		TypedQuery<Partner> customQuery = em.createNamedQuery("Partner.findAll", Partner.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<Partner> ps = customQuery.getResultList();
		for(Partner p: ps)
			this.logit(p);
		return ps;
	}
	
	public Partner findById(Long id) {
		Partner p = em.find(Partner.class, id);
		this.logit(p);
		return p;
	}

	public Partner save(Partner Partner) {

		if (Partner.getId() == null) {
			em.persist(Partner);
		} else {
			em.merge(Partner);
		}

		return Partner;
	}

	public void deleteById(Long id) {
		Partner partner = findById(id);
		em.remove(partner);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(Partner partner) {
		if(partner != null) {
			logger.info("partner --> {}", partner);
			logger.info("partner trainers --> {}", partner.getTrainers());
		}
	}

}
