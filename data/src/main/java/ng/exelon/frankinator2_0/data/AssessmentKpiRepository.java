package ng.exelon.frankinator2_0.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import ng.exelon.frankinator2_0.model.Assessment;
import ng.exelon.frankinator2_0.model.AssessmentKpi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class AssessmentKpiRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final int LIMITRESULTSPERPAGE = 10;
	
	@Autowired
	EntityManager em;

	public List<AssessmentKpi> findAll(Integer page) {
		logger.warn("Getting all AssessmentKpis from repository...");
		TypedQuery<AssessmentKpi> customQuery = em.createNamedQuery("AssessmentKpi.findAll", AssessmentKpi.class);
		customQuery.setFirstResult(((page == null) || (page < 1)) ? 0 : (page - 1) * LIMITRESULTSPERPAGE);
		customQuery.setMaxResults(LIMITRESULTSPERPAGE);
		List<AssessmentKpi> assessmentKpis = customQuery.getResultList();
		// Fetch relation in a single transaction
		for(AssessmentKpi ak: assessmentKpis)
			this.logit(ak);
		return assessmentKpis;
	}
	
	public AssessmentKpi findById(Long id) {
		AssessmentKpi assessmentKpi = em.find(AssessmentKpi.class, id);
		// Fetch relation in a single transaction
		this.logit(assessmentKpi);
		return 	assessmentKpi;	
	}

	public AssessmentKpi save(AssessmentKpi AssessmentKpi) {

		if (AssessmentKpi.getId() == null) {
			em.persist(AssessmentKpi);
		} else {
			em.merge(AssessmentKpi);
		}

		return AssessmentKpi;
	}

	public void deleteById(Long id) {
		AssessmentKpi AssessmentKpi = findById(id);
		em.remove(AssessmentKpi);
	}
	
	/**
	 * A little hack to avoid using an EAGER fetch type for optimization
	 */
	private void logit(AssessmentKpi assessmentKpi) {
		if(assessmentKpi != null) {
			logger.info("AssessmentKpi --> {}", assessmentKpi);
			logger.info("Assessment --> {}", assessmentKpi.getAssessment().getId());
			logger.info("Kpi --> " + assessmentKpi.getKpi().getId());
		}
	}

}
