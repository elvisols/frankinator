package ng.exelon.frankinator.config;

import io.swagger.annotations.Api;
import ng.exelon.frankinator2_0.logs.LoggingInterceptor;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableWebMvc
@EnableSwagger2
@ComponentScan(basePackages = {"ng.exelon.frankinator2_0.data","ng.exelon.frankinator2_0.service","ng.exelon.frankinator", "ng.exelon.frankinator2_0.exception"})
public class RootConfig extends WebMvcConfigurerAdapter {

	@Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(new LoggingInterceptor()).addPathPatterns("/**");
    }
	
	@Bean
    public Docket api() {
       return new Docket(DocumentationType.SWAGGER_2).select()
               .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
               .paths(PathSelectors.any()).build().pathMapping("/")
               .apiInfo(generateApiInfo()).useDefaultResponseMessages(false);
    }
	
	@Bean 
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver mr = new CommonsMultipartResolver();
//		mr.setMaxUploadSizePerFile(123456789);
		return mr;
	}
	
	// See http://springfox.github.io/springfox/docs/snapshot/#springfox-spring-mvc-and-spring-boot
	private ApiInfo generateApiInfo()
    {
        return new ApiInfo("Frankinator Server Applicant Test Service", "This service is to check the technology of DigitalSkills", "Version 1.0 - mw",
            "urn:tos", "test@frankinator.com", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");
    }
	
//	@Override
//	public void addViewControllers(ViewControllerRegistry registry) {
//	    registry.addRedirectViewController("/api/v2/api-docs", "/v2/api-docs");
//	    registry.addRedirectViewController("/api/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui");
//	    registry.addRedirectViewController("/api/swagger-resources/configuration/security", "/swagger-resources/configuration/security");
//	    registry.addRedirectViewController("/api/swagger-resources", "/swagger-resources");
//	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    registry.addResourceHandler("/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
	    registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
//	@Bean
//	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
//	    ObjectMapper mapper = new ObjectMapper();
//	    mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//	    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(mapper);
//	    return converter;
//	}

}
