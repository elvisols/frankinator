package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.value.TargetQuarter;
import ng.exelon.frankinator2_0.model.Target;
import ng.exelon.frankinator2_0.model.TargetDTO;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import ng.exelon.frankinator2_0.model.TraineeTier;

public class TargetMapper {

    public static Target makeTarget(TargetDTO targetDTO)
    {
        Target target = new Target(targetDTO.getPartner(), targetDTO.getCourse(), targetDTO.getCountry(), targetDTO.getTier(), targetDTO.getGroup(), targetDTO.getName(), targetDTO.getDescription(), targetDTO.getTargetYear(), targetDTO.getTargetValue(), targetDTO.getQuarter(), targetDTO.getStartDate(), targetDTO.getEndDate());
        return target;
    }
    

    public static TargetDTO makeTargetDTO(Target target)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	target.getPartner().setEmployees(null);
    	target.getPartner().setTrainers(null);
    	
        TargetDTO.TargetDTOBuilder targetDTOBuilder = TargetDTO.newBuilder()
            .setId(target.getId())
            .setCountry(target.getCountry())
            .setCourse(target.getCourse())
            .setDescription(target.getDescription())
            .setEndDate(target.getEndDate())
            .setGroup(target.getGroup())
            .setName(target.getName())
            .setPartner(target.getPartner())
            .setQuarter(target.getQuarter())
            .setStartDate(target.getStartDate())
            .setTargetValue(target.getTargetValue())
            .setTargetYear(target.getTargetYear())
            .setTier(target.getTier());
        return targetDTOBuilder.createTargetDTO();
    }
    
    public static List<TargetDTO> makeTargetDTOList(Collection<Target> targets) {
        return targets.stream()
            .map(TargetMapper::makeTargetDTO)
            .collect(Collectors.toList());
    }
    
}
