package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.KpiCategory;
import ng.exelon.frankinator2_0.model.KpiCategoryDTO;

public class KpiCategoryMapper {

    public static KpiCategory makeKpiCategory(KpiCategoryDTO kpiCategoryDTO)
    {
        KpiCategory kpiCategory = new KpiCategory(kpiCategoryDTO.getId(), kpiCategoryDTO.getName(), kpiCategoryDTO.getDescription(), kpiCategoryDTO.getKpis());
        return kpiCategory;
    }
    

    public static KpiCategoryDTO makeKpiCategoryDTO(KpiCategory kpiCategory)
    {
//    	kpiCategory.getKpis().get(0)
    	
        KpiCategoryDTO.KpiCategoryDTOBuilder kpiCategoryDTOBuilder = KpiCategoryDTO.newBuilder()
            .setId(kpiCategory.getId())
            .setName(kpiCategory.getName())
            .setDescription(kpiCategory.getDescription())
            .setKpis(kpiCategory.getKpis());
        return kpiCategoryDTOBuilder.createKpiCategoryDTO();
    }
    
    public static List<KpiCategoryDTO> makeKpiCategoryDTOList(Collection<KpiCategory> kpiCategorys) {
        return kpiCategorys.stream()
            .map(KpiCategoryMapper::makeKpiCategoryDTO)
            .collect(Collectors.toList());
    }
    
}
