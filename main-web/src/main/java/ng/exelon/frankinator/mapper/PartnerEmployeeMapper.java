package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.PartnerEmployee;
import ng.exelon.frankinator2_0.model.PartnerEmployeeDTO;

public class PartnerEmployeeMapper {

    public static PartnerEmployee makePartnerEmployee(PartnerEmployeeDTO partnerEmployeeDTO)
    {
        PartnerEmployee partnerEmployee = new PartnerEmployee(partnerEmployeeDTO.getFirstName(), partnerEmployeeDTO.getMiddleName(), partnerEmployeeDTO.getLastName(), partnerEmployeeDTO.getSex(), partnerEmployeeDTO.getDateOfBirth(), partnerEmployeeDTO.getMaritalStatus(), partnerEmployeeDTO.getMobileNumber(), partnerEmployeeDTO.getEmail(), partnerEmployeeDTO.getBirthCountry(), partnerEmployeeDTO.getResidentCountry(), partnerEmployeeDTO.getPartner());
        return partnerEmployee;
    }
    

    public static PartnerEmployeeDTO makePartnerEmployeeDTO(PartnerEmployee partnerEmployee)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	partnerEmployee.getPartner().setEmployees(null);
    	partnerEmployee.getPartner().setTrainers(null);
    	
        PartnerEmployeeDTO.PEDTOBuilder partnerEmployeeDTOBuilder = PartnerEmployeeDTO.newPEDTOBuilder()
    		.setId(partnerEmployee.getId())
            .setPartner(partnerEmployee.getPartner())
            .setFirstName(partnerEmployee.getFirstName())
            .setMiddleName(partnerEmployee.getMiddleName())
            .setLastName(partnerEmployee.getLastName())
            .setSex(partnerEmployee.getSex())
            .setDateOfBirth(partnerEmployee.getDateOfBirth())
            .setMaritalStatus(partnerEmployee.getMaritalStatus())
            .setMobileNumber(partnerEmployee.getMobileNumber())
            .setEmail(partnerEmployee.getEmail())
            .setBirthCountry(partnerEmployee.getBirthCountry())
            .setResidentCountry(partnerEmployee.getResidentCountry());
        return partnerEmployeeDTOBuilder.createPEDTO();
    }
    
    public static List<PartnerEmployeeDTO> makePartnerEmployeeDTOList(Collection<PartnerEmployee> partnerEmployees) {
        return partnerEmployees.stream()
            .map(PartnerEmployeeMapper::makePartnerEmployeeDTO)
            .collect(Collectors.toList());
    }
    
}
