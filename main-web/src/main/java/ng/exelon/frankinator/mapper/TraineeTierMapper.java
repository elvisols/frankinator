package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.TraineeTier;
import ng.exelon.frankinator2_0.model.TraineeTierDTO;

public class TraineeTierMapper {

    public static TraineeTier makeTraineeTier(TraineeTierDTO traineeTierDTO)
    {
        TraineeTier traineeTier = new TraineeTier(traineeTierDTO.getCode(), traineeTierDTO.getName(), traineeTierDTO.getDescription());
        return traineeTier;
    }
    

    public static TraineeTierDTO makeTraineeTierDTO(TraineeTier traineeTier)
    {
        TraineeTierDTO.TraineeTierDTOBuilder traineeTierDTOBuilder = TraineeTierDTO.newBuilder()
            .setCode(traineeTier.getCode())
            .setDescription(traineeTier.getDescription())
            .setName(traineeTier.getName());
        
        return traineeTierDTOBuilder.createTraineeTierDTO();
    }
    
    public static List<TraineeTierDTO> makeTraineeTierDTOList(Collection<TraineeTier> traineeTiers) {
        return traineeTiers.stream()
            .map(TraineeTierMapper::makeTraineeTierDTO)
            .collect(Collectors.toList());
    }
    
}
