package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.TraineeDTO;

public class TraineeMapper {

    public static Trainee makeTrainee(TraineeDTO traineeDTO)
    {
        Trainee trainee = new Trainee(traineeDTO.getFirstName(), traineeDTO.getMiddleName(), traineeDTO.getLastName(), traineeDTO.getSex(), traineeDTO.getDateOfBirth(), traineeDTO.getMaritalStatus(), traineeDTO.getMobileNumber(), traineeDTO.getEmail(), traineeDTO.getBirthCountry(), traineeDTO.getResidentCountry(), traineeDTO.getTrainingUpload());
        return trainee;
    }
    

    public static TraineeDTO makeTraineeDTO(Trainee trainee)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	trainee.getTraining().setMedia(null);
    	trainee.getTraining().setPartner(null);
    	trainee.getTraining().setTrainees(null);
    	trainee.getTraining().setTrainer(null);
    	
        TraineeDTO.TraineeDTOBuilder traineeDTOBuilder = TraineeDTO.newBuilder()
            .setId(trainee.getId())
            .setTrainingUpload(trainee.getTraining())
            .setFirstName(trainee.getFirstName())
            .setMiddleName(trainee.getMiddleName())
            .setLastName(trainee.getLastName())
            .setSex(trainee.getSex())
            .setDateOfBirth(trainee.getDateOfBirth())
            .setMaritalStatus(trainee.getMaritalStatus())
            .setMobileNumber(trainee.getMobileNumber())
            .setEmail(trainee.getEmail())
            .setBirthCountry(trainee.getBirthCountry())
            .setResidentCountry(trainee.getResidentCountry());
      
        return traineeDTOBuilder.createTraineeDTO();
    }
    
    public static List<TraineeDTO> makeTraineeDTOList(Collection<Trainee> trainees) {
        return trainees.stream()
            .map(TraineeMapper::makeTraineeDTO)
            .collect(Collectors.toList());
    }
    
}
