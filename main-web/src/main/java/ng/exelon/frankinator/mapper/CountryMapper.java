package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.CountryDTO;

public class CountryMapper {

    public static Country makeCountry(CountryDTO countryDTO)
    {
        Country country = new Country(countryDTO.getCode(), countryDTO.getName());
        return country;
    }
    

    public static CountryDTO makeCountryDTO(Country country)
    {
        CountryDTO.CountryDTOBuilder countryDTOBuilder = CountryDTO.newBuilder()
            .setCode(country.getCode())
        	.setName(country.getName());
        return countryDTOBuilder.createCountryDTO();
    }
    
    public static List<CountryDTO> makeCountryDTOList(Collection<Country> countrys) {
        return countrys.stream()
            .map(CountryMapper::makeCountryDTO)
            .collect(Collectors.toList());
    }
    
}
