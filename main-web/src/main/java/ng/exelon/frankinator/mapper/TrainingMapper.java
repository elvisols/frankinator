package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.Language;
import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import ng.exelon.frankinator2_0.model.TraineeTier;
import ng.exelon.frankinator2_0.model.Trainer;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.Training;
import ng.exelon.frankinator2_0.model.TrainingDTO;
import ng.exelon.frankinator2_0.model.User;

public class TrainingMapper {

    public static Training makeTraining(TrainingDTO trainingDTO)
    {
        Training training = new Training(trainingDTO.getTrainer(), trainingDTO.getPartner(), trainingDTO.getTier(), trainingDTO.getAudience(), trainingDTO.getCourse(), trainingDTO.getScheduledBy(), trainingDTO.getApprovedBy(), trainingDTO.getRequestDate(), trainingDTO.getRequestTime(), trainingDTO.getApprovedDate(), trainingDTO.getApprovedTime(), trainingDTO.getCountry(), trainingDTO.getLongitude(), trainingDTO.getLatitude(), trainingDTO.getLanguage(), trainingDTO.getScheduledDate(), trainingDTO.getScheduledTime(), trainingDTO.getDurationInMins(), trainingDTO.getExpectedAttendees(), trainingDTO.getAddress());
        return training;
    }
    

    public static TrainingDTO makeTrainingDTO(Training t)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	t.getPartner().setEmployees(null);
    	t.getPartner().setTrainers(null);
    	t.getScheduledBy().setRoles(null);
    	t.getTrainer().setPartner(null);
    	
        TrainingDTO.TrainingDTOBuilder trainingDTOBuilder = TrainingDTO.newBuilder()
            .setId(t.getId())
            .setAddress(t.getAddress())
            .setApprovedBy(t.getApprovedBy())
            .setApprovedDate(t.getApprovedDate())
            .setApprovedTime(t.getApprovedTime())
            .setAudience(t.getAudience())
            .setCountry(t.getCountry())
            .setCourse(t.getCourse())
            .setDurationInMins(t.getDurationInMins())
            .setExpectedAttendees(t.getExpectedAttendees())
            .setLanguage(t.getLanguage())
            .setLatitude(t.getLatitude())
            .setLongitude(t.getLongitude())
            .setPartner(t.getPartner())
            .setRequestDate(t.getRequestDate())
            .setRequestTime(t.getRequestTime())
            .setScheduledBy(t.getScheduledBy())
            .setScheduledDate(t.getScheduledDate())
            .setScheduledTime(t.getScheduledTime())
            .setTier(t.getTier())
            .setTrainer(t.getTrainer());
        return trainingDTOBuilder.createTrainingDTO();
    }
    
    public static List<TrainingDTO> makeTrainingDTOList(Collection<Training> trainings) {
        return trainings.stream()
            .map(TrainingMapper::makeTrainingDTO)
            .collect(Collectors.toList());
    }
    
}
