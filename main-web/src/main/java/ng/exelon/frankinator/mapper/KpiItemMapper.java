package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.KpiItem;
import ng.exelon.frankinator2_0.model.KpiItemDTO;

public class KpiItemMapper {

    public static KpiItem makeKpiItem(KpiItemDTO kpiItemDTO)
    {
        KpiItem kpiItem = new KpiItem(kpiItemDTO.getName(), kpiItemDTO.getDescription(), kpiItemDTO.getKpi(), kpiItemDTO.getWeight());
        return kpiItem;
    }
    

    public static KpiItemDTO makeKpiItemDTO(KpiItem kpiItem)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	kpiItem.getKpi().setCategory(null);
    	kpiItem.getKpi().setInputType(null);
    	
        KpiItemDTO.KpiItemDTOBuilder kpiItemDTOBuilder = KpiItemDTO.newBuilder()
            .setId(kpiItem.getId())
            .setName(kpiItem.getName())
            .setDescription(kpiItem.getDescription())
            .setWeight(kpiItem.getWeight())
            .setKpi(kpiItem.getKpi());
        return kpiItemDTOBuilder.createKpiItemDTO();
    }
    
    public static List<KpiItemDTO> makeKpiItemDTOList(Collection<KpiItem> kpiItems) {
        return kpiItems.stream()
            .map(KpiItemMapper::makeKpiItemDTO)
            .collect(Collectors.toList());
    }
    
}
