package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.TierEnum;
import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.CourseDTO;
import ng.exelon.frankinator2_0.model.CourseTierDeleted;

public class CourseMapper {

    public static Course makeCourse(CourseDTO courseDTO)
    {
//    	CourseTierDeleted ct = new CourseTierDeleted(courseDTO.getTier().toString(), "Unavailable...");
        Course course = new Course(courseDTO.getName(), courseDTO.getDescription());
        return course;
    }
    

    public static CourseDTO makeCourseDTO(Course course)
    {
        CourseDTO.CourseDTOBuilder courseDTOBuilder = CourseDTO.newBuilder()
            .setId(course.getId())
            .setName(course.getName())
            .setDescription(course.getDescription());
        
//        if (course != null)
//        {
//        	courseDTOBuilder.setTier(TierEnum.valueOf(course.getTier().getCode()));
//        }

        return courseDTOBuilder.createCourseDTO();
    }
    
    public static List<CourseDTO> makeCourseDTOList(Collection<Course> Courses)
    {
        return Courses.stream()
            .map(CourseMapper::makeCourseDTO)
            .collect(Collectors.toList());
    }
    
}
