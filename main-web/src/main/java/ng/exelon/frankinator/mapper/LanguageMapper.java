package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.Language;
import ng.exelon.frankinator2_0.model.LanguageDTO;

public class LanguageMapper {

    public static Language makeLanguage(LanguageDTO languageDTO)
    {
        Language language = new Language(languageDTO.getCode(), languageDTO.getName());
        return language;
    }
    

    public static LanguageDTO makeLanguageDTO(Language language)
    {
        LanguageDTO.LanguageDTOBuilder languageDTOBuilder = LanguageDTO.newBuilder()
            .setCode(language.getCode())
            .setName(language.getName());
        return languageDTOBuilder.createLanguageDTO();
    }
    
    public static List<LanguageDTO> makeLanguageDTOList(Collection<Language> languages) {
        return languages.stream()
            .map(LanguageMapper::makeLanguageDTO)
            .collect(Collectors.toList());
    }
    
}
