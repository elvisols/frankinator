package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.KpiDTO;

public class KpiMapper {

    public static Kpi makeKpi(KpiDTO kpiDTO)
    {
        Kpi kpi = new Kpi(kpiDTO.getName(), kpiDTO.getDescription(), kpiDTO.getInputType(), kpiDTO.getCategory());
        return kpi;
    }
    

    public static KpiDTO makeKpiDTO(Kpi kpi)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	kpi.getCategory().setKpis(null);
    	
        KpiDTO.KpiDTOBuilder kpiDTOBuilder = KpiDTO.newBuilder()
            .setId(kpi.getId())
            .setName(kpi.getName())
            .setDescription(kpi.getDescription())
            .setInputType(kpi.getInputType())
            .setCategory(kpi.getCategory());
        return kpiDTOBuilder.createKpiDTO();
    }
    
    public static List<KpiDTO> makeKpiDTOList(Collection<Kpi> kpis) {
        return kpis.stream()
            .map(KpiMapper::makeKpiDTO)
            .collect(Collectors.toList());
    }
    
}
