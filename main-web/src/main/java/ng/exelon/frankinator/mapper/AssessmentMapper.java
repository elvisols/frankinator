package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import ng.exelon.frankinator2_0.model.Assessment;
import ng.exelon.frankinator2_0.model.AssessmentDTO;
import ng.exelon.frankinator2_0.model.Trainer;
import ng.exelon.frankinator2_0.model.TrainerDTO;
import ng.exelon.frankinator2_0.model.User;

public class AssessmentMapper {
	
//	@Autowired
//	EntityManager em;
//	
//	public void getUser() {
//		 Session session = em.unwrap(Session.class);
//		 User user = session.load(User.class, 1);
//	}
	
    public static Assessment makeAssessment(AssessmentDTO assessmentDTO)
    {
        Assessment assessment = new Assessment(assessmentDTO.getAssessor(), assessmentDTO.getTrainer(), assessmentDTO.getTraining(), assessmentDTO.getCreatedBy(), assessmentDTO.getKpis());
        return assessment;
    }
    

    public static AssessmentDTO makeAssessmentDTO(Assessment assessment)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	assessment.getTrainer().setPartner(null); // Trainer...
    	assessment.getTraining().setPartner(null); // Training...
    	assessment.getTraining().setApprovedBy(null);
    	assessment.getTraining().setAudience(null);
    	assessment.getTraining().setCountry(null);
    	assessment.getTraining().setCourse(null);
    	assessment.getTraining().setLanguage(null);
    	assessment.getTraining().setScheduledBy(null);
    	assessment.getTraining().setTier(null);
    	assessment.getTraining().setTrainer(null);
    	System.out.println("User id: " + assessment.getCreatedBy().getId());
//    	if(assessment.getCreatedBy().getRoles().isEmpty()) {
//    		System.out.println("Roles is empty");
//    	} else {
//    		System.out.println("Role not empty: " + assessment.getCreatedBy().getRoles());
//    		assessment.getCreatedBy().setRoles(null); // User...
//    	}
    	
    	if(assessment.getKpis().isEmpty()) {
    		System.out.println("Kpis is empty");
    	} else {
    		System.out.println("Kpis not empty: " + assessment.getKpis());
    		assessment.getKpis().get(0).setAssessment(null);
    		assessment.getKpis().get(0).setKpi(null);
    	}
    	
        AssessmentDTO.AssessmentDTOBuilder assessmentDTOBuilder = AssessmentDTO.newBuilder()
            .setId(assessment.getId())
            .setAssessor(assessment.getAssessor())
            .setTrainer(assessment.getTrainer())
            .setTraining(assessment.getTraining())
            .setCreatedBy(assessment.getCreatedBy()) 
            .setKpis(assessment.getKpis());
        return assessmentDTOBuilder.createAssessmentDTO();
    }
    
    public static List<AssessmentDTO> makeAssessmentDTOList(Collection<Assessment> assessments) {
        return assessments.stream()
            .map(AssessmentMapper::makeAssessmentDTO)
            .collect(Collectors.toList());
    }
    
}
