package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.Trainer;
import ng.exelon.frankinator2_0.model.AbstractEmbeddable.Address;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.PartnerDTO;
import ng.exelon.frankinator2_0.model.PartnerEmployee;

public class PartnerMapper {

    public static Partner makePartner(PartnerDTO partnerDTO)
    {
    	Partner partner = new Partner(partnerDTO.getName(), partnerDTO.getContactPhone(), partnerDTO.getRcNumber(), partnerDTO.getIncDate(), partnerDTO.getMobileNumber(), partnerDTO.getEmail(), partnerDTO.getWebSite(), partnerDTO.getAddress(), partnerDTO.getGis(), partnerDTO.getShortName(), partnerDTO.getDescription(), partnerDTO.getDateAdded(), partnerDTO.getTrainers(), partnerDTO.getEmployees());
        return partner;
    }
    

    public static PartnerDTO makePartnerDTO(Partner partner)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	
        PartnerDTO.PartnerDTOBuilder partnerDTOBuilder = PartnerDTO.newBuilder()
            .setId(partner.getId())
            .setEmployees(partner.getEmployees())
            .setTrainers(partner.getTrainers())
            .setAddress(partner.getAddress())
            .setContactPhone(partner.getContactPhone())
            .setDateAdded(partner.getDateAdded())
            .setDescription(partner.getDescription())
            .setEmail(partner.getEmail())
            .setGis(partner.getGis())
            .setIncDate(partner.getIncDate())
            .setMobileNumber(partner.getMobileNumber())
            .setName(partner.getName())
            .setRcNumber(partner.getRcNumber())
            .setShortName(partner.getShortName())
            .setWebSite(partner.getWebSite());
        
        return partnerDTOBuilder.createPartnerDTO();
    }
    
    public static List<PartnerDTO> makePartnerDTOList(Collection<Partner> partners) {
        return partners.stream()
            .map(PartnerMapper::makePartnerDTO)
            .collect(Collectors.toList());
    }
    
}
