package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.User;
import ng.exelon.frankinator2_0.model.UserDTO;

public class UserMapper {

    public static User makeUser(UserDTO userDTO)
    {
    	Role role = new Role(userDTO.getRole());
    	PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        User user = new User(userDTO.getEmail(), passwordEncoder.encode(userDTO.getPassword()), userDTO.getMobileNumber(), userDTO.getFirstName(), userDTO.getLastName(), userDTO.getSex(), userDTO.getProfileEndDate(), userDTO.getProfileEndDate());
//        user.addRole(role);
        return user;
    }
    

    public static UserDTO makeUserDTO(User user)
    {
    	System.out.println("User:>>> " + user);
        UserDTO.UserDTOBuilder userDTOBuilder = UserDTO.newBuilder()
            .setId(user.getId())
            .setEmail(user.getEmail())
            .setPassword(user.getPassword())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName())
            .setEnabled(user.getEnabled())
            .setMobileNumber(user.getMobileNumber())
            .setProfileEndDate(user.getProfileEndDate())
            .setProfileStartDate(user.getProfileStartDate())
            .setSex(user.getSex());
        
        List<Role> roles = user.getRoles();
        if (roles != null)
        {
        	for(Role r: roles) {
        		Map<String, Object> userRole = new HashMap<>();
        		userRole.put("id", r.getId());
        		userRole.put("role", r.getRole());
        		userDTOBuilder.addRole(userRole);
        	}
        }

        return userDTOBuilder.createUserDTO();
    }
    
    public static List<UserDTO> makeUserDTOList(Collection<User> users)
    {
        return users.stream()
            .map(UserMapper::makeUserDTO)
            .collect(Collectors.toList());
    }
    
}
