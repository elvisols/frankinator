package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.MediaType;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.TrainingMedia;
import ng.exelon.frankinator2_0.model.TrainingMediaDTO;
import ng.exelon.frankinator2_0.model.TrainingUpload;

public class TrainingMediaMapper {

    public static TrainingMedia makeTrainingMedia(TrainingMediaDTO trainingMediaDTO)
    {
        TrainingMedia trainingMedia = new TrainingMedia(trainingMediaDTO.getTraining(), trainingMediaDTO.getMediaType(), trainingMediaDTO.getFilename(), new Date(), new Date());
        return trainingMedia;
    }
    

    public static TrainingMediaDTO makeTrainingMediaDTO(TrainingMedia trainingMedia)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	trainingMedia.getTraining().setMedia(null);
    	trainingMedia.getTraining().setPartner(null);
    	trainingMedia.getTraining().setTrainees(null);
    	trainingMedia.getTraining().setTrainer(null);
    	
        TrainingMediaDTO.TrainingMediaDTOBuilder trainingMediaDTOBuilder = TrainingMediaDTO.newBuilder()
            .setId(trainingMedia.getId())
            .setFilename(trainingMedia.getFilename())
            .setMediaType(trainingMedia.getMediaType())
            .setTraining(trainingMedia.getTraining())
            .setUploadDate(trainingMedia.getUploadDate())
            .setUploadTime(trainingMedia.getUploadTime());
        
        return trainingMediaDTOBuilder.createTrainingMediaDTO();
    }
    
    public static List<TrainingMediaDTO> makeTrainingMediaDTOList(Collection<TrainingMedia> trainingMedias) {
        return trainingMedias.stream()
            .map(TrainingMediaMapper::makeTrainingMediaDTO)
            .collect(Collectors.toList());
    }
    
}
