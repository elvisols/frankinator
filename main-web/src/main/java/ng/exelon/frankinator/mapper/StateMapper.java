package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.State;
import ng.exelon.frankinator2_0.model.StateDTO;

public class StateMapper {

    public static State makeState(StateDTO stateDTO)
    {
        State state = new State(stateDTO.getCountry(), stateDTO.getCode(), stateDTO.getName(), stateDTO.getCapital());
        return state;
    }
    

    public static StateDTO makeStateDTO(State state)
    {
        StateDTO.StateDTOBuilder stateDTOBuilder = StateDTO.newBuilder()
            .setId(state.getId())
            .setCapital(state.getCapital())
            .setCode(state.getCode())
            .setCountry(state.getCountry())
            .setName(state.getName());
        return stateDTOBuilder.createStateDTO();
    }
    
    public static List<StateDTO> makeStateDTOList(Collection<State> states) {
        return states.stream()
            .map(StateMapper::makeStateDTO)
            .collect(Collectors.toList());
    }
    
}
