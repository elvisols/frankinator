package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import ng.exelon.frankinator2_0.model.TraineeGroupDTO;

public class TraineeGroupMapper {

    public static TraineeGroup makeTraineeGroup(TraineeGroupDTO traineeGroupDTO)
    {
        TraineeGroup traineeGroup = new TraineeGroup(traineeGroupDTO.getCode(), traineeGroupDTO.getDescription());
        return traineeGroup;
    }
    

    public static TraineeGroupDTO makeTraineeGroupDTO(TraineeGroup traineeGroup)
    {
        TraineeGroupDTO.TraineeGroupDTOBuilder traineeGroupDTOBuilder = TraineeGroupDTO.newBuilder()
            .setCode(traineeGroup.getCode())
            .setDescription(traineeGroup.getDescription());
        
        return traineeGroupDTOBuilder.createTraineeGroupDTO();
    }
    
    public static List<TraineeGroupDTO> makeTraineeGroupDTOList(Collection<TraineeGroup> traineeGroups) {
        return traineeGroups.stream()
            .map(TraineeGroupMapper::makeTraineeGroupDTO)
            .collect(Collectors.toList());
    }
    
}
