package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.Trainer;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.value.TrainingStatusType;
import ng.exelon.frankinator2_0.model.TrainingStatus;
import ng.exelon.frankinator2_0.model.TrainingStatusDTO;
import ng.exelon.frankinator2_0.model.User;

public class TrainingStatusMapper {

    public static TrainingStatus makeTrainingStatus(TrainingStatusDTO trainingStatusDTO)
    {
        TrainingStatus trainingStatus = new TrainingStatus(trainingStatusDTO.getUser(), trainingStatusDTO.getTraining(), trainingStatusDTO.getStatusDate(), trainingStatusDTO.getStatusTime(), trainingStatusDTO.getMessage(), trainingStatusDTO.getStatusType());
        return trainingStatus;
    }
    

    public static TrainingStatusDTO makeTrainingStatusDTO(TrainingStatus trainingStatus)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	trainingStatus.getUser().setRoles(null);
    	trainingStatus.getTraining().setPartner(null);
    	
        TrainingStatusDTO.TrainingStatusDTOBuilder trainingStatusDTOBuilder = TrainingStatusDTO.newBuilder()
            .setId(trainingStatus.getId())
            .setMessage(trainingStatus.getMessage())
            .setStatusDate(trainingStatus.getStatusDate())
            .setStatusTime(trainingStatus.getStatusTime())
            .setStatusType(trainingStatus.getStatusType())
            .setTraining(trainingStatus.getTraining())
            .setUser(trainingStatus.getUser());
        
        return trainingStatusDTOBuilder.createTrainingStatusDTO();
    }
    
    public static List<TrainingStatusDTO> makeTrainingStatusDTOList(Collection<TrainingStatus> trainingStatuss) {
        return trainingStatuss.stream()
            .map(TrainingStatusMapper::makeTrainingStatusDTO)
            .collect(Collectors.toList());
    }
    
}
