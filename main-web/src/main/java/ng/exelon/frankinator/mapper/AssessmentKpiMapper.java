package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import ng.exelon.frankinator2_0.model.AssessmentKpi;
import ng.exelon.frankinator2_0.model.AssessmentKpiDTO;

public class AssessmentKpiMapper {

    public static AssessmentKpi makeAssessmentKpi(AssessmentKpiDTO assessmentKpiDTO)
    {
        AssessmentKpi assessmentKpi = new AssessmentKpi(assessmentKpiDTO.getAssessment(), assessmentKpiDTO.getKpi(), assessmentKpiDTO.getValue());
        return assessmentKpi;
    }
    

    public static AssessmentKpiDTO makeAssessmentKpiDTO(AssessmentKpi assessmentKpi)
    {
    	// Nullify relation for avoid LazyInitialization Exception
    	assessmentKpi.getAssessment().setAssessor(null);
    	assessmentKpi.getAssessment().setCreatedBy(null);
    	assessmentKpi.getAssessment().setKpis(null);
    	assessmentKpi.getAssessment().setTrainer(null);
    	assessmentKpi.getAssessment().setTraining(null);
    	assessmentKpi.getKpi().setCategory(null);
    	assessmentKpi.getKpi().setInputType(null);
    	
        AssessmentKpiDTO.AssessmentKpiDTOBuilder assessmentKpiDTOBuilder = AssessmentKpiDTO.newBuilder()
            .setId(assessmentKpi.getId())
            .setAssessment(assessmentKpi.getAssessment())
            .setKpi(assessmentKpi.getKpi())
            .setValue(assessmentKpi.getValue());
        return assessmentKpiDTOBuilder.createAssessmentKpiDTO();
    }
    
    public static List<AssessmentKpiDTO> makeAssessmentKpiDTOList(Collection<AssessmentKpi> assessmentKpis) {
        return assessmentKpis.stream()
            .map(AssessmentKpiMapper::makeAssessmentKpiDTO)
            .collect(Collectors.toList());
    }
    
}
