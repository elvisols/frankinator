package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.AssessorDTO;

public class AssessorMapper {

    public static Assessor makeAssessor(AssessorDTO assessorDTO)
    {
        Assessor assessor = new Assessor(assessorDTO.getFirstName(), assessorDTO.getMiddleName(), assessorDTO.getLastName(), assessorDTO.getSex(), assessorDTO.getDateOfBirth(), assessorDTO.getMaritalStatus(), assessorDTO.getMobileNumber(), assessorDTO.getEmail(), assessorDTO.getBirthCountry(), assessorDTO.getResidentCountry());
        return assessor;
    }
    

    public static AssessorDTO makeAssessorDTO(Assessor assessor)
    {
        AssessorDTO.AssessorDTOBuilder assessorDTOBuilder = AssessorDTO.newBuilder()
            .setId(assessor.getId())
            .setFirstName(assessor.getFirstName())
            .setMiddleName(assessor.getMiddleName())
            .setLastName(assessor.getLastName())
            .setSex(assessor.getSex())
            .setDateOfBirth(assessor.getDateOfBirth())
            .setMaritalStatus(assessor.getMaritalStatus())
            .setMobileNumber(assessor.getMobileNumber())
            .setEmail(assessor.getEmail())
            .setBirthCountry(assessor.getBirthCountry())
            .setResidentCountry(assessor.getResidentCountry());
        return assessorDTOBuilder.createAssessorDTO();
    }
    
    public static List<AssessorDTO> makeAssessorDTOList(Collection<Assessor> assessors) {
        return assessors.stream()
            .map(AssessorMapper::makeAssessorDTO)
            .collect(Collectors.toList());
    }
    
}
