package ng.exelon.frankinator.mapper;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.Role;
import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.Trainer;
import ng.exelon.frankinator2_0.model.TrainingMedia;
import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;
import ng.exelon.frankinator2_0.model.TrainingUpload;
import ng.exelon.frankinator2_0.model.TrainingUploadDTO;

public class TrainingUploadMapper {

    public static TrainingUpload makeTrainingUpload(TrainingUploadDTO trainingUploadDTO)
    {
        TrainingUpload trainingUpload = new TrainingUpload(trainingUploadDTO.getTrainer(), trainingUploadDTO.getPartner(), trainingUploadDTO.getTrainees(), trainingUploadDTO.getMedia(), trainingUploadDTO.getActualAttendees());
        return trainingUpload;
    }
    

    public static TrainingUploadDTO makeTrainingUploadDTO(TrainingUpload trainingUpload)
    {
    	// Nullify Relation to avoid LazyInitialization exception
    	trainingUpload.getPartner().setEmployees(null);
    	trainingUpload.getPartner().setTrainers(null);
    	trainingUpload.getTrainer().setPartner(null);
    	
        TrainingUploadDTO.TrainingUploadDTOBuilder trainingUploadDTOBuilder = TrainingUploadDTO.newBuilder()
            .setId(trainingUpload.getId())
            .setActualAttendees(trainingUpload.getActualAttendees())
            .setMedia(trainingUpload.getMedia())
            .setPartner(trainingUpload.getPartner())
            .setTrainees(trainingUpload.getTrainees())
            .setTrainer(trainingUpload.getTrainer());
        
        return trainingUploadDTOBuilder.createTrainingUploadDTO();
    }
    
    public static List<TrainingUploadDTO> makeTrainingUploadDTOList(Collection<TrainingUpload> trainingUploads) {
        return trainingUploads.stream()
            .map(TrainingUploadMapper::makeTrainingUploadDTO)
            .collect(Collectors.toList());
    }
    
}
