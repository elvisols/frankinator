package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TrainingUploadMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingUpload;
import ng.exelon.frankinator2_0.model.TrainingUploadDTO;
import ng.exelon.frankinator2_0.service.TrainingUploadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("traininguploads")
//@Api(value = "TrainingUploads")
public class TrainingUploadController {
	
	@Autowired
	TrainingUploadService trainingUploadService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all trainingUploads")
	public ResponseEntity<ResponseWrapper> findTrainingUploads(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TrainingUploads in batch of 10 fetched successfully!", TrainingUploadMapper.makeTrainingUploadDTOList(trainingUploadService.findAllTrainingUploads(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{trainingUploadId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single trainingUpload by id")
	public ResponseEntity<ResponseWrapper> findTrainingUploadById(@PathVariable Long trainingUploadId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single trainingUpload fetched successfully!", TrainingUploadMapper.makeTrainingUploadDTO(trainingUploadService.findOne(trainingUploadId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an trainingUpload")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTrainingUpload(@Valid @RequestBody TrainingUploadDTO trainingUploadDTO) throws ConstraintsViolationException {
		TrainingUpload trainingUpload = TrainingUploadMapper.makeTrainingUpload(trainingUploadDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TrainingUpload created successfully!", TrainingUploadMapper.makeTrainingUploadDTO(trainingUploadService.create(trainingUpload))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{trainingUploadId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an trainingUpload with the specified id")
	public ResponseEntity<ResponseWrapper> updateTrainingUpload(@Valid @PathVariable Long trainingUploadId, @RequestBody TrainingUploadDTO trainingUploadDTO) throws ConstraintsViolationException, EntityNotFoundException {
		TrainingUpload trainingUpload = TrainingUploadMapper.makeTrainingUpload(trainingUploadDTO);
		//Set Id
		trainingUpload.setId(trainingUploadDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated trainingUpload with an ID: " + trainingUploadId, TrainingUploadMapper.makeTrainingUploadDTO(trainingUploadService.update(trainingUpload))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{trainingUploadId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an trainingUpload with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTrainingUpload(@Valid @PathVariable Long trainingUploadId) throws ConstraintsViolationException, EntityNotFoundException {
		trainingUploadService.delete(trainingUploadId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted trainingUpload with an ID: " + trainingUploadId), HttpStatus.OK);
	}
	
}
