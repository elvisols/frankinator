package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.AssessmentMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessment;
import ng.exelon.frankinator2_0.model.AssessmentDTO;
import ng.exelon.frankinator2_0.service.AssessmentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("assessments")
//@Api(value = "Assessments")
public class AssessmentController {
	
	@Autowired
	AssessmentService assessmentService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all assessments")
	public ResponseEntity<ResponseWrapper> findAssessments(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Assessments in batch of 10 fetched successfully!", AssessmentMapper.makeAssessmentDTOList(assessmentService.findAllAssessments(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{assessmentId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single assessment by id")
	public ResponseEntity<ResponseWrapper> findAssessmentById(@PathVariable Long assessmentId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single assessment fetched successfully!", AssessmentMapper.makeAssessmentDTO(assessmentService.findOne(assessmentId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an assessment")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addAssessment(@Valid @RequestBody AssessmentDTO assessmentDTO) throws ConstraintsViolationException {
		Assessment assessment = AssessmentMapper.makeAssessment(assessmentDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Assessment created successfully!", AssessmentMapper.makeAssessmentDTO(assessmentService.create(assessment))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{assessmentId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an assessment with the specified id")
	public ResponseEntity<ResponseWrapper> updateAssessment(@Valid @PathVariable Long assessmentId, @RequestBody AssessmentDTO assessmentDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Assessment assessment = AssessmentMapper.makeAssessment(assessmentDTO);
		//Set Id
		assessment.setId(assessmentDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated assessment with an ID: " + assessmentId, AssessmentMapper.makeAssessmentDTO(assessmentService.update(assessment))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{assessmentId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an assessment with a particular id")
	public ResponseEntity<ResponseWrapper> deleteAssessment(@Valid @PathVariable Long assessmentId) throws ConstraintsViolationException, EntityNotFoundException {
		assessmentService.delete(assessmentId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted assessment with an ID: " + assessmentId), HttpStatus.OK);
	}
	
}
