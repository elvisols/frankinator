package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import ng.exelon.frankinator2_0.model.User;
import ng.exelon.frankinator2_0.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("files")
//@Api(value = "Files")
public class FilesController {
 
	@Autowired
	UserService userService;
	
    @Value("${server.upload.path}")
    private String uploadPath;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ApiOperation(value = "Upload images")
    public ResponseEntity<FileInfo> handleFileUpload(
    		@RequestParam(value = "image") MultipartFile myImage,
    		@RequestParam(value = "reference", required = true) String reference, HttpServletRequest request){
        FileInfo fileInfo = new FileInfo();
        HttpHeaders headers = new HttpHeaders();
        if (!myImage.isEmpty() && !reference.isEmpty() && reference != null) {
        	System.out.println("Validated !"); 
        	try {
				  String imageFilename = myImage.getOriginalFilename();
				  String directoryName = uploadPath +  File.separator + reference;
				  File directory = new File(directoryName);
                  if (! directory.exists()){
				      directory.mkdir();
				  }
				  File myImageDestinationFile = new File(directoryName + File.separator + imageFilename);
				  myImage.transferTo(myImageDestinationFile);
				  
				  fileInfo.setFileName(myImageDestinationFile.getPath());
				  fileInfo.setStatus(200);
				  fileInfo.setMessage("File Uploaded Successfully.");
				  headers.add("File Uploaded Successfully - ", imageFilename);
				  /**** Update user account table  *****/
				  User user = userService.findOne(1L);
//				  user.setImage(passportFilename);
				  userService.create(user);
				  return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
			 } catch (Exception e) {    
				 fileInfo.setStatus(500);
				 fileInfo.setMessage("Error! uploading... " + e.getMessage());
				 return new ResponseEntity<FileInfo>(fileInfo, HttpStatus.INTERNAL_SERVER_ERROR);
			 }
        }else{
        	fileInfo.setStatus(401);
        	fileInfo.setMessage("Could not validate request!");
        	return new ResponseEntity<FileInfo>(fileInfo, HttpStatus.UNAUTHORIZED);
        }
        
    }
    
    private class FileInfo {
    	
    	private String fileName;
    	private Integer status;
    	private String message;
    	
    	public String getFileName() {
    		return fileName;
    	}
    	public void setFileName(String fileName) {
    		this.fileName = fileName;
    	}
    	public Integer getStatus() {
    		return status;
    	}
    	public void setStatus(Integer status) {
    		this.status = status;
    	}
    	public String getMessage() {
    		return message;
    	}
    	public void setMessage(String message) {
    		this.message = message;
    	}
    }
    
}