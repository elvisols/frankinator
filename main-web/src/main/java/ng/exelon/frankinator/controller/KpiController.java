package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.AssessorMapper;
import ng.exelon.frankinator.mapper.KpiMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.AssessorDTO;
import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.KpiDTO;
import ng.exelon.frankinator2_0.service.AssessorService;
import ng.exelon.frankinator2_0.service.KpiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("kpis")
//@Api(value = "Kpis")
public class KpiController {
	
	@Autowired
	KpiService kpiService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all kpis")
	public ResponseEntity<ResponseWrapper> findKpis(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Kpis in batch of 10 fetched successfully!", KpiMapper.makeKpiDTOList(kpiService.findAllKpis(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{kpiId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single kpi by id")
	public ResponseEntity<ResponseWrapper> findKpiById(@PathVariable Long kpiId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single kpi fetched successfully!", KpiMapper.makeKpiDTO(kpiService.findOne(kpiId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an kpi")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addKpi(@Valid @RequestBody KpiDTO kpiDTO) throws ConstraintsViolationException {
		Kpi kpi = KpiMapper.makeKpi(kpiDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Kpi created successfully!", KpiMapper.makeKpiDTO(kpiService.create(kpi))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{kpiId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an kpi with the specified id")
	public ResponseEntity<ResponseWrapper> updateKpi(@Valid @PathVariable Long kpiId, @RequestBody KpiDTO kpiDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Kpi kpi = KpiMapper.makeKpi(kpiDTO);
		//Set Id
		kpi.setId(kpiDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated kpi with an ID: " + kpiId, KpiMapper.makeKpiDTO(kpiService.update(kpi))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{kpiId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an kpi with a particular id")
	public ResponseEntity<ResponseWrapper> deleteKpi(@Valid @PathVariable Long kpiId) throws ConstraintsViolationException, EntityNotFoundException {
		kpiService.delete(kpiId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted kpi with an ID: " + kpiId), HttpStatus.OK);
	}
	
}
