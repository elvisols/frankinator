package ng.exelon.frankinator.controller;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseWrapper {
	
	private Integer status;
	private String message;
	private Object body;
	
	public ResponseWrapper(Integer status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public ResponseWrapper(Integer status, String message, Object body) {
		this.status = status;
		this.message = message;
		this.body = body;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setBody(Object body) {
		this.body = body;
	}

	public Integer getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public Object getBody() {
		return body;
	}
	
}
