package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.AssessorMapper;
import ng.exelon.frankinator.mapper.CountryMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.AssessorDTO;
import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.CountryDTO;
import ng.exelon.frankinator2_0.service.AssessorService;
import ng.exelon.frankinator2_0.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("countries")
//@Api(value = "Countries")
public class CountryController {
	
	@Autowired
	CountryService countryService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all countries")
	public ResponseEntity<ResponseWrapper> findCountrys(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Countries in batch of 10 fetched successfully!", CountryMapper.makeCountryDTOList(countryService.findAllCountrys(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{countryId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single country by id")
	public ResponseEntity<ResponseWrapper> findCountryById(@PathVariable String countryId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single country fetched successfully!", CountryMapper.makeCountryDTO(countryService.findOne(countryId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create a country")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addCountry(@Valid @RequestBody CountryDTO countryDTO) throws ConstraintsViolationException {
		Country country = CountryMapper.makeCountry(countryDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Country created successfully!", CountryMapper.makeCountryDTO(countryService.create(country))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{countryId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update a country with the specified id")
	public ResponseEntity<ResponseWrapper> updateCountry(@Valid @PathVariable String countryId, @RequestBody CountryDTO countryDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Country country = CountryMapper.makeCountry(countryDTO);
		//Set Id
		country.setCode(countryDTO.getCode());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated country with an ID: " + countryId, CountryMapper.makeCountryDTO(countryService.update(country))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{countryId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete a country with a particular id")
	public ResponseEntity<ResponseWrapper> deleteCountry(@Valid @PathVariable String countryId) throws ConstraintsViolationException, EntityNotFoundException {
		countryService.delete(countryId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted country with an ID: " + countryId), HttpStatus.OK);
	}
	
}
