package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.AssessmentKpiMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.AssessmentKpi;
import ng.exelon.frankinator2_0.model.AssessmentKpiDTO;
import ng.exelon.frankinator2_0.service.AssessmentKpiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("assessmentkpis")
//@Api(value = "AssessmentKpis")
public class AssessmentKpiController {
	
	@Autowired
	AssessmentKpiService assessmentKpiService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all assessmentKpis")
	public ResponseEntity<ResponseWrapper> findAssessmentKpis(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "AssessmentKpis in batch of 10 fetched successfully!", AssessmentKpiMapper.makeAssessmentKpiDTOList(assessmentKpiService.findAllAssessmentKpis(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{assessmentKpiId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single assessmentKpi by id")
	public ResponseEntity<ResponseWrapper> findAssessmentKpiById(@PathVariable Long assessmentKpiId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single assessmentKpi fetched successfully!", AssessmentKpiMapper.makeAssessmentKpiDTO(assessmentKpiService.findOne(assessmentKpiId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an assessmentKpi")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addAssessmentKpi(@Valid @RequestBody AssessmentKpiDTO assessmentKpiDTO) throws ConstraintsViolationException {
		AssessmentKpi assessmentKpi = AssessmentKpiMapper.makeAssessmentKpi(assessmentKpiDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "AssessmentKpi created successfully!", AssessmentKpiMapper.makeAssessmentKpiDTO(assessmentKpiService.create(assessmentKpi))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{assessmentKpiId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an assessmentKpi with the specified id")
	public ResponseEntity<ResponseWrapper> updateAssessmentKpi(@Valid @PathVariable Long assessmentKpiId, @RequestBody AssessmentKpiDTO assessmentKpiDTO) throws ConstraintsViolationException, EntityNotFoundException {
		AssessmentKpi assessmentKpi = AssessmentKpiMapper.makeAssessmentKpi(assessmentKpiDTO);
		//Set Id
		assessmentKpi.setId(assessmentKpiDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated assessmentKpi with an ID: " + assessmentKpiId, AssessmentKpiMapper.makeAssessmentKpiDTO(assessmentKpiService.update(assessmentKpi))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{assessmentKpiId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an assessmentKpi with a particular id")
	public ResponseEntity<ResponseWrapper> deleteAssessmentKpi(@Valid @PathVariable Long assessmentKpiId) throws ConstraintsViolationException, EntityNotFoundException {
		assessmentKpiService.delete(assessmentKpiId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted assessmentKpi with an ID: " + assessmentKpiId), HttpStatus.OK);
	}
	
}
