package ng.exelon.frankinator.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ng.exelon.frankinator.mapper.CourseMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.CourseDTO;
import ng.exelon.frankinator2_0.model.value.RoleEnum;
import ng.exelon.frankinator2_0.model.value.TierEnum;
import ng.exelon.frankinator2_0.service.CourseService;
import ng.exelon.frankinator2_0.service.CourseService;

@RestController
@RequestMapping("courses")
//@Api(value = "Courses")
public class CourseController {
	
	@Autowired
	CourseService CourseService;
	
//	@GetMapping(params="format=json", produces=MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
//	@PreAuthorize("#oauth2.hasScope('foo') and hasRole('ADMIN')")
	@ApiOperation(value = "Get all Courses")
	public List<CourseDTO> findCourses() {
		return CourseMapper.makeCourseDTOList(CourseService.findAllCourses());
	}
	
	@GetMapping(value="/{CourseId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'Course')")
	@ApiOperation(value = "Get a single Course by id")
	public CourseDTO findCourseById(@PathVariable Long CourseId) throws EntityNotFoundException {
		return CourseMapper.makeCourseDTO(CourseService.findOne(CourseId));
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'Course')")
	@ApiOperation(value = "Create a Course")
	@ResponseStatus(HttpStatus.CREATED)
	public CourseDTO addCourse(@Valid @RequestBody CourseDTO CourseDTO) throws ConstraintsViolationException {
		Course Course = CourseMapper.makeCourse(CourseDTO);
		return CourseMapper.makeCourseDTO(CourseService.create(Course));
	}
	
	@PutMapping(value="/{CourseId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'Course')")
	@ApiOperation(value = "Update a Course with the specified id")
	public CourseDTO updateCourse(@Valid @PathVariable long CourseId, @RequestBody CourseDTO CourseDTO) throws ConstraintsViolationException, EntityNotFoundException {
//		CourseService.update(CourseId);
		return null;
	}
	
	@DeleteMapping(value="/{CourseId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete a Course with a particular id")
	public CourseDTO deleteCourse(@Valid @PathVariable long CourseId) throws ConstraintsViolationException, EntityNotFoundException {
		CourseService.delete(CourseId);
		return null;
	}
	
	@GetMapping(value="/byroles",produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'Course')")
	@ApiOperation(value = "Get all Courses belonging to a tier")
	public List<CourseDTO> getRoleCourses(@RequestParam TierEnum tier) throws ConstraintsViolationException, EntityNotFoundException {
		return CourseMapper.makeCourseDTOList(CourseService.findCoursesByTier(tier));
	}
	
}
