package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TraineeGroupMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import ng.exelon.frankinator2_0.model.TraineeGroupDTO;
import ng.exelon.frankinator2_0.service.TraineeGroupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("traineegroups")
//@Api(value = "TraineeGroups")
public class TraineeGroupController {
	
	@Autowired
	TraineeGroupService traineeGroupService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all traineeGroups")
	public ResponseEntity<ResponseWrapper> findTraineeGroups(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TraineeGroups in batch of 10 fetched successfully!", TraineeGroupMapper.makeTraineeGroupDTOList(traineeGroupService.findAllTraineeGroups(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{traineeGroupId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single traineeGroup by id")
	public ResponseEntity<ResponseWrapper> findTraineeGroupById(@PathVariable String traineeGroupId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single traineeGroup fetched successfully!", TraineeGroupMapper.makeTraineeGroupDTO(traineeGroupService.findOne(traineeGroupId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an traineeGroup")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTraineeGroup(@Valid @RequestBody TraineeGroupDTO traineeGroupDTO) throws ConstraintsViolationException {
		TraineeGroup traineeGroup = TraineeGroupMapper.makeTraineeGroup(traineeGroupDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TraineeGroup created successfully!", TraineeGroupMapper.makeTraineeGroupDTO(traineeGroupService.create(traineeGroup))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{traineeGroupId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an traineeGroup with the specified id")
	public ResponseEntity<ResponseWrapper> updateTraineeGroup(@Valid @PathVariable String traineeGroupId, @RequestBody TraineeGroupDTO traineeGroupDTO) throws ConstraintsViolationException, EntityNotFoundException {
		TraineeGroup traineeGroup = TraineeGroupMapper.makeTraineeGroup(traineeGroupDTO);
		//Set Id
		traineeGroup.setCode(traineeGroupDTO.getCode());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated traineeGroup with an ID: " + traineeGroupId, TraineeGroupMapper.makeTraineeGroupDTO(traineeGroupService.update(traineeGroup))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{traineeGroupId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an traineeGroup with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTraineeGroup(@Valid @PathVariable String traineeGroupId) throws ConstraintsViolationException, EntityNotFoundException {
		traineeGroupService.delete(traineeGroupId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted traineeGroup with an ID: " + traineeGroupId), HttpStatus.OK);
	}
	
}
