package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TargetMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Target;
import ng.exelon.frankinator2_0.model.TargetDTO;
import ng.exelon.frankinator2_0.service.TargetService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("targets")
//@Api(value = "Targets")
public class TargetController {
	
	@Autowired
	TargetService targetService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all targets")
	public ResponseEntity<ResponseWrapper> findTargets(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Targets in batch of 10 fetched successfully!", TargetMapper.makeTargetDTOList(targetService.findAllTargets(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{targetId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single target by id")
	public ResponseEntity<ResponseWrapper> findTargetById(@PathVariable Long targetId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single target fetched successfully!", TargetMapper.makeTargetDTO(targetService.findOne(targetId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an target")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTarget(@Valid @RequestBody TargetDTO targetDTO) throws ConstraintsViolationException {
		Target target = TargetMapper.makeTarget(targetDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Target created successfully!", TargetMapper.makeTargetDTO(targetService.create(target))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{targetId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an target with the specified id")
	public ResponseEntity<ResponseWrapper> updateTarget(@Valid @PathVariable Long targetId, @RequestBody TargetDTO targetDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Target target = TargetMapper.makeTarget(targetDTO);
		//Set Id
		target.setId(targetDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated target with an ID: " + targetId, TargetMapper.makeTargetDTO(targetService.update(target))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{targetId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an target with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTarget(@Valid @PathVariable Long targetId) throws ConstraintsViolationException, EntityNotFoundException {
		targetService.delete(targetId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted target with an ID: " + targetId), HttpStatus.OK);
	}
	
}
