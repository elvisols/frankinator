package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.LanguageMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Language;
import ng.exelon.frankinator2_0.model.LanguageDTO;
import ng.exelon.frankinator2_0.service.LanguageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("languages")
//@Api(value = "Languages")
public class LanguageController {
	
	@Autowired
	LanguageService languageService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all languages")
	public ResponseEntity<ResponseWrapper> findLanguages(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Languages in batch of 10 fetched successfully!", LanguageMapper.makeLanguageDTOList(languageService.findAllLanguages(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{languageId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single language by id")
	public ResponseEntity<ResponseWrapper> findLanguageById(@PathVariable String languageId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single language fetched successfully!", LanguageMapper.makeLanguageDTO(languageService.findOne(languageId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an language")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addLanguage(@Valid @RequestBody LanguageDTO languageDTO) throws ConstraintsViolationException {
		Language language = LanguageMapper.makeLanguage(languageDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Language created successfully!", LanguageMapper.makeLanguageDTO(languageService.create(language))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{languageId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an language with the specified id")
	public ResponseEntity<ResponseWrapper> updateLanguage(@Valid @PathVariable String languageId, @RequestBody LanguageDTO languageDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Language language = LanguageMapper.makeLanguage(languageDTO);
		//Set Id
		language.setCode(languageDTO.getCode());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated language with an ID: " + languageId, LanguageMapper.makeLanguageDTO(languageService.update(language))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{languageId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an language with a particular id")
	public ResponseEntity<ResponseWrapper> deleteLanguage(@Valid @PathVariable String languageId) throws ConstraintsViolationException, EntityNotFoundException {
		languageService.delete(languageId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted language with an ID: " + languageId), HttpStatus.OK);
	}
	
}
