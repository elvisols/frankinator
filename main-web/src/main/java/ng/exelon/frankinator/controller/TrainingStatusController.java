package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TrainingStatusMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingStatus;
import ng.exelon.frankinator2_0.model.TrainingStatusDTO;
import ng.exelon.frankinator2_0.service.TrainingStatusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("trainingstatuses")
//@Api(value = "TrainingStatus")
public class TrainingStatusController {
	
	@Autowired
	TrainingStatusService trainingStatusService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all trainingStatuss")
	public ResponseEntity<ResponseWrapper> findTrainingStatuss(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TrainingStatuss in batch of 10 fetched successfully!", TrainingStatusMapper.makeTrainingStatusDTOList(trainingStatusService.findAllTrainingStatus(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{trainingStatusId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single trainingStatus by id")
	public ResponseEntity<ResponseWrapper> findTrainingStatusById(@PathVariable Long trainingStatusId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single trainingStatus fetched successfully!", TrainingStatusMapper.makeTrainingStatusDTO(trainingStatusService.findOne(trainingStatusId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an trainingStatus")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTrainingStatus(@Valid @RequestBody TrainingStatusDTO trainingStatusDTO) throws ConstraintsViolationException {
		TrainingStatus trainingStatus = TrainingStatusMapper.makeTrainingStatus(trainingStatusDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TrainingStatus created successfully!", TrainingStatusMapper.makeTrainingStatusDTO(trainingStatusService.create(trainingStatus))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{trainingStatusId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an trainingStatus with the specified id")
	public ResponseEntity<ResponseWrapper> updateTrainingStatus(@Valid @PathVariable Long trainingStatusId, @RequestBody TrainingStatusDTO trainingStatusDTO) throws ConstraintsViolationException, EntityNotFoundException {
		TrainingStatus trainingStatus = TrainingStatusMapper.makeTrainingStatus(trainingStatusDTO);
		//Set Id
		trainingStatus.setId(trainingStatusDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated trainingStatus with an ID: " + trainingStatusId, TrainingStatusMapper.makeTrainingStatusDTO(trainingStatusService.update(trainingStatus))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{trainingStatusId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an trainingStatus with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTrainingStatus(@Valid @PathVariable Long trainingStatusId) throws ConstraintsViolationException, EntityNotFoundException {
		trainingStatusService.delete(trainingStatusId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted trainingStatus with an ID: " + trainingStatusId), HttpStatus.OK);
	}
	
}
