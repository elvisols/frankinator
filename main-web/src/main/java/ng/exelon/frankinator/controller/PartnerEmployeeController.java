package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.PartnerEmployeeMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.PartnerEmployee;
import ng.exelon.frankinator2_0.model.PartnerEmployeeDTO;
import ng.exelon.frankinator2_0.service.PartnerEmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("partneremployees")
//@Api(value = "PartnerEmployees")
public class PartnerEmployeeController {
	
	@Autowired
	PartnerEmployeeService partnerEmployeeService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all partnerEmployees")
	public ResponseEntity<ResponseWrapper> findPartnerEmployees(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "PartnerEmployees in batch of 10 fetched successfully!", PartnerEmployeeMapper.makePartnerEmployeeDTOList(partnerEmployeeService.findAllPartnerEmployees(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{partnerEmployeeId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single partnerEmployee by id")
	public ResponseEntity<ResponseWrapper> findPartnerEmployeeById(@PathVariable Long partnerEmployeeId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single partnerEmployee fetched successfully!", PartnerEmployeeMapper.makePartnerEmployeeDTO(partnerEmployeeService.findOne(partnerEmployeeId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an partnerEmployee")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addPartnerEmployee(@Valid @RequestBody PartnerEmployeeDTO partnerEmployeeDTO) throws ConstraintsViolationException {
		PartnerEmployee partnerEmployee = PartnerEmployeeMapper.makePartnerEmployee(partnerEmployeeDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "PartnerEmployee created successfully!", PartnerEmployeeMapper.makePartnerEmployeeDTO(partnerEmployeeService.create(partnerEmployee))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{partnerEmployeeId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an partnerEmployee with the specified id")
	public ResponseEntity<ResponseWrapper> updatePartnerEmployee(@Valid @PathVariable Long partnerEmployeeId, @RequestBody PartnerEmployeeDTO partnerEmployeeDTO) throws ConstraintsViolationException, EntityNotFoundException {
		PartnerEmployee partnerEmployee = PartnerEmployeeMapper.makePartnerEmployee(partnerEmployeeDTO);
		//Set Id
		partnerEmployee.setId(partnerEmployeeDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated partnerEmployee with an ID: " + partnerEmployeeId, PartnerEmployeeMapper.makePartnerEmployeeDTO(partnerEmployeeService.update(partnerEmployee))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{partnerEmployeeId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an partnerEmployee with a particular id")
	public ResponseEntity<ResponseWrapper> deletePartnerEmployee(@Valid @PathVariable Long partnerEmployeeId) throws ConstraintsViolationException, EntityNotFoundException {
		partnerEmployeeService.delete(partnerEmployeeId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted partnerEmployee with an ID: " + partnerEmployeeId), HttpStatus.OK);
	}
	
}
