package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TrainingMediaMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingMedia;
import ng.exelon.frankinator2_0.model.TrainingMediaDTO;
import ng.exelon.frankinator2_0.service.TrainingMediaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("trainingmedia")
//@Api(value = "TrainingMedia")
public class TrainingMediaController {
	
	@Autowired
	TrainingMediaService trainingMediaService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all trainingMedias")
	public ResponseEntity<ResponseWrapper> findTrainingMedias(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TrainingMedias in batch of 10 fetched successfully!", TrainingMediaMapper.makeTrainingMediaDTOList(trainingMediaService.findAllTrainingMedias(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{trainingMediaId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single trainingMedia by id")
	public ResponseEntity<ResponseWrapper> findTrainingMediaById(@PathVariable Long trainingMediaId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single trainingMedia fetched successfully!", TrainingMediaMapper.makeTrainingMediaDTO(trainingMediaService.findOne(trainingMediaId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an trainingMedia")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTrainingMedia(@Valid @RequestBody TrainingMediaDTO trainingMediaDTO) throws ConstraintsViolationException {
		TrainingMedia trainingMedia = TrainingMediaMapper.makeTrainingMedia(trainingMediaDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TrainingMedia created successfully!", TrainingMediaMapper.makeTrainingMediaDTO(trainingMediaService.create(trainingMedia))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{trainingMediaId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an trainingMedia with the specified id")
	public ResponseEntity<ResponseWrapper> updateTrainingMedia(@Valid @PathVariable Long trainingMediaId, @RequestBody TrainingMediaDTO trainingMediaDTO) throws ConstraintsViolationException, EntityNotFoundException {
		TrainingMedia trainingMedia = TrainingMediaMapper.makeTrainingMedia(trainingMediaDTO);
		//Set Id
		trainingMedia.setId(trainingMediaDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated trainingMedia with an ID: " + trainingMediaId, TrainingMediaMapper.makeTrainingMediaDTO(trainingMediaService.update(trainingMedia))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{trainingMediaId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an trainingMedia with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTrainingMedia(@Valid @PathVariable Long trainingMediaId) throws ConstraintsViolationException, EntityNotFoundException {
		trainingMediaService.delete(trainingMediaId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted trainingMedia with an ID: " + trainingMediaId), HttpStatus.OK);
	}
	
}
