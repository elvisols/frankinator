package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.AssessorMapper;
import ng.exelon.frankinator.mapper.KpiItemMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.AssessorDTO;
import ng.exelon.frankinator2_0.model.KpiItem;
import ng.exelon.frankinator2_0.model.KpiItemDTO;
import ng.exelon.frankinator2_0.service.AssessorService;
import ng.exelon.frankinator2_0.service.KpiItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("kpitems")
//@Api(value = "KpiItems")
public class KpiItemController {
	
	@Autowired
	KpiItemService kpiItemService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all kpiItems")
	public ResponseEntity<ResponseWrapper> findKpiItems(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "KpiItems in batch of 10 fetched successfully!", KpiItemMapper.makeKpiItemDTOList(kpiItemService.findAllKpiItems(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{kpiItemId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single kpiItem by id")
	public ResponseEntity<ResponseWrapper> findKpiItemById(@PathVariable Long kpiItemId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single kpiItem fetched successfully!", KpiItemMapper.makeKpiItemDTO(kpiItemService.findOne(kpiItemId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an kpiItem")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addKpiItem(@Valid @RequestBody KpiItemDTO kpiItemDTO) throws ConstraintsViolationException {
		KpiItem kpiItem = KpiItemMapper.makeKpiItem(kpiItemDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "KpiItem created successfully!", KpiItemMapper.makeKpiItemDTO(kpiItemService.create(kpiItem))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{kpiItemId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an kpiItem with the specified id")
	public ResponseEntity<ResponseWrapper> updateKpiItem(@Valid @PathVariable Long kpiItemId, @RequestBody KpiItemDTO kpiItemDTO) throws ConstraintsViolationException, EntityNotFoundException {
		KpiItem kpiItem = KpiItemMapper.makeKpiItem(kpiItemDTO);
		//Set Id
		kpiItem.setId(kpiItemDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated kpiItem with an ID: " + kpiItemId, KpiItemMapper.makeKpiItemDTO(kpiItemService.update(kpiItem))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{kpiItemId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an kpiItem with a particular id")
	public ResponseEntity<ResponseWrapper> deleteKpiItem(@Valid @PathVariable Long kpiItemId) throws ConstraintsViolationException, EntityNotFoundException {
		kpiItemService.delete(kpiItemId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted kpiItem with an ID: " + kpiItemId), HttpStatus.OK);
	}
	
}
