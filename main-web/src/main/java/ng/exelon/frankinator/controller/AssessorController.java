package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.AssessorMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.AssessorDTO;
import ng.exelon.frankinator2_0.service.AssessorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("assessors")
//@Api(value = "Assessors")
public class AssessorController {
	
	@Autowired
	AssessorService assessorService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all assessors")
	public ResponseEntity<ResponseWrapper> findAssessors(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Assessors in batch of 10 fetched successfully!", AssessorMapper.makeAssessorDTOList(assessorService.findAllAssessors(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{assessorId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single assessor by id")
	public ResponseEntity<ResponseWrapper> findAssessorById(@PathVariable Long assessorId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single assessor fetched successfully!", AssessorMapper.makeAssessorDTO(assessorService.findOne(assessorId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an assessor")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addAssessor(@Valid @RequestBody AssessorDTO assessorDTO) throws ConstraintsViolationException {
		Assessor assessor = AssessorMapper.makeAssessor(assessorDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Assessor created successfully!", AssessorMapper.makeAssessorDTO(assessorService.create(assessor))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{assessorId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an assessor with the specified id")
	public ResponseEntity<ResponseWrapper> updateAssessor(@Valid @PathVariable Long assessorId, @RequestBody AssessorDTO assessorDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Assessor assessor = AssessorMapper.makeAssessor(assessorDTO);
		//Set Id
		assessor.setId(assessorDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated assessor with an ID: " + assessorId, AssessorMapper.makeAssessorDTO(assessorService.update(assessor))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{assessorId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an assessor with a particular id")
	public ResponseEntity<ResponseWrapper> deleteAssessor(@Valid @PathVariable Long assessorId) throws ConstraintsViolationException, EntityNotFoundException {
		assessorService.delete(assessorId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted assessor with an ID: " + assessorId), HttpStatus.OK);
	}
	
}
