package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.AssessorMapper;
import ng.exelon.frankinator.mapper.KpiCategoryMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.AssessorDTO;
import ng.exelon.frankinator2_0.model.KpiCategory;
import ng.exelon.frankinator2_0.model.KpiCategoryDTO;
import ng.exelon.frankinator2_0.service.AssessorService;
import ng.exelon.frankinator2_0.service.KpiCategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("kpicategories")
//@Api(value = "KpiCategories")
public class KpiCategoryController {
	
	@Autowired
	KpiCategoryService kpiCategoryService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all kpiCategorys")
	public ResponseEntity<ResponseWrapper> findKpiCategorys(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "KpiCategorys in batch of 10 fetched successfully!", KpiCategoryMapper.makeKpiCategoryDTOList(kpiCategoryService.findAllKpiCategorys(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{kpiCategoryId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single kpiCategory by id")
	public ResponseEntity<ResponseWrapper> findKpiCategoryById(@PathVariable String kpiCategoryId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single kpiCategory fetched successfully!", KpiCategoryMapper.makeKpiCategoryDTO(kpiCategoryService.findOne(kpiCategoryId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an kpiCategory")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addKpiCategory(@Valid @RequestBody KpiCategoryDTO kpiCategoryDTO) throws ConstraintsViolationException {
		KpiCategory kpiCategory = KpiCategoryMapper.makeKpiCategory(kpiCategoryDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "KpiCategory created successfully!", KpiCategoryMapper.makeKpiCategoryDTO(kpiCategoryService.create(kpiCategory))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{kpiCategoryId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an kpiCategory with the specified id")
	public ResponseEntity<ResponseWrapper> updateKpiCategory(@Valid @PathVariable Long kpiCategoryId, @RequestBody KpiCategoryDTO kpiCategoryDTO) throws ConstraintsViolationException, EntityNotFoundException {
		KpiCategory kpiCategory = KpiCategoryMapper.makeKpiCategory(kpiCategoryDTO);
		//Set Id
		kpiCategory.setId(kpiCategoryDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated kpiCategory with an ID: " + kpiCategoryId, KpiCategoryMapper.makeKpiCategoryDTO(kpiCategoryService.update(kpiCategory))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{kpiCategoryId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an kpiCategory with a particular id")
	public ResponseEntity<ResponseWrapper> deleteKpiCategory(@Valid @PathVariable String kpiCategoryId) throws ConstraintsViolationException, EntityNotFoundException {
		kpiCategoryService.delete(kpiCategoryId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted kpiCategory with an ID: " + kpiCategoryId), HttpStatus.OK);
	}
	
}
