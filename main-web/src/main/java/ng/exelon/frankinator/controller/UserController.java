package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.UserMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.User;
import ng.exelon.frankinator2_0.model.UserDTO;
import ng.exelon.frankinator2_0.model.value.RoleEnum;
import ng.exelon.frankinator2_0.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("users")
//@Api(value = "Users")
public class UserController {
	
	@Autowired
	UserService userService;
	
//	@GetMapping(params="format=json", produces=MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
//	@PreAuthorize("#oauth2.hasScope('foo') and hasRole('ADMIN')")
	@ApiOperation(value = "Get all users")
	public List<UserDTO> findUsers(@RequestParam(required = false) Integer page) {
		return UserMapper.makeUserDTOList(userService.findAllUsers(page));
	}
	
	@GetMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single user by id")
	public UserDTO findUserById(@PathVariable Long userId) throws EntityNotFoundException {
		return UserMapper.makeUserDTO(userService.findOne(userId));
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create a user")
	@ResponseStatus(HttpStatus.CREATED)
	public UserDTO addUser(@Valid @RequestBody UserDTO userDTO) throws ConstraintsViolationException {
		User user = UserMapper.makeUser(userDTO);
		return UserMapper.makeUserDTO(userService.create(user));
	}
	
	@PutMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update a user with the specified id")
	public UserDTO updateUser(@Valid @PathVariable long userId, @RequestBody UserDTO userDTO) throws ConstraintsViolationException, EntityNotFoundException {
//		userService.update(userId);
		return null;
	}
	
	@DeleteMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete a user with a particular id")
	public UserDTO deleteUser(@Valid @PathVariable long userId) throws ConstraintsViolationException, EntityNotFoundException {
		userService.delete(userId);
		return null;
	}
	
	@GetMapping(value="/byroles",produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get all users to a role")
	public List<UserDTO> getRoleUsers(@RequestParam RoleEnum role) throws ConstraintsViolationException, EntityNotFoundException {
		return UserMapper.makeUserDTOList(userService.findRoleUsers(role));
	}
	
//	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET, headers="Accept=application/json")
//	@PreAuthorize("hasRole('ADMIN')")
//	@PreAuthorize("#oauth2.hasScope('foo') and hasRole('ADMIN')")
//	public ResponseEntity<Map<String, Object>> listAllUser(UserDTO userDTO) {
//		Map<String, Object> msg = new HashMap<>();
//		msg.put("message", message);
//		return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
//	}
	
}
