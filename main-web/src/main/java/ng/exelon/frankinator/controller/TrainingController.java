package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TrainingMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Training;
import ng.exelon.frankinator2_0.model.TrainingDTO;
import ng.exelon.frankinator2_0.service.TrainingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("trainings")
//@Api(value = "Trainings")
public class TrainingController {
	
	@Autowired
	TrainingService trainingService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all trainings")
	public ResponseEntity<ResponseWrapper> findTrainings(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Trainings in batch of 10 fetched successfully!", TrainingMapper.makeTrainingDTOList(trainingService.findAllTrainings(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{trainingId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single training by id")
	public ResponseEntity<ResponseWrapper> findTrainingById(@PathVariable Long trainingId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single training fetched successfully!", TrainingMapper.makeTrainingDTO(trainingService.findOne(trainingId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an training")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTraining(@Valid @RequestBody TrainingDTO trainingDTO) throws ConstraintsViolationException {
		Training training = TrainingMapper.makeTraining(trainingDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Training created successfully!", TrainingMapper.makeTrainingDTO(trainingService.create(training))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{trainingId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an training with the specified id")
	public ResponseEntity<ResponseWrapper> updateTraining(@Valid @PathVariable Long trainingId, @RequestBody TrainingDTO trainingDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Training training = TrainingMapper.makeTraining(trainingDTO);
		//Set Id
		training.setId(trainingDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated training with an ID: " + trainingId, TrainingMapper.makeTrainingDTO(trainingService.update(training))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{trainingId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an training with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTraining(@Valid @PathVariable Long trainingId) throws ConstraintsViolationException, EntityNotFoundException {
		trainingService.delete(trainingId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted training with an ID: " + trainingId), HttpStatus.OK);
	}
	
}
