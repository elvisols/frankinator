package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TraineeTierMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TraineeTier;
import ng.exelon.frankinator2_0.model.TraineeTierDTO;
import ng.exelon.frankinator2_0.service.TraineeTierService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("traineetiers")
//@Api(value = "TraineeTiers")
public class TraineeTierController {
	
	@Autowired
	TraineeTierService traineeTierService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all traineeTiers")
	public ResponseEntity<ResponseWrapper> findTraineeTiers(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TraineeTiers in batch of 10 fetched successfully!", TraineeTierMapper.makeTraineeTierDTOList(traineeTierService.findAllTraineeTiers(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{traineeTierId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single traineeTier by id")
	public ResponseEntity<ResponseWrapper> findTraineeTierById(@PathVariable String traineeTierId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single traineeTier fetched successfully!", TraineeTierMapper.makeTraineeTierDTO(traineeTierService.findOne(traineeTierId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an traineeTier")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTraineeTier(@Valid @RequestBody TraineeTierDTO traineeTierDTO) throws ConstraintsViolationException {
		TraineeTier traineeTier = TraineeTierMapper.makeTraineeTier(traineeTierDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "TraineeTier created successfully!", TraineeTierMapper.makeTraineeTierDTO(traineeTierService.create(traineeTier))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{traineeTierId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an traineeTier with the specified id")
	public ResponseEntity<ResponseWrapper> updateTraineeTier(@Valid @PathVariable String traineeTierId, @RequestBody TraineeTierDTO traineeTierDTO) throws ConstraintsViolationException, EntityNotFoundException {
		TraineeTier traineeTier = TraineeTierMapper.makeTraineeTier(traineeTierDTO);
		//Set Id
		traineeTier.setCode(traineeTierDTO.getCode());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated traineeTier with an ID: " + traineeTierId, TraineeTierMapper.makeTraineeTierDTO(traineeTierService.update(traineeTier))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{traineeTierId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an traineeTier with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTraineeTier(@Valid @PathVariable String traineeTierId) throws ConstraintsViolationException, EntityNotFoundException {
		traineeTierService.delete(traineeTierId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted traineeTier with an ID: " + traineeTierId), HttpStatus.OK);
	}
	
}
