package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.StateMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.State;
import ng.exelon.frankinator2_0.model.StateDTO;
import ng.exelon.frankinator2_0.service.StateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("states")
//@Api(value = "States")
public class StateController {
	
	@Autowired
	StateService stateService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all states")
	public ResponseEntity<ResponseWrapper> findStates(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "States in batch of 10 fetched successfully!", StateMapper.makeStateDTOList(stateService.findAllStates(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{stateId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single state by id")
	public ResponseEntity<ResponseWrapper> findStateById(@PathVariable Long stateId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single state fetched successfully!", StateMapper.makeStateDTO(stateService.findOne(stateId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an state")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addState(@Valid @RequestBody StateDTO stateDTO) throws ConstraintsViolationException {
		State state = StateMapper.makeState(stateDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "State created successfully!", StateMapper.makeStateDTO(stateService.create(state))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{stateId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an state with the specified id")
	public ResponseEntity<ResponseWrapper> updateState(@Valid @PathVariable Long stateId, @RequestBody StateDTO stateDTO) throws ConstraintsViolationException, EntityNotFoundException {
		State state = StateMapper.makeState(stateDTO);
		//Set Id
		state.setId(stateDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated state with an ID: " + stateId, StateMapper.makeStateDTO(stateService.update(state))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{stateId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an state with a particular id")
	public ResponseEntity<ResponseWrapper> deleteState(@Valid @PathVariable Long stateId) throws ConstraintsViolationException, EntityNotFoundException {
		stateService.delete(stateId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted state with an ID: " + stateId), HttpStatus.OK);
	}
	
}
