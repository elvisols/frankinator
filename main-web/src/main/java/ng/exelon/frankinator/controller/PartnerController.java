package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.PartnerMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.PartnerDTO;
import ng.exelon.frankinator2_0.service.PartnerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("partners")
//@Api(value = "Partners")
public class PartnerController {
	
	@Autowired
	PartnerService partnerService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all partners")
	public ResponseEntity<ResponseWrapper> findPartners(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Partners in batch of 10 fetched successfully!", PartnerMapper.makePartnerDTOList(partnerService.findAllPartners(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{partnerId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single partner by id")
	public ResponseEntity<ResponseWrapper> findPartnerById(@PathVariable Long partnerId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single partner fetched successfully!", PartnerMapper.makePartnerDTO(partnerService.findOne(partnerId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an partner")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addPartner(@Valid @RequestBody PartnerDTO partnerDTO) throws ConstraintsViolationException {
		Partner partner = PartnerMapper.makePartner(partnerDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Partner created successfully!", PartnerMapper.makePartnerDTO(partnerService.create(partner))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{partnerId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an partner with the specified id")
	public ResponseEntity<ResponseWrapper> updatePartner(@Valid @PathVariable Long partnerId, @RequestBody PartnerDTO partnerDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Partner partner = PartnerMapper.makePartner(partnerDTO);
		//Set Id
		partner.setId(partnerDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated partner with an ID: " + partnerId, PartnerMapper.makePartnerDTO(partnerService.update(partner))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{partnerId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an partner with a particular id")
	public ResponseEntity<ResponseWrapper> deletePartner(@Valid @PathVariable Long partnerId) throws ConstraintsViolationException, EntityNotFoundException {
		partnerService.delete(partnerId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted partner with an ID: " + partnerId), HttpStatus.OK);
	}
	
}
