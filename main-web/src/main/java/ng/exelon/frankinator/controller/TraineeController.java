package ng.exelon.frankinator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.validation.Valid;

import ng.exelon.frankinator.mapper.TraineeMapper;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.TraineeDTO;
import ng.exelon.frankinator2_0.service.TraineeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("trainees")
//@Api(value = "Trainees")
public class TraineeController {
	
	@Autowired
	TraineeService traineeService;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Get all trainees")
	public ResponseEntity<ResponseWrapper> findTrainees(@RequestParam(required = false) Integer page) {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Trainees in batch of 10 fetched successfully!", TraineeMapper.makeTraineeDTOList(traineeService.findAllTrainees(page))), 
			HttpStatus.OK
		);
	}
	
	@GetMapping(value="/{traineeId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Get a single trainee by id")
	public ResponseEntity<ResponseWrapper> findTraineeById(@PathVariable Long traineeId) throws EntityNotFoundException {
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Single trainee fetched successfully!", TraineeMapper.makeTraineeDTO(traineeService.findOne(traineeId))), 
			HttpStatus.OK
		);
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Create an trainee")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseWrapper> addTrainee(@Valid @RequestBody TraineeDTO traineeDTO) throws ConstraintsViolationException {
		Trainee trainee = TraineeMapper.makeTrainee(traineeDTO);
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Trainee created successfully!", TraineeMapper.makeTraineeDTO(traineeService.create(trainee))), 
			HttpStatus.CREATED
		);
	}
	
	@PutMapping(value="/{traineeId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@ApiOperation(value = "Update an trainee with the specified id")
	public ResponseEntity<ResponseWrapper> updateTrainee(@Valid @PathVariable Long traineeId, @RequestBody TraineeDTO traineeDTO) throws ConstraintsViolationException, EntityNotFoundException {
		Trainee trainee = TraineeMapper.makeTrainee(traineeDTO);
		//Set Id
		trainee.setId(traineeDTO.getId());
		return new ResponseEntity<ResponseWrapper>(
			new ResponseWrapper(HttpStatus.OK.value(), "Updated trainee with an ID: " + traineeId, TraineeMapper.makeTraineeDTO(traineeService.update(trainee))), 
			HttpStatus.OK
		);
	}
	
	@DeleteMapping(value="/{traineeId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Delete an trainee with a particular id")
	public ResponseEntity<ResponseWrapper> deleteTrainee(@Valid @PathVariable Long traineeId) throws ConstraintsViolationException, EntityNotFoundException {
		traineeService.delete(traineeId);
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted trainee with an ID: " + traineeId), HttpStatus.OK);
	}
	
}
