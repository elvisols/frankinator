package ng.exelon.frankinator.config;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import org.joda.time.DateTime;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

public class CustomTokenEnhancer implements TokenEnhancer {
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        System.out.println("Authentication name is " + authentication.getName());
        System.out.println("Authorities " + authentication.getAuthorities());
        additionalInfo.put("client_reference", randomAlphabetic(4));
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        ((DefaultOAuth2AccessToken) accessToken).setExpiration(this.getDate());;
        return accessToken;
    }
    
    private Date getDate() {
    	DateTime today = new DateTime();
		java.util.Date n_date = new Date(today.plusMinutes(60).getMillis());
		System.out.println("Next date is " + n_date);
	    return n_date;
    }

}
