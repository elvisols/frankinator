package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface PartnerService {
	
	Partner findOne(Long id) throws EntityNotFoundException;
	    
    Partner create(Partner Partner) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Partner update(Partner partner) throws EntityNotFoundException;
    
    List<Partner> findAllPartners(Integer page);

}
