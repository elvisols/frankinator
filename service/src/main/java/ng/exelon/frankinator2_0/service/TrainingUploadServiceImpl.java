package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TrainingUploadRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingUpload;
import ng.exelon.frankinator2_0.model.TrainingUpload;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingUploadServiceImpl implements TrainingUploadService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TrainingUploadServiceImpl.class);

    @Autowired
    private TrainingUploadRepository trainingUploadRepository; 

	public TrainingUpload findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting TrainingUpload with ID ", id);
		return trainingUploadRepository.findById(id);
	}

	public TrainingUpload create(TrainingUpload TrainingUpload) throws ConstraintsViolationException {
		LOG.warn("Saving TrainingUpload ", TrainingUpload);
		return trainingUploadRepository.save(TrainingUpload);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting TrainingUpload with ID ", id);
		trainingUploadRepository.deleteById(id);
	}

	public TrainingUpload update(TrainingUpload trainingUpload) throws EntityNotFoundException {
		TrainingUpload tup = trainingUploadRepository.findById(trainingUpload.getId());
		// Throw exception for missing TrainingUpload ID
		if(tup == null) throw new EntityNotFoundException("Not found TrainingUpload with ID: " + trainingUpload.getId());
		// Update trainingUpload's records accordingly
		tup.setActualAttendees(trainingUpload.getActualAttendees() == null ? tup.getActualAttendees() : trainingUpload.getActualAttendees());
		tup.setMedia(trainingUpload.getMedia() == null ? tup.getMedia() : trainingUpload.getMedia());
		tup.setPartner(trainingUpload.getPartner() == null ? tup.getPartner() : trainingUpload.getPartner());
		tup.setTrainees(trainingUpload.getTrainees() == null ? tup.getTrainees() : trainingUpload.getTrainees());
		tup.setTrainer(trainingUpload.getTrainer() == null ? tup.getTrainer() : trainingUpload.getTrainer());
		LOG.warn("updating TrainingUpload with ID " + trainingUpload.getId());
		return trainingUploadRepository.save(tup);
	}
	
	public List<TrainingUpload> findAllTrainingUploads(Integer page) {
		return (List<TrainingUpload>) trainingUploadRepository.findAll(page);
	}
    
}
