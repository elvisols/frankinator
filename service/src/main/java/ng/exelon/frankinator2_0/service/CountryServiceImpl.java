package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.CountryRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImpl implements CountryService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(CountryServiceImpl.class);

    @Autowired
    private CountryRepository countryRepository; 

	public Country findOne(String id) throws EntityNotFoundException {
		LOG.warn("Getting Country with ID ", id);
		return countryRepository.findById(id);
	}

	public Country create(Country country) throws ConstraintsViolationException {
		LOG.warn("Saving Country ", country);
		return countryRepository.save(country);
	}

	public void delete(String id) throws EntityNotFoundException {
		LOG.warn("Deleting Country with ID ", id);
		countryRepository.deleteById(id);
	}

	public Country update(Country country) throws EntityNotFoundException {
		Country c = countryRepository.findById(country.getCode());
		// Throw exception for missing Country ID
		if(c == null) throw new EntityNotFoundException("Not found Country with ID: " + country.getCode());
		// Update country's records accordingly
		// Todo ...
		LOG.warn("updating Country with ID " + country.getCode());
		return countryRepository.save(country);
	}
	
	public List<Country> findAllCountrys(Integer page) {
		return (List<Country>) countryRepository.findAll(page);
	}
    
}
