package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TraineeService {
	
	Trainee findOne(Long id) throws EntityNotFoundException;
	    
    Trainee create(Trainee Trainee) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Trainee update(Trainee trainee) throws EntityNotFoundException;
    
    List<Trainee> findAllTrainees(Integer page);

}
