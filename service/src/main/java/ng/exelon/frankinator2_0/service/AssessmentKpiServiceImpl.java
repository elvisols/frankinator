package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.AssessmentKpiRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.AssessmentKpi;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssessmentKpiServiceImpl implements AssessmentKpiService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(AssessmentKpiServiceImpl.class);

    @Autowired
    private AssessmentKpiRepository assessmentKpiRepository; 

	public AssessmentKpi findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting AssessmentKpi with ID ", id);
		return assessmentKpiRepository.findById(id);
	}

	public AssessmentKpi create(AssessmentKpi AssessmentKpi) throws ConstraintsViolationException {
		LOG.warn("Saving AssessmentKpi ", AssessmentKpi);
		return assessmentKpiRepository.save(AssessmentKpi);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting AssessmentKpi with ID ", id);
		assessmentKpiRepository.deleteById(id);
	}

	public AssessmentKpi update(AssessmentKpi assessmentKpi) throws EntityNotFoundException {
		AssessmentKpi a = assessmentKpiRepository.findById(assessmentKpi.getId());
		// Throw exception for missing AssessmentKpi ID
		if(a == null) throw new EntityNotFoundException("Not found AssessmentKpi with ID: " + assessmentKpi.getId());
		// Update assessmentKpi's records accordingly
		a.setAssessment(assessmentKpi.getAssessment() == null ? a.getAssessment() : assessmentKpi.getAssessment());
		a.setKpi(assessmentKpi.getKpi() == null ? a.getKpi() : assessmentKpi.getKpi());
		a.setValue(assessmentKpi.getValue() == null ? a.getValue() : assessmentKpi.getValue());
		LOG.warn("updating AssessmentKpi with ID " + assessmentKpi.getId());
		return assessmentKpiRepository.save(a);
	}
	
	public List<AssessmentKpi> findAllAssessmentKpis(Integer page) {
		return (List<AssessmentKpi>) assessmentKpiRepository.findAll(page);
	}
    
}
