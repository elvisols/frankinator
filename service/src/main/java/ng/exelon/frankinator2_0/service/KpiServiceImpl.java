package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.KpiRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Kpi;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KpiServiceImpl implements KpiService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(KpiServiceImpl.class);

    @Autowired
    private KpiRepository kpiRepository; 

	public Kpi findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Kpi with ID ", id);
		return kpiRepository.findById(id);
	}

	public Kpi create(Kpi Kpi) throws ConstraintsViolationException {
		LOG.warn("Saving Kpi ", Kpi);
		return kpiRepository.save(Kpi);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Kpi with ID ", id);
		kpiRepository.deleteById(id);
	}

	public Kpi update(Kpi kpi) throws EntityNotFoundException {
		Kpi k = kpiRepository.findById(kpi.getId());
		// Throw exception for missing Kpi ID
		if(k == null) throw new EntityNotFoundException("Not found Kpi with ID: " + kpi.getId());
		// Update kpi's records accordingly
		k.setCategory(kpi.getCategory() == null ? k.getCategory() : kpi.getCategory());
		k.setDescription(kpi.getDescription() == null ? k.getDescription() : kpi.getDescription());
		k.setInputType(kpi.getInputType() == null ? k.getInputType() : kpi.getInputType());
		k.setName(kpi.getName() == null ? k.getName() : kpi.getName());
		// Todo ...
		LOG.warn("updating Kpi with ID " + kpi.getId());
		return kpiRepository.save(k);
	}
	
	public List<Kpi> findAllKpis(Integer page) {
		return (List<Kpi>) kpiRepository.findAll(page);
	}
    
}
