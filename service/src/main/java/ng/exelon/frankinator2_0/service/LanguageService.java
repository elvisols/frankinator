package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Language;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface LanguageService {
	
	Language findOne(String id) throws EntityNotFoundException;
	    
    Language create(Language Language) throws ConstraintsViolationException;

    void delete(String id) throws EntityNotFoundException;

    Language update(Language language) throws EntityNotFoundException;
    
    List<Language> findAllLanguages(Integer page);

}
