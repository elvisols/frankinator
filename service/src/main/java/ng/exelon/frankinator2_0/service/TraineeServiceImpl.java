package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TraineeRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.Trainee;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TraineeServiceImpl implements TraineeService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TraineeServiceImpl.class);

    @Autowired
    private TraineeRepository traineeRepository; 

	public Trainee findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Trainee with ID ", id);
		return traineeRepository.findById(id);
	}

	public Trainee create(Trainee Trainee) throws ConstraintsViolationException {
		LOG.warn("Saving Trainee ", Trainee);
		return traineeRepository.save(Trainee);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Trainee with ID ", id);
		traineeRepository.deleteById(id);
	}

	public Trainee update(Trainee trainee) throws EntityNotFoundException {
		Trainee t = traineeRepository.findById(trainee.getId());
		// Throw exception for missing Trainee ID
		if(t == null) throw new EntityNotFoundException("Not found Trainee with ID: " + trainee.getId());
		// Update trainee's records accordingly
		t.setFirstName(trainee.getFirstName() == null ? t.getFirstName() : trainee.getFirstName());
		t.setMiddleName(trainee.getMiddleName() == null ? t.getMiddleName() : trainee.getMiddleName());
		t.setLastName(trainee.getLastName() == null ? t.getLastName() : trainee.getLastName());
		t.setSex(trainee.getSex() == null ? t.getSex() : trainee.getSex());
		t.setDateOfBirth(trainee.getDateOfBirth() == null ? t.getDateOfBirth() : trainee.getDateOfBirth());
		t.setMaritalStatus(trainee.getMaritalStatus() == null ? t.getMaritalStatus() : trainee.getMaritalStatus());
		t.setMobileNumber(trainee.getMobileNumber() == null ? t.getMobileNumber() : trainee.getMobileNumber());
		t.setEmail(trainee.getEmail() == null ? t.getEmail() : trainee.getEmail());
		t.setBirthCountry(trainee.getBirthCountry() == null ? t.getBirthCountry() : trainee.getBirthCountry());
		t.setResidentCountry(trainee.getResidentCountry() == null ? t.getResidentCountry() : trainee.getResidentCountry());
		t.setTraining(trainee.getTraining() == null ? t.getTraining() : trainee.getTraining());
		LOG.warn("updating Trainee with ID " + trainee.getId());
		return traineeRepository.save(t);
	}
	
	public List<Trainee> findAllTrainees(Integer page) {
		return (List<Trainee>) traineeRepository.findAll(page);
	}
    
}
