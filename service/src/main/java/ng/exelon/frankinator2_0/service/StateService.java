package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.State;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface StateService {
	
	State findOne(Long id) throws EntityNotFoundException;
	    
    State create(State State) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    State update(State state) throws EntityNotFoundException;
    
    List<State> findAllStates(Integer page);

}
