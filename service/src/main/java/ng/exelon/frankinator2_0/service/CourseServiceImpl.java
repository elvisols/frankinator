package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.CourseRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.CourseTierDeleted;
import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.value.RoleEnum;
import ng.exelon.frankinator2_0.model.value.TierEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(CourseServiceImpl.class);

    @Autowired
    private CourseRepository courseRepository; 

	public Course findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Course with ID ", id);
		return courseRepository.findById(id);
	}

	public Course create(Course Course) throws ConstraintsViolationException {
		LOG.warn("Saving Course ", Course);
		return courseRepository.save(Course);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Course with ID ", id);
		courseRepository.deleteById(id);
	}

	public Course update(Course course) throws EntityNotFoundException {
		Course c = courseRepository.findById(course.getId());
		// Throw exception for missing Course ID
		if(c == null) throw new EntityNotFoundException("Not found Course with ID: " + course.getId());
		// Update course's records accordingly
		c.setDateAdded(course.getDateAdded() == null ? c.getDateAdded() : course.getDateAdded());
		c.setDescription(course.getDescription() == null ? c.getDescription() : course.getDescription());
		c.setName(course.getName() == null ? c.getName() : course.getName());
		LOG.warn("updating Course with ID ", course.getId());
		return courseRepository.save(c);
	}

	public List<Course> findCoursesByTier(TierEnum te) {
		return (List<Course>) courseRepository.findCoursesByTier(te);
	}
	
	public List<Course> findAllCourses() {
		return (List<Course>) courseRepository.findAll();
	}
    
}
