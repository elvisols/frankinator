package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.KpiItemRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.KpiItem;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KpiItemServiceImpl implements KpiItemService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(KpiItemServiceImpl.class);

    @Autowired
    private KpiItemRepository kpiItemRepository; 

	public KpiItem findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting KpiItem with ID ", id);
		return kpiItemRepository.findById(id);
	}

	public KpiItem create(KpiItem KpiItem) throws ConstraintsViolationException {
		LOG.warn("Saving KpiItem ", KpiItem);
		return kpiItemRepository.save(KpiItem);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting KpiItem with ID ", id);
		kpiItemRepository.deleteById(id);
	}

	public KpiItem update(KpiItem kpiItem) throws EntityNotFoundException {
		KpiItem c = kpiItemRepository.findById(kpiItem.getId());
		// Throw exception for missing KpiItem ID
		if(c == null) throw new EntityNotFoundException("Not found KpiItem with ID: " + kpiItem.getId());
		// Update kpiItem's records accordingly
		c.setName(kpiItem.getName() == null ? c.getName() : kpiItem.getName());
		c.setDescription(kpiItem.getDescription() == null ? c.getDescription() : kpiItem.getDescription());
		c.setWeight(kpiItem.getWeight() == null ? c.getWeight() : kpiItem.getWeight());
		c.setKpi(kpiItem.getKpi() == null ? c.getKpi() : kpiItem.getKpi());
		// Todo ...
		LOG.warn("updating KpiItem with ID " + kpiItem.getId());
		return kpiItemRepository.save(c);
	}
	
	public List<KpiItem> findAllKpiItems(Integer page) {
		return (List<KpiItem>) kpiItemRepository.findAll(page);
	}
    
}
