package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.User;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface UserService {
	
	User findOne(Long id) throws EntityNotFoundException;
	    
    User create(User user) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    User update(User user) throws EntityNotFoundException;
    
    List<User> findRoleUsers(RoleEnum role);
    
    List<User> findAllUsers(Integer page);

}
