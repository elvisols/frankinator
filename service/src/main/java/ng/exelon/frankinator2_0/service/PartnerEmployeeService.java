package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.PartnerEmployee;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface PartnerEmployeeService {
	
	PartnerEmployee findOne(Long id) throws EntityNotFoundException;
	    
    PartnerEmployee create(PartnerEmployee PartnerEmployee) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    PartnerEmployee update(PartnerEmployee pe) throws EntityNotFoundException;
    
    List<PartnerEmployee> findAllPartnerEmployees(Integer page);

}
