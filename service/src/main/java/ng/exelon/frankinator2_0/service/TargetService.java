package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Target;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TargetService {
	
	Target findOne(Long id) throws EntityNotFoundException;
	    
    Target create(Target Target) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Target update(Target target) throws EntityNotFoundException;
    
    List<Target> findAllTargets(Integer page);

}
