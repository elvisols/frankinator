package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.AssessorRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssessorServiceImpl implements AssessorService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(AssessorServiceImpl.class);

    @Autowired
    private AssessorRepository assessorRepository; 

	public Assessor findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Assessor with ID ", id);
		return assessorRepository.findById(id);
	}

	public Assessor create(Assessor assessor) throws ConstraintsViolationException {
		LOG.warn("Saving Assessor ", assessor);
		return assessorRepository.save(assessor);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Assessor with ID ", id);
		assessorRepository.deleteById(id);
	}

	public Assessor update(Assessor assessor) throws EntityNotFoundException {
		Assessor a = assessorRepository.findById(assessor.getId());
		// Throw exception for missing Assessor ID
		if(a == null) throw new EntityNotFoundException("Not found Assessor with ID: " + assessor.getId());
		// Update assessor's records accordingly
		a.setFirstName(assessor.getFirstName() == null ? a.getFirstName() : assessor.getFirstName());
		a.setMiddleName(assessor.getMiddleName() == null ? a.getMiddleName() : assessor.getMiddleName());
		a.setLastName(assessor.getLastName() == null ? a.getLastName() : assessor.getLastName());
		a.setSex(assessor.getSex() == null ? a.getSex() : assessor.getSex());
		a.setDateOfBirth(assessor.getDateOfBirth() == null ? a.getDateOfBirth() : assessor.getDateOfBirth());
		a.setMaritalStatus(assessor.getMaritalStatus() == null ? a.getMaritalStatus() : assessor.getMaritalStatus());
		a.setMobileNumber(assessor.getMobileNumber() == null ? a.getMobileNumber() : assessor.getMobileNumber());
		a.setEmail(assessor.getEmail() == null ? a.getEmail() : assessor.getEmail());
		a.setBirthCountry(assessor.getBirthCountry() == null ? a.getBirthCountry() : assessor.getBirthCountry());
		a.setResidentCountry(assessor.getResidentCountry() == null ? a.getResidentCountry() : assessor.getResidentCountry());
		LOG.warn("updating Assessor with ID " + assessor.getId());
		return assessorRepository.save(a);
	}
	
	public List<Assessor> findAllAssessors(Integer page) {
		return (List<Assessor>) assessorRepository.findAll(page);
	}
    
}
