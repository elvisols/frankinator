package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TrainingStatusRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingStatus;
import ng.exelon.frankinator2_0.model.TrainingStatus;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingStatusServiceImpl implements TrainingStatusService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TrainingStatusServiceImpl.class);

    @Autowired
    private TrainingStatusRepository trainingStatusRepository; 

	public TrainingStatus findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting TrainingStatus with ID ", id);
		return trainingStatusRepository.findById(id);
	}

	public TrainingStatus create(TrainingStatus TrainingStatus) throws ConstraintsViolationException {
		LOG.warn("Saving TrainingStatus ", TrainingStatus);
		return trainingStatusRepository.save(TrainingStatus);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting TrainingStatus with ID ", id);
		trainingStatusRepository.deleteById(id);
	}

	public TrainingStatus update(TrainingStatus trainingStatus) throws EntityNotFoundException {
		TrainingStatus ts = trainingStatusRepository.findById(trainingStatus.getId());
		// Throw exception for missing TrainingStatus ID
		if(ts == null) throw new EntityNotFoundException("Not found TrainingStatus with ID: " + trainingStatus.getId());
		// Update trainingStatus's records accordingly
		ts.setMessage(trainingStatus.getMessage() == null ? ts.getMessage() : trainingStatus.getMessage());
		ts.setStatusDate(trainingStatus.getStatusDate() == null ? ts.getStatusDate() : trainingStatus.getStatusDate());
		ts.setStatusTime(trainingStatus.getStatusTime() == null ? ts.getStatusTime() : trainingStatus.getStatusTime());
		ts.setStatusType(trainingStatus.getStatusType() == null ? ts.getStatusType() : trainingStatus.getStatusType());
		ts.setTraining(trainingStatus.getTraining() == null ? ts.getTraining() : trainingStatus.getTraining());
		ts.setUser(trainingStatus.getUser() == null ? ts.getUser() : trainingStatus.getUser());
		LOG.warn("updating TrainingStatus with ID " + trainingStatus.getId());
		return trainingStatusRepository.save(ts);
	}
	
	public List<TrainingStatus> findAllTrainingStatus(Integer page) {
		return (List<TrainingStatus>) trainingStatusRepository.findAll(page);
	}
    
}
