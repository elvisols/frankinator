package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingMedia;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TrainingMediaService {
	
	TrainingMedia findOne(Long id) throws EntityNotFoundException;
	    
    TrainingMedia create(TrainingMedia TrainingMedia) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    TrainingMedia update(TrainingMedia tm) throws EntityNotFoundException;
    
    List<TrainingMedia> findAllTrainingMedias(Integer page);

}
