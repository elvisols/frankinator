package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.UserRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.User;
import ng.exelon.frankinator2_0.model.User;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository; 

	public User findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting user with ID ", id);
		return userRepository.findById(id);
	}

	public User create(User user) throws ConstraintsViolationException {
		LOG.warn("Saving user ", user);
		return userRepository.save(user);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting user with ID ", id);
		userRepository.deleteById(id);
	}

	public User update(User user) throws EntityNotFoundException {
		User u = userRepository.findById(user.getId());
		// Throw exception for missing User ID
		if(u == null) throw new EntityNotFoundException("Not found User with ID: " + user.getId());
		// Update user's records accordingly
		u.setEmail(user.getEmail() == null ? u.getEmail() : user.getEmail());
		u.setEnabled(user.getEnabled() == null ? u.getEnabled() : user.getEnabled());
		u.setFirstName(user.getFirstName() == null ? u.getFirstName() : user.getFirstName());
		u.setLastLogDate(user.getLastLogDate() == null ? u.getLastLogDate() : user.getLastLogDate());
		u.setLastLogTime(user.getLastLogTime() == null ? u.getLastLogTime() : user.getLastLogTime());
		u.setLastName(user.getLastName() == null ? u.getLastName() : user.getLastName());
		u.setLoggedInIp(user.getLoggedInIp() == null ? u.getLoggedInIp() : user.getLoggedInIp());
		u.setMobileNumber(user.getMobileNumber() == null ? u.getMobileNumber() : user.getMobileNumber());
		u.setPassword(user.getPassword() == null ? u.getPassword() : user.getPassword());
		u.setProfileStartDate(user.getProfileStartDate() == null ? u.getProfileStartDate() : user.getProfileStartDate());
		u.setProfileEndDate(user.getProfileEndDate() == null ? u.getProfileEndDate() : user.getProfileEndDate());
		u.setRoles(user.getRoles() == null ? u.getRoles() : user.getRoles());
		u.setSex(user.getSex() == null ? u.getSex() : user.getSex());
		LOG.warn("updating user with ID ", user.getId());
		return userRepository.save(u);
	}

	public List<User> findRoleUsers(RoleEnum role) {
		return (List<User>) userRepository.findRoleUsers(role);
	}
	
	public List<User> findAllUsers(Integer page) {
		return (List<User>) userRepository.findAll(page);
	}
    
}
