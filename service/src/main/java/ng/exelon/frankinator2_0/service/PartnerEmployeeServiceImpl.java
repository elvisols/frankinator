package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.PartnerEmployeeRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.PartnerEmployee;
import ng.exelon.frankinator2_0.model.PartnerEmployee;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartnerEmployeeServiceImpl implements PartnerEmployeeService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(PartnerEmployeeServiceImpl.class);

    @Autowired
    private PartnerEmployeeRepository PartnerEmployeeRepository; 

	public PartnerEmployee findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting PartnerEmployee with ID ", id);
		return PartnerEmployeeRepository.findById(id);
	}

	public PartnerEmployee create(PartnerEmployee PartnerEmployee) throws ConstraintsViolationException {
		LOG.warn("Saving PartnerEmployee ", PartnerEmployee);
		return PartnerEmployeeRepository.save(PartnerEmployee);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting PartnerEmployee with ID ", id);
		PartnerEmployeeRepository.deleteById(id);
	}

	public PartnerEmployee update(PartnerEmployee partnerEmployee) throws EntityNotFoundException {
		PartnerEmployee pe = PartnerEmployeeRepository.findById(partnerEmployee.getId());
		// Throw exception for missing PartnerEmployee ID
		if(pe == null) throw new EntityNotFoundException("Not found PartnerEmployee with ID: " + partnerEmployee.getId());
		// Update partnerEmployee's records accordingly
		pe.setFirstName(partnerEmployee.getFirstName() == null ? pe.getFirstName() : partnerEmployee.getFirstName());
		pe.setMiddleName(partnerEmployee.getMiddleName() == null ? pe.getMiddleName() : partnerEmployee.getMiddleName());
		pe.setLastName(partnerEmployee.getLastName() == null ? pe.getLastName() : partnerEmployee.getLastName());
		pe.setSex(partnerEmployee.getSex() == null ? pe.getSex() : partnerEmployee.getSex());
		pe.setDateOfBirth(partnerEmployee.getDateOfBirth() == null ? pe.getDateOfBirth() : partnerEmployee.getDateOfBirth());
		pe.setMaritalStatus(partnerEmployee.getMaritalStatus() == null ? pe.getMaritalStatus() : partnerEmployee.getMaritalStatus());
		pe.setMobileNumber(partnerEmployee.getMobileNumber() == null ? pe.getMobileNumber() : partnerEmployee.getMobileNumber());
		pe.setEmail(partnerEmployee.getEmail() == null ? pe.getEmail() : partnerEmployee.getEmail());
		pe.setBirthCountry(partnerEmployee.getBirthCountry() == null ? pe.getBirthCountry() : partnerEmployee.getBirthCountry());
		pe.setResidentCountry(partnerEmployee.getResidentCountry() == null ? pe.getResidentCountry() : partnerEmployee.getResidentCountry());
		pe.setPartner(partnerEmployee.getPartner() == null ? pe.getPartner() : partnerEmployee.getPartner());
		LOG.warn("updating PartnerEmployee with ID " + partnerEmployee.getId());
		return PartnerEmployeeRepository.save(pe);
	}
	
	public List<PartnerEmployee> findAllPartnerEmployees(Integer page) {
		return (List<PartnerEmployee>) PartnerEmployeeRepository.findAll(page);
	}
    
}
