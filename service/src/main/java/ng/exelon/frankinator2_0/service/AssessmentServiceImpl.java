package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.AssessmentRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessment;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssessmentServiceImpl implements AssessmentService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(AssessmentServiceImpl.class);

    @Autowired
    private AssessmentRepository assessmentRepository; 

	public Assessment findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Assessment with ID ", id);
		return assessmentRepository.findById(id);
	}

	public Assessment create(Assessment assessment) throws ConstraintsViolationException {
		LOG.warn("Saving Assessment ", assessment);
		return assessmentRepository.save(assessment);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Assessment with ID ", id);
		assessmentRepository.deleteById(id);
	}

	public Assessment update(Assessment assessment) throws EntityNotFoundException {
		Assessment a = assessmentRepository.findById(assessment.getId());
		if(a == null) throw new EntityNotFoundException("Not found Assessment with ID: " + assessment.getId());
		// Update assessment
		a.setAssessor(assessment.getAssessor() == null ? a.getAssessor() : assessment.getAssessor());
		a.setTrainer(assessment.getTrainer() == null ? a.getTrainer() : assessment.getTrainer());
		a.setTraining(assessment.getTraining() == null ? a.getTraining() : assessment.getTraining());
		a.setCreatedBy(assessment.getCreatedBy() == null ? a.getCreatedBy() : assessment.getCreatedBy());
		a.setKpis(assessment.getKpis() == null ? a.getKpis() : assessment.getKpis());
		LOG.warn("updating Assessment with ID " + assessment.getId());
		return assessmentRepository.save(a);
	}
	
	public List<Assessment> findAllAssessments(Integer page) {
		return (List<Assessment>) assessmentRepository.findAll(page);
	}
    
}
