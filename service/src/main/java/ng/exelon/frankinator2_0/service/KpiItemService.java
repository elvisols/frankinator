package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.KpiItem;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface KpiItemService {
	
	KpiItem findOne(Long id) throws EntityNotFoundException;
	    
    KpiItem create(KpiItem KpiItem) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    KpiItem update(KpiItem kpiItem) throws EntityNotFoundException;
    
    List<KpiItem> findAllKpiItems(Integer page);

}
