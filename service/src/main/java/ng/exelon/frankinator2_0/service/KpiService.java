package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface KpiService {
	
	Kpi findOne(Long id) throws EntityNotFoundException;
	    
    Kpi create(Kpi Kpi) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Kpi update(Kpi kpi) throws EntityNotFoundException;
    
    List<Kpi> findAllKpis(Integer page);

}
