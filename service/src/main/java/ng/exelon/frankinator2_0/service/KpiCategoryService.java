package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.KpiCategory;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface KpiCategoryService {
	
	KpiCategory findOne(String id) throws EntityNotFoundException;
	    
    KpiCategory create(KpiCategory KpiCategory) throws ConstraintsViolationException;

    void delete(String id) throws EntityNotFoundException;

    KpiCategory update(KpiCategory kpiCategory) throws EntityNotFoundException;
    
    List<KpiCategory> findAllKpiCategorys(Integer page);

}
