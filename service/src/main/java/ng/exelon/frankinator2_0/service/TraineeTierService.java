package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TraineeTier;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TraineeTierService {
	
	TraineeTier findOne(String id) throws EntityNotFoundException;
	    
    TraineeTier create(TraineeTier TraineeTier) throws ConstraintsViolationException;

    void delete(String id) throws EntityNotFoundException;

    TraineeTier update(TraineeTier tt) throws EntityNotFoundException;
    
    List<TraineeTier> findAllTraineeTiers(Integer page);

}
