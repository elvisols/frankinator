package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TraineeGroupService {
	
	TraineeGroup findOne(String id) throws EntityNotFoundException;
	    
    TraineeGroup create(TraineeGroup TraineeGroup) throws ConstraintsViolationException;

    void delete(String id) throws EntityNotFoundException;

    TraineeGroup update(TraineeGroup tg) throws EntityNotFoundException;
    
    List<TraineeGroup> findAllTraineeGroups(Integer page);

}
