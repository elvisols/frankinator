package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.PartnerRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.Partner;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartnerServiceImpl implements PartnerService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(PartnerServiceImpl.class);

    @Autowired
    private PartnerRepository PartnerRepository; 

	public Partner findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Partner with ID ", id);
		return PartnerRepository.findById(id);
	}

	public Partner create(Partner Partner) throws ConstraintsViolationException {
		LOG.warn("Saving Partner ", Partner);
		return PartnerRepository.save(Partner);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Partner with ID ", id);
		PartnerRepository.deleteById(id);
	}

	public Partner update(Partner partner) throws EntityNotFoundException {
		Partner p = PartnerRepository.findById(partner.getId());
		// Throw exception for missing Partner ID
		if(p == null) throw new EntityNotFoundException("Not found Partner with ID: " + partner.getId());
		// Update partner's records accordingly
		p.setAddress(partner.getAddress() == null ? p.getAddress() : partner.getAddress());
		p.setContactPhone(partner.getContactPhone() == null ? p.getContactPhone() : partner.getContactPhone());
		p.setDateAdded(partner.getDateAdded() == null ? p.getDateAdded() : partner.getDateAdded());
		p.setDescription(partner.getDescription() == null ? p.getDescription() : partner.getDescription());
		p.setEmail(partner.getEmail() == null ? p.getEmail() : partner.getEmail());
		p.setGis(partner.getGis() == null ? p.getGis() : partner.getGis());
		p.setIncDate(partner.getIncDate() == null ? p.getIncDate() : partner.getIncDate());
		p.setMobileNumber(partner.getMobileNumber() == null ? p.getMobileNumber() : partner.getMobileNumber());
		p.setName(partner.getName() == null ? p.getName() : partner.getName());
		p.setRcNumber(partner.getRcNumber() == null ? p.getRcNumber() : partner.getRcNumber());
		p.setShortName(partner.getShortName() == null ? p.getShortName() : partner.getShortName());
		p.setWebSite(partner.getWebSite() == null ? p.getWebSite() : partner.getWebSite());
		p.setEmployees(partner.getEmployees().isEmpty() ? p.getEmployees() : partner.getEmployees());
		p.setTrainers(partner.getTrainers().isEmpty() ? p.getTrainers() : partner.getTrainers());
		LOG.warn("updating Partner with ID " + partner.getId());
		return PartnerRepository.save(p);
	}
	
	public List<Partner> findAllPartners(Integer page) {
		return (List<Partner>) PartnerRepository.findAll(page);
	}
    
}
