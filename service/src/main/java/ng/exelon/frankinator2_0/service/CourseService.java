package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Course;
import ng.exelon.frankinator2_0.model.value.TierEnum;

public interface CourseService {
	
	Course findOne(Long id) throws EntityNotFoundException;
	    
    Course create(Course Course) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Course update(Course course) throws EntityNotFoundException;
    
    List<Course> findCoursesByTier(TierEnum te);
    
    List<Course> findAllCourses();

}
