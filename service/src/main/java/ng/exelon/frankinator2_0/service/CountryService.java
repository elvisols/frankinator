package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Country;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface CountryService {
	
	Country findOne(String id) throws EntityNotFoundException;
	    
    Country create(Country Country) throws ConstraintsViolationException;

    void delete(String id) throws EntityNotFoundException;

    Country update(Country country) throws EntityNotFoundException;
    
    List<Country> findAllCountrys(Integer page);

}
