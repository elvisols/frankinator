package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessment;

public interface AssessmentService {
	
	Assessment findOne(Long id) throws EntityNotFoundException;
	    
    Assessment create(Assessment Assessment) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Assessment update(Assessment assessment) throws EntityNotFoundException;
    
    List<Assessment> findAllAssessments(Integer page);

}
