package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.AssessmentKpi;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface AssessmentKpiService {
	
	AssessmentKpi findOne(Long id) throws EntityNotFoundException;
	    
    AssessmentKpi create(AssessmentKpi assessmentKpi) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    AssessmentKpi update(AssessmentKpi assessmentKpi) throws EntityNotFoundException;
    
    List<AssessmentKpi> findAllAssessmentKpis(Integer page);

}
