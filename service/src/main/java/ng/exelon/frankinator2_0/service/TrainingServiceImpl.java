package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TrainingRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Training;
import ng.exelon.frankinator2_0.model.Training;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingServiceImpl implements TrainingService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TrainingServiceImpl.class);

    @Autowired
    private TrainingRepository trainingRepository; 

	public Training findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Training with ID ", id);
		return trainingRepository.findById(id);
	}

	public Training create(Training Training) throws ConstraintsViolationException {
		LOG.warn("Saving Training ", Training);
		return trainingRepository.save(Training);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Training with ID ", id);
		trainingRepository.deleteById(id);
	}

	public Training update(Training training) throws EntityNotFoundException {
		Training t = trainingRepository.findById(training.getId());
		// Throw exception for missing Training ID
		if(t == null) throw new EntityNotFoundException("Not found Training with ID: " + training.getId());
		// Update training's records accordingly
		t.setAddress(training.getAddress() == null ? t.getAddress() : training.getAddress());
		t.setApprovedBy(training.getApprovedBy() == null ? t.getApprovedBy() : training.getApprovedBy());
		t.setApprovedDate(training.getApprovedDate() == null ? t.getApprovedDate() : training.getApprovedDate());
		t.setApprovedTime(training.getApprovedTime() == null ? t.getApprovedTime() : training.getApprovedTime());
		t.setAudience(training.getAudience() == null ? t.getAudience() : training.getAudience());
		t.setCountry(training.getCountry() == null ? t.getCountry() : training.getCountry());
		t.setCourse(training.getCourse() == null ? t.getCourse() : training.getCourse());
		t.setDurationInMins(training.getDurationInMins() == null ? t.getDurationInMins() : training.getDurationInMins());
		t.setExpectedAttendees(training.getExpectedAttendees() == null ? t.getExpectedAttendees() : training.getExpectedAttendees());
		t.setLanguage(training.getLanguage() == null ? t.getLanguage() : training.getLanguage());
		t.setLatitude(training.getLatitude() == null ? t.getLatitude() : training.getLatitude());
		t.setLongitude(training.getLongitude() == null ? t.getLongitude() : training.getLongitude());
		t.setPartner(training.getPartner() == null ? t.getPartner() : training.getPartner());
		t.setRequestDate(training.getRequestDate() == null ? t.getRequestDate() : training.getRequestDate());
		t.setRequestTime(training.getRequestTime() == null ? t.getRequestTime() : training.getRequestTime());
		t.setScheduledBy(training.getScheduledBy() == null ? t.getScheduledBy() : training.getScheduledBy());
		t.setScheduledTime(training.getScheduledTime() == null ? t.getScheduledTime() : training.getScheduledTime());
		t.setScheduledDate(training.getScheduledDate() == null ? t.getScheduledDate() : training.getScheduledDate());
		t.setTier(training.getTier() == null ? t.getTier() : training.getTier());
		t.setTrainer(training.getTrainer() == null ? t.getTrainer() : training.getTrainer());
//		t.setTypeOfTraining(training.getTypeOfTraining() == null ? t.getTypeOfTraining() : training.getTypeOfTraining());
		LOG.warn("updating Training with ID " + training.getId());
		return trainingRepository.save(t);
	}
	
	public List<Training> findAllTrainings(Integer page) {
		return (List<Training>) trainingRepository.findAll(page);
	}
    
}
