package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.KpiCategoryRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.KpiCategory;
import ng.exelon.frankinator2_0.model.KpiCategory;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KpiCategoryServiceImpl implements KpiCategoryService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(KpiCategoryServiceImpl.class);

    @Autowired
    private KpiCategoryRepository kpiCategoryRepository; 

	public KpiCategory findOne(String id) throws EntityNotFoundException {
		LOG.warn("Getting KpiCategory with ID ", id);
		return kpiCategoryRepository.findById(id);
	}

	public KpiCategory create(KpiCategory kpiCategory) throws ConstraintsViolationException {
		LOG.warn("Saving KpiCategory ", kpiCategory);
		return kpiCategoryRepository.save(kpiCategory);
	}

	public void delete(String id) throws EntityNotFoundException {
		LOG.warn("Deleting KpiCategory with ID ", id);
		kpiCategoryRepository.deleteById(id);
	}

	public KpiCategory update(KpiCategory kpiCategory) throws EntityNotFoundException {
		KpiCategory c = kpiCategoryRepository.findById(kpiCategory.getId());
		// Throw exception for missing KpiCategory ID
		if(c == null) throw new EntityNotFoundException("Not found KpiCategory with ID: " + kpiCategory.getId());
		// Update kpiCategory's records accordingly
		c.setName(kpiCategory.getName() == null ? c.getName() : kpiCategory.getName());
		c.setDescription(kpiCategory.getDescription() == null ? c.getDescription() : kpiCategory.getDescription());
		c.setKpis(kpiCategory.getKpis().isEmpty() ? c.getKpis() : kpiCategory.getKpis());
		// Todo ...
		LOG.warn("updating KpiCategory with ID " + kpiCategory.getId());
		return kpiCategoryRepository.save(c);
	}
	
	public List<KpiCategory> findAllKpiCategorys(Integer page) {
		return (List<KpiCategory>) kpiCategoryRepository.findAll(page);
	}
    
}
