package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TraineeGroupRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import ng.exelon.frankinator2_0.model.TraineeGroup;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TraineeGroupServiceImpl implements TraineeGroupService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TraineeGroupServiceImpl.class);

    @Autowired
    private TraineeGroupRepository traineeGroupRepository; 

	public TraineeGroup findOne(String id) throws EntityNotFoundException {
		LOG.warn("Getting TraineeGroup with ID ", id);
		return traineeGroupRepository.findById(id);
	}

	public TraineeGroup create(TraineeGroup TraineeGroup) throws ConstraintsViolationException {
		LOG.warn("Saving TraineeGroup ", TraineeGroup);
		return traineeGroupRepository.save(TraineeGroup);
	}

	public void delete(String id) throws EntityNotFoundException {
		LOG.warn("Deleting TraineeGroup with ID ", id);
		traineeGroupRepository.deleteById(id);
	}

	public TraineeGroup update(TraineeGroup traineeGroup) throws EntityNotFoundException {
		TraineeGroup tg = traineeGroupRepository.findById(traineeGroup.getCode());
		// Throw exception for missing TraineeGroup ID
		if(tg == null) throw new EntityNotFoundException("Not found TraineeGroup with ID: " + traineeGroup.getCode());
		// Update traineeGroup's records accordingly
		tg.setDescription(traineeGroup.getDescription() == null ? tg.getDescription() : traineeGroup.getDescription());
		// Todo ...
		LOG.warn("updating TraineeGroup with ID " + traineeGroup.getCode());
		return traineeGroupRepository.save(tg);
	}
	
	public List<TraineeGroup> findAllTraineeGroups(Integer page) {
		return (List<TraineeGroup>) traineeGroupRepository.findAll(page);
	}
    
}
