package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TraineeTierRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TraineeTier;
import ng.exelon.frankinator2_0.model.TraineeTier;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TraineeTierServiceImpl implements TraineeTierService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TraineeTierServiceImpl.class);

    @Autowired
    private TraineeTierRepository traineeTierRepository; 

	public TraineeTier findOne(String id) throws EntityNotFoundException {
		LOG.warn("Getting TraineeTier with ID ", id);
		return traineeTierRepository.findById(id);
	}

	public TraineeTier create(TraineeTier TraineeTier) throws ConstraintsViolationException {
		LOG.warn("Saving TraineeTier ", TraineeTier);
		return traineeTierRepository.save(TraineeTier);
	}

	public void delete(String id) throws EntityNotFoundException {
		LOG.warn("Deleting TraineeTier with ID ", id);
		traineeTierRepository.deleteById(id);
	}

	public TraineeTier update(TraineeTier traineeTier) throws EntityNotFoundException {
		TraineeTier tt = traineeTierRepository.findById(traineeTier.getCode());
		// Throw exception for missing TraineeTier ID
		if(tt == null) throw new EntityNotFoundException("Not found TraineeTier with ID: " + traineeTier.getCode());
		// Update traineeTier's records accordingly
		tt.setName(traineeTier.getName() == null ? tt.getName() : traineeTier.getName());
		tt.setDescription(traineeTier.getDescription() == null ? tt.getDescription() : traineeTier.getDescription());
		LOG.warn("updating TraineeTier with ID " + traineeTier.getCode());
		return traineeTierRepository.save(tt);
	}
	
	public List<TraineeTier> findAllTraineeTiers(Integer page) {
		return (List<TraineeTier>) traineeTierRepository.findAll(page);
	}
    
}
