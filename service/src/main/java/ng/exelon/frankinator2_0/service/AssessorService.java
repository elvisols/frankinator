package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Assessor;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface AssessorService {
	
	Assessor findOne(Long id) throws EntityNotFoundException;
	    
    Assessor create(Assessor Assessor) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Assessor update(Assessor assessor) throws EntityNotFoundException;
    
    List<Assessor> findAllAssessors(Integer page);

}
