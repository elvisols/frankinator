package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Training;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TrainingService {
	
	Training findOne(Long id) throws EntityNotFoundException;
	    
    Training create(Training Training) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    Training update(Training t) throws EntityNotFoundException;
    
    List<Training> findAllTrainings(Integer page);

}
