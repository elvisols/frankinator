package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TrainingMediaRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingMedia;
import ng.exelon.frankinator2_0.model.TrainingMedia;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingMediaServiceImpl implements TrainingMediaService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TrainingMediaServiceImpl.class);

    @Autowired
    private TrainingMediaRepository trainingMediaRepository; 

	public TrainingMedia findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting TrainingMedia with ID ", id);
		return trainingMediaRepository.findById(id);
	}

	public TrainingMedia create(TrainingMedia TrainingMedia) throws ConstraintsViolationException {
		LOG.warn("Saving TrainingMedia ", TrainingMedia);
		return trainingMediaRepository.save(TrainingMedia);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting TrainingMedia with ID ", id);
		trainingMediaRepository.deleteById(id);
	}

	public TrainingMedia update(TrainingMedia trainingMedia) throws EntityNotFoundException {
		TrainingMedia tm = trainingMediaRepository.findById(trainingMedia.getId());
		// Throw exception for missing TrainingMedia ID
		if(tm == null) throw new EntityNotFoundException("Not found TrainingMedia with ID: " + trainingMedia.getId());
		// Update trainingMedia's records accordingly
		tm.setFilename(trainingMedia.getFilename() == null ? tm.getFilename() : trainingMedia.getFilename());
		tm.setMediaType(trainingMedia.getMediaType() == null ? tm.getMediaType() : trainingMedia.getMediaType());
		tm.setTraining(trainingMedia.getTraining() == null ? tm.getTraining() : trainingMedia.getTraining());
		tm.setUploadDate(trainingMedia.getUploadDate() == null ? tm.getUploadDate() : trainingMedia.getUploadDate());
		tm.setUploadTime(trainingMedia.getUploadTime() == null ? tm.getUploadTime() : trainingMedia.getUploadTime());
		LOG.warn("updating TrainingMedia with ID " + trainingMedia.getId());
		return trainingMediaRepository.save(tm);
	}
	
	public List<TrainingMedia> findAllTrainingMedias(Integer page) {
		return (List<TrainingMedia>) trainingMediaRepository.findAll(page);
	}
    
}
