package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingUpload;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TrainingUploadService {
	
	TrainingUpload findOne(Long id) throws EntityNotFoundException;
	    
    TrainingUpload create(TrainingUpload TrainingUpload) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    TrainingUpload update(TrainingUpload tu) throws EntityNotFoundException;
    
    List<TrainingUpload> findAllTrainingUploads(Integer page);

}
