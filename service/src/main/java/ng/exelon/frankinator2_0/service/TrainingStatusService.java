package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.TrainingStatus;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

public interface TrainingStatusService {
	
	TrainingStatus findOne(Long id) throws EntityNotFoundException;
	    
    TrainingStatus create(TrainingStatus TrainingStatus) throws ConstraintsViolationException;

    void delete(Long id) throws EntityNotFoundException;

    TrainingStatus update(TrainingStatus ts) throws EntityNotFoundException;
    
    List<TrainingStatus> findAllTrainingStatus(Integer page);

}
