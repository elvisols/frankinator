package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.TargetRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.Target;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TargetServiceImpl implements TargetService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(TargetServiceImpl.class);

    @Autowired
    private TargetRepository targetRepository; 

	public Target findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting Target with ID ", id);
		return targetRepository.findById(id);
	}

	public Target create(Target Target) throws ConstraintsViolationException {
		LOG.warn("Saving Target ", Target);
		return targetRepository.save(Target);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting Target with ID ", id);
		targetRepository.deleteById(id);
	}

	public Target update(Target target) throws EntityNotFoundException {
		Target t = targetRepository.findById(target.getId());
		// Throw exception for missing Target ID
		if(t == null) throw new EntityNotFoundException("Not found Target with ID: " + target.getId());
		// Update target's records accordingly
		t.setCountry(target.getCountry() == null ? t.getCountry() : target.getCountry());
		t.setCourse(target.getCourse() == null ? t.getCourse() : target.getCourse());
		t.setDescription(target.getDescription() == null ? t.getDescription() : target.getDescription());
		t.setEndDate(target.getEndDate() == null ? t.getEndDate() : target.getEndDate());
		t.setGroup(target.getGroup() == null ? t.getGroup() : target.getGroup());
		t.setName(target.getName() == null ? t.getName() : target.getName());
		t.setPartner(target.getPartner() == null ? t.getPartner() : target.getPartner());
		t.setQuarter(target.getQuarter() == null ? t.getQuarter() : target.getQuarter());
		t.setStartDate(target.getStartDate() == null ? t.getStartDate() : target.getStartDate());
		t.setTargetValue(target.getTargetValue() == null ? t.getTargetValue() : target.getTargetValue());
		t.setTargetYear(target.getTargetYear() == null ? t.getTargetYear() : target.getTargetYear());
		t.setTier(target.getTier() == null ? t.getTier() : target.getTier());
		LOG.warn("updating Target with ID " + target.getId());
		return targetRepository.save(t);
	}
	
	public List<Target> findAllTargets(Integer page) {
		return (List<Target>) targetRepository.findAll(page);
	}
    
}
