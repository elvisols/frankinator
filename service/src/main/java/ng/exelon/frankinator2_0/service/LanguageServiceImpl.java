package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.LanguageRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.Language;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageServiceImpl implements LanguageService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(LanguageServiceImpl.class);

    @Autowired
    private LanguageRepository languageRepository; 

	public Language findOne(String id) throws EntityNotFoundException {
		LOG.warn("Getting Language with ID ", id);
		return languageRepository.findById(id);
	}

	public Language create(Language Language) throws ConstraintsViolationException {
		LOG.warn("Saving Language ", Language);
		return languageRepository.save(Language);
	}

	public void delete(String id) throws EntityNotFoundException {
		LOG.warn("Deleting Language with ID ", id);
		languageRepository.deleteById(id);
	}

	public Language update(Language language) throws EntityNotFoundException {
		Language l = languageRepository.findById(language.getCode());
		// Throw exception for missing Language ID
		if(l == null) throw new EntityNotFoundException("Not found Language with ID: " + language.getCode());
		// Update language's records accordingly
		// Todo ...
		LOG.warn("updating Language with ID " + language.getCode());
		return languageRepository.save(l);
	}
	
	public List<Language> findAllLanguages(Integer page) {
		return (List<Language>) languageRepository.findAll(page);
	}
    
}
