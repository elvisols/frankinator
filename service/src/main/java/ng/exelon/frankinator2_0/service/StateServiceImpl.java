package ng.exelon.frankinator2_0.service;

import java.util.List;

import ng.exelon.frankinator2_0.data.StateRepository;
import ng.exelon.frankinator2_0.exception.ConstraintsViolationException;
import ng.exelon.frankinator2_0.exception.EntityNotFoundException;
import ng.exelon.frankinator2_0.model.Kpi;
import ng.exelon.frankinator2_0.model.State;
import ng.exelon.frankinator2_0.model.value.RoleEnum;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StateServiceImpl implements StateService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(StateServiceImpl.class);

    @Autowired
    private StateRepository StateRepository; 

	public State findOne(Long id) throws EntityNotFoundException {
		LOG.warn("Getting State with ID ", id);
		return StateRepository.findById(id);
	}

	public State create(State State) throws ConstraintsViolationException {
		LOG.warn("Saving State ", State);
		return StateRepository.save(State);
	}

	public void delete(Long id) throws EntityNotFoundException {
		LOG.warn("Deleting State with ID ", id);
		StateRepository.deleteById(id);
	}

	public State update(State state) throws EntityNotFoundException {
		State s = StateRepository.findById(state.getId());
		// Throw exception for missing State ID
		if(s == null) throw new EntityNotFoundException("Not found State with ID: " + state.getId());
		// Update state's records accordingly
		LOG.warn("updating State with ID " + state.getId());
		return StateRepository.save(s);
	}
	
	public List<State> findAllStates(Integer page) {
		return (List<State>) StateRepository.findAll(page);
	}
    
}
