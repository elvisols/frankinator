/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraineeGroupDTO {
	
	private String code;
	
	private String description;
	
	public TraineeGroupDTO() { }
	
	public TraineeGroupDTO(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static TraineeGroupDTOBuilder newBuilder() {
		return new TraineeGroupDTOBuilder();
	}
	
	public static class TraineeGroupDTOBuilder {
		
		private String code;
		private String description;
		
		public TraineeGroupDTOBuilder setCode(String code) {
			this.code = code;
			return this;
		}
		public TraineeGroupDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		public TraineeGroupDTO createTraineeGroupDTO() {
			return new TraineeGroupDTO(code, description);
		}
		
	}
	

}
