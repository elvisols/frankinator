/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerEmployeeDTO extends AbstractPersonDTO {

	private Long id;

	private Partner partner;

	public PartnerEmployeeDTO() {
		super();
	}
	
	public PartnerEmployeeDTO(Long id, String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry, Partner partner) {
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		this.partner = partner;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	public Partner getPartner() {
		return partner;
	}

	public static PEDTOBuilder newPEDTOBuilder() {
		return new PEDTOBuilder();
	}
	
	public static class PEDTOBuilder extends AbstractPersonDTOBuilder {
		private Long id;
		private Partner partner;
		
		public PEDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public PEDTOBuilder setPartner(Partner partner) {
			this.partner = partner;
			return this;
		}
		public PartnerEmployeeDTO createPEDTO() {
			return new PartnerEmployeeDTO(id, firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry, partner);
		}
	}
	
}
