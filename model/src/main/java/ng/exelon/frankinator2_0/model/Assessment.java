/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_assessment")
@NamedQueries( {
	@NamedQuery(name = "Assessment.findAll", query = "select a from Assessment a order by a.id")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Assessment implements Serializable {
	private static final Long serialVersionUID = 1L;

	@SequenceGenerator(name="assessment_id_gen",  sequenceName="assessment_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="assessment_id_gen")
	private Long id;

	@ManyToOne(/*fetch = FetchType.LAZY*/)
	@JoinColumn(name="assessor", nullable=false)
	private Assessor assessor;

	@ManyToOne(/*fetch = FetchType.LAZY*/)
	@JoinColumn(name="trainer", nullable=false)
	private Trainer trainer;

	@ManyToOne(/*fetch = FetchType.LAZY*/)
	@JoinColumn(name="training", nullable=false)
	private Training training;

	@ManyToOne(/*fetch = FetchType.LAZY*/)
	@JoinColumn(name="created_by", nullable=true)
	private User createdBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="create_date", nullable = false)
	private Date createDate = new Date();
	
	@Temporal(TemporalType.TIME)
	@Column(name="create_time", nullable = false)
	private Date createTime = new Date();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "assessment")
	private List<AssessmentKpi> kpis;
	
	public Assessment() {
	}

	public Assessment(Assessor assessor, Trainer trainer, Training training, User createdBy, List<AssessmentKpi> kpis) {
		this.assessor = assessor;
		this.trainer = trainer;
		this.training = training;
		this.createdBy = createdBy;
		this.kpis = kpis;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Assessor getAssessor() {
		return assessor;
	}

	public void setAssessor(Assessor assessor) {
		this.assessor = assessor;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Assessment other = (Assessment) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public List<AssessmentKpi> getKpis() {
		return kpis;
	}

	public void setKpis(List<AssessmentKpi> kpis) {
		this.kpis = kpis;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getCreateTime() {
		return createTime;
	}
	
	

}
