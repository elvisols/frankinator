package ng.exelon.frankinator2_0.model.value;

public enum RoleEnum
{
    ROLE_ADMIN, ROLE_USER, ROLE_PARTNER, ROLE_TRAINER, ROLE_ACCESSOR
}
