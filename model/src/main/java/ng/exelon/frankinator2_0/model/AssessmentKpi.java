/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_assessment_kpi",
	uniqueConstraints= @UniqueConstraint(columnNames={"assessment", "kpi"}))
@NamedQueries( {
	@NamedQuery(name = "AssessmentKpi.findAll", query = "select ak from AssessmentKpi ak order by ak.id")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssessmentKpi implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="asskpi_id_gen",  sequenceName="asskpi_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="asskpi_id_gen")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="assessment", nullable=false)
	private Assessment assessment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="kpi", nullable=false)
	private Kpi kpi;

	
	@Column(name="kpi_value", nullable = false )
	private Float value;


	public AssessmentKpi() {	}


	public AssessmentKpi(Assessment assessment, Kpi kpi, Float value) {
		this.assessment = assessment;
		this.kpi = kpi;
		this.value = value;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Assessment getAssessment() {
		return assessment;
	}


	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}


	public Kpi getKpi() {
		return kpi;
	}


	public void setKpi(Kpi kpi) {
		this.kpi = kpi;
	}


	public Float getValue() {
		return value;
	}


	public void setValue(Float value) {
		this.value = value;
	}
	
	
	
}
