/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.TargetQuarter;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TargetDTO implements Serializable {
	
	private Long id;
	
	private Partner partner;
	
	private Course course;
	
	private Country country;
	
	private TraineeTier tier;
	
	private TraineeGroup group;
	
	private String name; 
	
	private String description;
	
	private Integer targetYear;
	
	private Long targetValue;
	
	@Enumerated(EnumType.STRING)
	private TargetQuarter quarter;
	
	private Date startDate;
	
	private Date endDate;

	public TargetDTO() { }
	
	public TargetDTO(Partner partner, Course course, Country country, TraineeTier tier, TraineeGroup group, String name, String description, Integer targetYear, Long targetValue, TargetQuarter quarter, Date startDate, Date endDate) {
		this.partner = partner;
		this.course = course;
		this.country = country;
		this.tier = tier;
		this.group = group;
		this.name = name;
		this.description = description;
		this.targetYear = targetYear;
		this.targetValue = targetValue;
		this.quarter = quarter;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Integer getTargetYear() {
		return targetYear;
	}

	public TargetQuarter getQuarter() {
		return quarter;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Partner getPartner() {
		return partner;
	}

	public Course getCourse() {
		return course;
	}

	public Country getCountry() {
		return country;
	}

	public TraineeTier getTier() {
		return tier;
	}

	public TraineeGroup getGroup() {
		return group;
	}

	public Long getTargetValue() {
		return targetValue;
	}

	public static TargetDTOBuilder newBuilder() {
		return new TargetDTOBuilder();
	}
	
	public static class TargetDTOBuilder {
		private Long id;
		private Partner partner;
		private Course course;
		private Country country;
		private TraineeTier tier;
		private TraineeGroup group;
		private String name; 
		private String description;
		private Integer targetYear;
		private Long targetValue;
		@Enumerated(EnumType.STRING)
		private TargetQuarter quarter;
		private Date startDate;
		private Date endDate;
		
		public TargetDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public TargetDTOBuilder setPartner(Partner partner) {
			this.partner = partner;
			return this;
		}
		public TargetDTOBuilder setCourse(Course course) {
			this.course = course;
			return this;
		}
		public TargetDTOBuilder setCountry(Country country) {
			this.country = country;
			return this;
		}
		public TargetDTOBuilder setTier(TraineeTier tier) {
			this.tier = tier;
			return this;
		}
		public TargetDTOBuilder setGroup(TraineeGroup group) {
			this.group = group;
			return this;
		}
		public TargetDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		public TargetDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		public TargetDTOBuilder setTargetYear(Integer targetYear) {
			this.targetYear = targetYear;
			return this;
		}
		public TargetDTOBuilder setTargetValue(Long targetValue) {
			this.targetValue = targetValue;
			return this;
		}
		public TargetDTOBuilder setQuarter(TargetQuarter quarter) {
			this.quarter = quarter;
			return this;
		}
		public TargetDTOBuilder setStartDate(Date startDate) {
			this.startDate = startDate;
			return this;
		}
		public TargetDTOBuilder setEndDate(Date endDate) {
			this.endDate = endDate;
			return this;
		}
		public TargetDTO createTargetDTO() {
			return new TargetDTO(partner, course, country, tier, group, name, description, targetYear, targetValue, quarter, startDate, endDate);
		}
	}

}
