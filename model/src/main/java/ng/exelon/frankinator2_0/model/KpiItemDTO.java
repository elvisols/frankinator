/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KpiItemDTO {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name; 
	
	private String description;
		
	private Kpi kpi;

	private Float weight;

	public KpiItemDTO() { }
	
	public KpiItemDTO(Long id, String name, String description, Kpi kpi, Float weight) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.kpi = kpi;
		this.weight = weight;
	}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Kpi getKpi() {
		return kpi;
	}

	public Float getWeight() {
		return weight;
	}

	public static KpiItemDTOBuilder newBuilder() {
		return new KpiItemDTOBuilder();
	}
	
	public static class KpiItemDTOBuilder {
		private Long id;
		private String name; 
		private String description;
		private Kpi kpi;
		private Float weight;
		
		public KpiItemDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public KpiItemDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		public KpiItemDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		public KpiItemDTOBuilder setKpi(Kpi kpi) {
			this.kpi = kpi;
			return this;
		}
		public KpiItemDTOBuilder setWeight(Float weight) {
			this.weight = weight;
			return this;
		}
		
		public KpiItemDTO createKpiItemDTO() {
			return new KpiItemDTO(id, name, description, kpi, weight);
		}
		
	}
	
}
