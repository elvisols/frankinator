/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssessorDTO extends AbstractPersonDTO {
	
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	public AssessorDTO() {
		super();
	}

	public AssessorDTO(Long id, String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry) { 
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		this.id = id;
	}
	
	public static AssessorDTOBuilder newBuilder() {
		return new AssessorDTOBuilder();
	}
	
	public static class AssessorDTOBuilder extends AbstractPersonDTOBuilder {
		
		private Long id;
		
		public AssessorDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		
		public AssessorDTO createAssessorDTO() {
			return new AssessorDTO(id, firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		}

	}

}
