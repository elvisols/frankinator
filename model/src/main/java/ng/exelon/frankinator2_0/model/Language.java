/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_ref_language")
@NamedQueries( {
	@NamedQuery(name = "Language.findAll", 	query = "select c from Language c  order by c.code ")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Language implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 10, name = "code", unique = false, nullable = false)
	@Id
	private String code;
	
	@Column(length = 40, name = "name", unique = false, nullable = false)
	private String name;

	public Language() {
	}

	public Language(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
