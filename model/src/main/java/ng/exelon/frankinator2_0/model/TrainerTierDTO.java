/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.TierEnum;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainerTierDTO implements Serializable {
	private static final Long serialVersionUID = 1L;

	private String code;
	
	@NotNull(message = "TrainerTier name must not be null.")
	private String name; 
	
	@NotNull(message = "TrainerTier description must not be null.")
	private String description;
	
	public TrainerTierDTO() {
	}
	
	public TrainerTierDTO(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public static TrainerTierDTOBuilder newBuilder() {
		return new TrainerTierDTOBuilder();
	}
	
	public static class TrainerTierDTOBuilder {
		
		private String code;
		private String name; 
		private String description;
		
		public TrainerTierDTOBuilder setCode(String code) {
			this.code = code;
			return this;
		}
		
		public TrainerTierDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		
		public TrainerTierDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		
		public TrainerTierDTO createTrainerTierDTO() {
			return new TrainerTierDTO(code, name, description);
		}
		
	}

}
