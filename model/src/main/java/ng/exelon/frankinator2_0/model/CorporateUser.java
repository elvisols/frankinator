/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;

import ng.exelon.frankinator2_0.model.AbstractEmbeddable.Address;

/**
 * @author ukaegbu
 *
 */
@MappedSuperclass
public abstract class CorporateUser extends AbstractEmbeddable implements Serializable {
	protected static final long serialVersionUID = 1L;

	@Column(length = 60, name = "name", unique = true)
	protected String name;
	
	@Column(length = 20, name = "contact_phone", unique = false)
	protected String contactPhone;
	
	@Column(length = 20, name = "inc_number", unique = false)
	protected String rcNumber;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "inc_date")
	protected Date incDate;
	
	@Column(length = 20, name = "mobile_number", unique = false)
	protected String mobileNumber;
	
	@Column(length = 60, name = "email", unique = true, nullable=false)
	protected String email;
	
	@Column(length = 50, name = "website", unique = true)
	protected String webSite;
	
//	@Valid
	@Embedded
	protected Address address;
	
	@Column(length = 30, name = "gis", unique = true)
	protected String gis;
	
	/// display properties. we should remote it to another companion table
	@Column(length = 20, name = "short_name", unique = true)
	protected String shortName; 
	
	@Column(length = 70, name = "description", unique = false)
	protected String description; 

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_added")
	protected Date dateAdded;

	public CorporateUser() {
		// TODO Auto-generated constructor stub
	}
	
	public CorporateUser(String name, String contactPhone, String rcNumber, Date incDate, String mobileNumber, String email, String webSite, Address address, String gis, String shortName, String description, Date dateAdded) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.contactPhone = contactPhone;
		this.rcNumber = rcNumber;
		this.incDate = incDate;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.webSite = webSite;
		this.address = address;
		this.gis = gis;
		this.shortName = shortName;
		this.description = description;
		this.dateAdded = dateAdded;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the contactPhone
	 */
	public String getContactPhone() {
		return contactPhone;
	}

	/**
	 * @param contactPhone the contactPhone to set
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * @return the rcNumber
	 */
	public String getRcNumber() {
		return rcNumber;
	}

	/**
	 * @param rcNumber the rcNumber to set
	 */
	public void setRcNumber(String rcNumber) {
		this.rcNumber = rcNumber;
	}

	/**
	 * @return the incDate
	 */
	public Date getIncDate() {
		return incDate;
	}

	/**
	 * @param incDate the incDate to set
	 */
	public void setIncDate(Date incDate) {
		this.incDate = incDate;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the webSite
	 */
	public String getWebSite() {
		return webSite;
	}

	/**
	 * @param webSite the webSite to set
	 */
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the gis
	 */
	public String getGis() {
		return gis;
	}

	/**
	 * @param gis the gis to set
	 */
	public void setGis(String gis) {
		this.gis = gis;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the dateAdded
	 */
	public Date getDateAdded() {
		return dateAdded;
	}

	/**
	 * @param dateAdded the dateAdded to set
	 */
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

}
