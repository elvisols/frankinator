package ng.exelon.frankinator2_0.model.value;

public enum KpiInputType {
	
	SELECT("Select"),
	INPUT("Input"),
	CHECK("Check Box");
	
	private final String type;
	
	KpiInputType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return this.type;
	}
	
}
