/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingUploadDTO implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	private Long id;
	
	private Trainer trainer;

	private Partner partner;

	private List<Trainee> trainees;
	
	private List<TrainingMedia> media;
	
	private Integer actualAttendees;

	public TrainingUploadDTO() {
		// TODO Auto-generated constructor stub
	}

	public TrainingUploadDTO(Long id, Trainer trainer, Partner partner,
			List<Trainee> trainees, List<TrainingMedia> media, Integer actualAttendees) {
		this.id = id;
		this.trainer = trainer;
		this.partner = partner;
		this.trainees = trainees;
		this.media = media;
		this.actualAttendees = actualAttendees;
	}

	public Long getId() {
		return id;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public Partner getPartner() {
		return partner;
	}

	public List<Trainee> getTrainees() {
		return trainees;
	}

	public List<TrainingMedia> getMedia() {
		return media;
	}

	public Integer getActualAttendees() {
		return actualAttendees;
	}

	public static TrainingUploadDTOBuilder newBuilder() {
		return new TrainingUploadDTOBuilder();
	}
	
	public static class TrainingUploadDTOBuilder {
		private Long id;
		private Trainer trainer;
		private Partner partner;
		private List<Trainee> trainees;
		private List<TrainingMedia> media;
		private Integer actualAttendees;
		
		public TrainingUploadDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public TrainingUploadDTOBuilder setTrainer(Trainer trainer) {
			this.trainer = trainer;
			return this;
		}
		public TrainingUploadDTOBuilder setPartner(Partner partner) {
			this.partner = partner;
			return this;
		}
		public TrainingUploadDTOBuilder setTrainees(List<Trainee> trainees) {
			this.trainees = trainees;
			return this;
		}
		public TrainingUploadDTOBuilder setMedia(List<TrainingMedia> media) {
			this.media = media;
			return this;
		}
		public TrainingUploadDTOBuilder setActualAttendees(Integer actualAttendees) {
			this.actualAttendees = actualAttendees;
			return this;
		}
		public TrainingUploadDTO createTrainingUploadDTO() {
			return new TrainingUploadDTO(id, trainer, partner, trainees, media, actualAttendees);
		}
	}
	
}
