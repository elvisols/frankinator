/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;


/**
 * @author ukaegbu
 *
 */
public abstract class AbstractPersonDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected String middleName;
	
	protected String firstName;
	
	protected String lastName;
	
	@Enumerated(EnumType.STRING)
	protected Sex sex;
	
	@Temporal(TemporalType.DATE)
	protected Date dateOfBirth;
	
	@Enumerated(EnumType.STRING)
	protected MaritalStatus maritalStatus;
	
	protected String mobileNumber;
	
	protected String email;
	
	protected String birthCountry;
	
	protected String residentCountry;
	
	public AbstractPersonDTO() { }

	public AbstractPersonDTO(String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry) { 
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.sex = sex;
		this.dateOfBirth = dateOfBirth;
		this.maritalStatus = maritalStatus;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.birthCountry = birthCountry;
		this.residentCountry = residentCountry;
	}
	
	public String getMiddleName() {
		return middleName;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public Sex getSex() {
		return sex;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public String getEmail() {
		return email;
	}
	public String getBirthCountry() {
		return birthCountry;
	}
	public String getResidentCountry() {
		return residentCountry;
	}
	
	public static AbstractPersonDTOBuilder createAbstractPersonDTO() {
		return new AbstractPersonDTOBuilder();
	}
	
	@SuppressWarnings("unchecked")
	public static class AbstractPersonDTOBuilder {
		
		protected String middleName;
		protected String firstName;
		protected String lastName;
		@Enumerated(EnumType.STRING)
		protected Sex sex;
		@Temporal(TemporalType.DATE)
		protected Date dateOfBirth;
		@Enumerated(EnumType.STRING)
		protected MaritalStatus maritalStatus;
		protected String mobileNumber;
		protected String email;
		protected String birthCountry;
		protected String residentCountry;
		
		public <T extends AbstractPersonDTOBuilder> T  setMiddleName(String middleName) {
			this.middleName = middleName;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setFirstName(String firstName) {
			this.firstName = firstName;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setLastName(String lastName) {
			this.lastName = lastName;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setSex(Sex sex) {
			this.sex = sex;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setDateOfBirth(Date dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setMaritalStatus(MaritalStatus maritalStatus) {
			this.maritalStatus = maritalStatus;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setEmail(String email) {
			this.email = email;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setBirthCountry(String birthCountry) {
			this.birthCountry = birthCountry;
			return (T) this;
		}
		public <T extends AbstractPersonDTOBuilder> T setResidentCountry(String residentCountry) {
			this.residentCountry = residentCountry;
			return (T) this;
		}
		
	}
	
}
