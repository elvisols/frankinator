/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.TrainingStatusType;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_training_status")
@NamedQueries( {
	
	@NamedQuery(name = "TrainingStatus.findAll", 	
		query = "select c from TrainingStatus c order by c.id "),
	@NamedQuery(name = "TrainingStatus.findByTrainer", 	
	query = "select c from TrainingStatus c where c.training = :training order by c.id ")
	
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingStatus implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="training_status_id_gen",  sequenceName="training_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="training_status_id_gen")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id", nullable=false)
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="training", nullable=false)
	private Trainer training;
	
	@Temporal(TemporalType.DATE)
	@Column(name="status_date", nullable = false)
	private Date statusDate;
	
	@Temporal(TemporalType.TIME)
	@Column(name="status_time", nullable = false)
	private Date statusTime;
	
	@Column(name="message", length=255, nullable = true)
	private String message;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status_type", length = 3, nullable=false)
	private TrainingStatusType statusType;

	public TrainingStatus() {
		// TODO Auto-generated constructor stub
	}

	public TrainingStatus(User user, Trainer training, Date statusDate, Date statusTime, String message,
			TrainingStatusType statusType) {
		this.user = user;
		this.training = training;
		this.statusDate = statusDate;
		this.statusTime = statusTime;
		this.message = message;
		this.statusType = statusType;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the training
	 */
	public Trainer getTraining() {
		return training;
	}

	/**
	 * @param training the training to set
	 */
	public void setTraining(Trainer training) {
		this.training = training;
	}

	/**
	 * @return the statusDate
	 */
	public Date getStatusDate() {
		return statusDate;
	}

	/**
	 * @param statusDate the statusDate to set
	 */
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	/**
	 * @return the statusTime
	 */
	public Date getStatusTime() {
		return statusTime;
	}

	/**
	 * @param statusTime the statusTime to set
	 */
	public void setStatusTime(Date statusTime) {
		this.statusTime = statusTime;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the statusType
	 */
	public TrainingStatusType getStatusType() {
		return statusType;
	}

	/**
	 * @param statusType the statusType to set
	 */
	public void setStatusType(TrainingStatusType statusType) {
		this.statusType = statusType;
	}
	
	

}
