/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 * 
 * Participants Tiers		
	Tier 1	High Digital Knowledge	Knowledgeable about digital requires speci
	Tier 2	Semi knowledge	knows about the web but not it;s use for self improvement or career or job
	Tier 3	NTW	New to web
	Tier x	Mix	Mixed Group		

 *
 */
@Entity
@Table(name="ds_trainee_tier")
@NamedQueries( {
	@NamedQuery(name = "TraineeTier.findAll", query = "select c from TraineeTier c order by c.code"),
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraineeTier implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length=10)
	private String code;
	@Column(length=30, nullable = false, unique = true)
	private String name;
	
	@Column(length=50, nullable = false)
	private String description;
	
	public TraineeTier() {
		
	}
	
	public TraineeTier(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
