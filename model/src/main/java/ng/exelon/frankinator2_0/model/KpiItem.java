/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_kpi_item")
@NamedQueries( {
	@NamedQuery(name = "KpiItem.findAll", query = "select c from KpiItem c order by c.name")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KpiItem implements Serializable {
	
	private static final Long serialVersionUID = 1L;

	@SequenceGenerator(name="kpi_item_id_gen",  sequenceName="kpi_item_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="kpi_item_id_gen")
	private Long id;
	
	@Column(length = 30, name = "name", unique = false, nullable=false)
	private String name; 
	
	@Column(length = 120, name = "description")
	private String description;
		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="kpi", nullable=false)
	private Kpi kpi;

	@Column(nullable=false, name="weight")
	private Float weight;

	public KpiItem() { }
	
	public KpiItem(String name, String description, Kpi kpi, Float weight) {
		this.name = name;
		this.description = description;
		this.kpi = kpi;
		this.weight = weight;
	}
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KpiItem other = (KpiItem) obj;
		if (id != other.id)
			return false;
		return true;
	}


	public Kpi getKpi() {
		return kpi;
	}


	public void setKpi(Kpi kpi) {
		this.kpi = kpi;
	}


	public Float getWeight() {
		return weight;
	}


	public void setWeight(Float weight) {
		this.weight = weight;
	}



}
