/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 * 
 * Participant Groups		
	SMBs			Small and Medium Business owners	
	JS				Job Seekers	Graduates/school leavers who are looking for work
	Students		Current in University/ or Higher education
	Below 18's		Secondary school students

 *
 */
@Entity
@Table(name="ds_trainee_group")
@NamedQueries( {
	@NamedQuery(name = "TraineeGroup.findAll", query = "select c from TraineeGroup c order by c.code"),
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraineeGroup implements Serializable {
	
	@Id
	@Column(length=10)
	private String code;
	
	@Column(length=100, nullable = false)
	private String description;

	public TraineeGroup() {}
	
	public TraineeGroup(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
