/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.KpiInputType;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_kpi")
@NamedQueries( {
	@NamedQuery(name = "Kpi.findAll", query = "select c from Kpi c order by c.id")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Kpi {
	
	@SequenceGenerator(name="kpi_id_gen",  sequenceName="kpi_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="kpi_id_gen")
	private Long id;
	
	@Column(length = 90, name = "name", unique = true, nullable=false)
	private String name; 
	
	@Column(length = 120, name = "description")
	private String description;
	
	@Enumerated(EnumType.STRING)
	@Column(name="input_type", nullable = false)
	private KpiInputType inputType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="category", nullable=false)
	private KpiCategory category;

	public Kpi() { 	}
	
	public Kpi(String name, String description, KpiInputType inputType, KpiCategory category) {
		this.name = name;
		this.description = description;
		this.inputType = inputType;
		this.category = category;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public KpiCategory getCategory() {
		return category;
	}


	public void setCategory(KpiCategory category) {
		this.category = category;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kpi other = (Kpi) obj;
		if (id != other.id)
			return false;
		return true;
	}


	public KpiInputType getInputType() {
		return inputType;
	}


	public void setInputType(KpiInputType inputType) {
		this.inputType = inputType;
	}


}
