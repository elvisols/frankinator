/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.TrainingStatusType;


/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingStatusDTO implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	private Long id;
	
	private User user;
	
	private Trainer training;
	
	private Date statusDate;
	
	private Date statusTime;
	
	private String message;
	
	@Enumerated(EnumType.STRING)
	private TrainingStatusType statusType;

	public TrainingStatusDTO() {
		// TODO Auto-generated constructor stub
	}

	public TrainingStatusDTO(Long id, User user, Trainer training, Date statusDate,
			Date statusTime, String message, TrainingStatusType statusType) {
		this.id = id;
		this.user = user;
		this.training = training;
		this.statusDate = statusDate;
		this.statusTime = statusTime;
		this.message = message;
		this.statusType = statusType;
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public Trainer getTraining() {
		return training;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public Date getStatusTime() {
		return statusTime;
	}

	public String getMessage() {
		return message;
	}

	public TrainingStatusType getStatusType() {
		return statusType;
	}
	
	public static TrainingStatusDTOBuilder newBuilder() {
		return new TrainingStatusDTOBuilder();
	}

	public static class TrainingStatusDTOBuilder {
		private Long id;
		private User user;
		private Trainer training;
		private Date statusDate;
		private Date statusTime;
		private String message;
		@Enumerated(EnumType.STRING)
		private TrainingStatusType statusType;
		
		public TrainingStatusDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public TrainingStatusDTOBuilder setUser(User user) {
			this.user = user;
			return this;
		}
		public TrainingStatusDTOBuilder setTraining(Trainer training) {
			this.training = training;
			return this;
		}
		public TrainingStatusDTOBuilder setStatusDate(Date statusDate) {
			this.statusDate = statusDate;
			return this;
		}
		public TrainingStatusDTOBuilder setStatusTime(Date statusTime) {
			this.statusTime = statusTime;
			return this;
		}
		public TrainingStatusDTOBuilder setMessage(String message) {
			this.message = message;
			return this;
		}
		public TrainingStatusDTOBuilder setStatusType(TrainingStatusType statusType) {
			this.statusType = statusType;
			return this;
		}
		public TrainingStatusDTO createTrainingStatusDTO() {
			return new TrainingStatusDTO();
		}
	}

}
