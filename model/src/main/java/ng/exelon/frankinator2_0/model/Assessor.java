/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_assessor")
@NamedQueries( {
	
	@NamedQuery(name = "Assessor.findAll", 	
		query = "select c from Assessor c order by c.id "),
	@NamedQuery(name = "Assessor.findByMobileNumber", 	
	query = "select c from Assessor c where c.mobileNumber = :phone order by c.id "),
	@NamedQuery(name = "Assessor.findAssessorByEmail", 	
		query = "select c from Assessor c where lower(c.email) = :email order by c.id ")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Assessor extends AbstractPerson {
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(name="assessor_id_gen",  sequenceName="assessor_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="assessor_id_gen")
	private Long id;
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="partner", nullable=false)
	private Partner partner;
*/
	
	public Assessor() { 
		super();
	}
	
	public Assessor(String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry) { 
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	

}
