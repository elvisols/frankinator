/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_training")
@NamedQueries( {
	
	@NamedQuery(name = "Training.findAll", 	
		query = "select t from Training t order by t.id "),
	@NamedQuery(name = "Training.findByTrainer", 	
	query = "select c from Training c where c.trainer = :trainer order by c.id ")
	
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Training implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@SequenceGenerator(name="training_id_gen",  sequenceName="training_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="training_id_gen")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="trainer", nullable=false)
	private Trainer trainer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="partner", nullable=false)
	private Partner partner;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tier", nullable=false)
	private TraineeTier tier;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="audience", nullable=false)
	private TraineeGroup audience;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="training_type", nullable=false)
//	private TrainingTypeDeleted typeOfTraining;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="course", nullable=false)
	private Course course;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="scheduled_by", nullable=false)
	private User scheduledBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="approved_by", nullable=true)
	private User approvedBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="request_date", nullable = false)
	private Date requestDate;
	
	@Temporal(TemporalType.TIME)
	@Column(name="request_time", nullable = false)
	private Date requestTime;
	
	@Temporal(TemporalType.DATE)
	@Column(name="approved_date", nullable = true)
	private Date approvedDate;
	
	@Temporal(TemporalType.TIME)
	@Column(name="approved_time", nullable = true)
	private Date approvedTime;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="country", nullable=false)
	private Country country;
	
	@Column(name="loc_lng")
	private Float longitude;
	
	@Column(name="loc_lat")
	private Float latitude;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="language", nullable=false)
	private Language language;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name="scheduled_date", nullable = false)
	private Date scheduledDate;
	
	@Temporal(TemporalType.TIME)
	@Column(name="scheduled_time", nullable = false)
	private Date scheduledTime;
	
	@Column(name="duration", nullable = false)
	private Integer durationInMins;
	
	@Column(name="expected_attendees", nullable = false)
	private Integer expectedAttendees;
	
	@Column(name="address", length=60, nullable=false)
	private String address;
	
	public Training() { }
	
	public Training(Trainer trainer, Partner partner, TraineeTier tier, TraineeGroup audience, Course course, User scheduledBy, User approvedBy, Date requestDate, Date requestTime, Date approvedDate, Date approvedTime, Country country, Float longitude, Float latitude, Language language, Date scheduledDate, Date scheduledTime, Integer durationInMins, Integer expectedAttendees, String address) {
		this.trainer = trainer;
		this.partner = partner;
		this.tier = tier;
		this.audience = audience;
		this.course = course;
		this.scheduledBy = scheduledBy;
		this.approvedBy = approvedBy;
		this.requestDate = requestDate;
		this.requestTime = requestTime;
		this.approvedDate = approvedDate;
		this.approvedTime = approvedTime;
		this.country = country;
		this.longitude = longitude;
		this.latitude = latitude;
		this.language = language;
		this.scheduledDate = scheduledDate;
		this.scheduledTime = scheduledTime;
		this.durationInMins = durationInMins;
		this.expectedAttendees = expectedAttendees;
		this.address = address;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public TraineeTier getTier() {
		return tier;
	}

	public void setTier(TraineeTier tier) {
		this.tier = tier;
	}

	public TraineeGroup getAudience() {
		return audience;
	}

	public void setAudience(TraineeGroup group) {
		this.audience = group;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public User getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(User scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	public User getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(User approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public Date getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(Date scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Date getApprovedTime() {
		return approvedTime;
	}

	public void setApprovedTime(Date approvedTime) {
		this.approvedTime = approvedTime;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date trainingDate) {
		this.requestDate = trainingDate;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date trainingTime) {
		this.requestTime = trainingTime;
	}

	public Integer getDurationInMins() {
		return durationInMins;
	}

	public void setDurationInMins(Integer durationInMins) {
		this.durationInMins = durationInMins;
	}

	public Integer getExpectedAttendees() {
		return expectedAttendees;
	}

	public void setExpectedAttendees(Integer expectedAttendees) {
		this.expectedAttendees = expectedAttendees;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}




}
