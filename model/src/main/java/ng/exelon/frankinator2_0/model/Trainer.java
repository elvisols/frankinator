/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_trainer")
@NamedQueries( {
	
	@NamedQuery(name = "Trainer.findAll", 	
		query = "select c from Trainer c order by c.id "),
	@NamedQuery(name = "Trainer.findByMobileNumber", 	
	query = "select c from Trainer c where c.mobileNumber = :phone order by c.id "),
	@NamedQuery(name = "Trainer.findTrainerByEmail", 	
		query = "select c from Trainer c where lower(c.email) = :email order by c.id ")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Trainer extends AbstractPerson {
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="trainer_id_gen",  sequenceName="trainer_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="trainer_id_gen")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="partner", nullable=false)
	private Partner partner;

	/*@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "trainer")
	private List<Trainee> trainees;*/
	
	public Trainer() {
		super();
	}
	
	public Trainer(String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry, Partner partner) {
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		this.partner = partner;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	
}
