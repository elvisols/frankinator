package ng.exelon.frankinator2_0.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import ng.exelon.frankinator2_0.model.value.RoleEnum;

import java.io.Serializable;

/**
 * The persistent class for the ds_user_role database table.
 * 
 */
@Entity
@Table(name="ds_user_role")
@NamedQuery(name="Role.findAll", query="SELECT d FROM Role d")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Role implements Serializable {

	@Id
	@SequenceGenerator(name="DS_USER_ROLE_ID_GENERATOR", sequenceName="USERID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DS_USER_ROLE_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(length=50)
	@NotNull(message = "Role can not be null!")
	@Enumerated(EnumType.STRING)
	private RoleEnum role;

	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="email", referencedColumnName="email", nullable = true)
//	@JoinColumns({
//	    @JoinColumn(name="DEPT_NAME", referencedColumnName="Name"),
//	    @JoinColumn(name="DEPT_ID", referencedColumnName="ID")
//	})
	private User user;

	public Role() {
	}
	
	public Role(RoleEnum role) {
		this.role = role;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RoleEnum getRole() {
		return this.role;
	}

	public void setRole(RoleEnum role) {
		this.role = role;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", role=" + role + ", user=" + user.getEmail() + "]";
	}

}