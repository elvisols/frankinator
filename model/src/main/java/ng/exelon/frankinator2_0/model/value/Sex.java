package ng.exelon.frankinator2_0.model.value;

public enum Sex {
	M("Male"),
	F("Female");
	
	private String description;
	
	private Sex(String sex){
		this.description = sex;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}
