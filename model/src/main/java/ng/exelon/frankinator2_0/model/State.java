/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_ref_state")
@NamedQueries( {
	@NamedQuery(name = "State.findAll", 	query = "select c from State c  order by c.id ")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class State implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="state_id_gen",  sequenceName="state_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="state_id_gen")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="country", nullable=false)
	private Country country;

	
	@Column(length = 20, name = "state_code", unique = true, nullable = true)
	private String code;
	
	@Column(length = 50, name = "name", unique = true, nullable = false)
	private String name;
	
	@Column(length = 30, name = "capital", unique = false)
	private String capital;
	
	public State() { }
	
	public State(Country country, String code, String name, String capital) {
		this.country = country;
		this.code = code;
		this.name = name;
		this.capital = capital;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
}
