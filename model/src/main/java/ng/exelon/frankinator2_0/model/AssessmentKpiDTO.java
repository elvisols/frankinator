/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssessmentKpiDTO {
	
	private Long id;

	private Assessment assessment;

	private Kpi kpi;
	
	private Float value;

	public AssessmentKpiDTO() {
		
	}
	
	public AssessmentKpiDTO(Long id, Assessment assessment, Kpi kpi, Float value) {
		this.id = id;
		this.assessment = assessment;
		this.kpi = kpi;
		this.value = value;
	}
	
	public Long getId() {
		return id;
	}

	public Assessment getAssessment() {
		return assessment;
	}


	public Kpi getKpi() {
		return kpi;
	}

	public Float getValue() {
		return value;
	}

	public static AssessmentKpiDTOBuilder newBuilder() {
		return new AssessmentKpiDTOBuilder();
	}
	
	public static class AssessmentKpiDTOBuilder {
		private Long id;
		private Assessment assessment;
		private Kpi kpi;
		private Float value;
		// Setters
		public AssessmentKpiDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public AssessmentKpiDTOBuilder setAssessment(Assessment assessment) {
			this.assessment = assessment;
			return this;
		}
		public AssessmentKpiDTOBuilder setKpi(Kpi kpi) {
			this.kpi = kpi;
			return this;
		}
		public AssessmentKpiDTOBuilder setValue(Float value) {
			this.value = value;
			return this;
		}
		public AssessmentKpiDTO createAssessmentKpiDTO() {
			return new AssessmentKpiDTO(id, assessment, kpi, value);
		}
	}
	
}
