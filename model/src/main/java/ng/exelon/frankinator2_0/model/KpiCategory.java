/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_kpi_category")
@NamedQueries( {
	@NamedQuery(name = "KpiCategory.findAll", query = "select kc from KpiCategory kc order by kc.name")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KpiCategory implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id	
	@Column(length=15)
	private String id;
	
	@Column(length = 30, name = "name", unique = true)
	private String name; 
	
	@Column(length = 120, name = "description")
	private String description;

	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "category")
	private List<Kpi> kpis;
	
	public KpiCategory() {	}
	
	public KpiCategory(String id, String name, String description, List<Kpi> kpis) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.kpis = kpis;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Kpi> getKpis() {
		return kpis;
	}

	public void setKpis(List<Kpi> kpis) {
		this.kpis = kpis;
	}
	
}
