package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Parameter
 *
 */
@Entity
@Table(name="ds_parameter")
public class Parameter implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String DEFAULT="system";
	
	@Id
	@Column(length=10)
	private String id;
	
	
	@Column()
	private Integer timeout;
	
	
	
	public Parameter() {
		super();
	}   
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public Integer getTimeout() {
		return timeout;
	}
	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}
	
}
