/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_course_tier")
@NamedQueries( {
	@NamedQuery(name = "CourseTier.findAll", query = "select c from CourseTierDeleted c order by c.code"),
})
public class CourseTierDeleted implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length=10)
	private String code;
	
	@Column(length=50, nullable = false)
	private String description;
	

	public CourseTierDeleted() { }
	
	public CourseTierDeleted(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
