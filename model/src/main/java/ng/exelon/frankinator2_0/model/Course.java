/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_course")
@NamedQueries( {
	@NamedQuery(name = "Course.findAll", query = "select c from Course c order by c.name"),
	@NamedQuery(name = "Course.findAllByCourseName"
		, query = "select c from Course c where lower(c.name) like lower(concat('%',concat(:name,'%')))  order by c.name"),
	@NamedQuery(name = "Course.findAllByDate", query = "select c from Course c where c.dateAdded = :responseDate"),
	@NamedQuery(name = "Course.findAllBetweenDates", 
		query = "select c from Course c where c.dateAdded between :startDate and :endDate")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Course implements Serializable {
	private static final Long serialVersionUID = 1L;

	@SequenceGenerator(name="course_id_gen",  sequenceName="course_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="course_id_gen")
	private Long id;
	
	@Column(length = 30, name = "name", unique = true)
	private String name; 
	
	@Column(length = 120, name = "description")
	private String description;
	
//	@ManyToOne
//	@JoinColumn(name="tier", nullable=false)
//	private CourseTierDeleted tier;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_added")
	private Date dateAdded;
	
	public Course() {
	}
	
	public Course(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	@XmlTransient
	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
