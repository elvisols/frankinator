/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerDTO extends CorporateUserDTO {
	
	private Long id;

	private List<Trainer> trainers;
	
	private List<PartnerEmployee> employees;
	
	public PartnerDTO() {	
		super();
	}
	
	public PartnerDTO(Long id, String name, String contactPhone, String rcNumber, Date incDate, String mobileNumber, String email, String webSite, Address address, String gis, String shortName, String description, Date dateAdded, List<Trainer> trainer, List<PartnerEmployee> employee) {	
		super(name, contactPhone, rcNumber, incDate, mobileNumber, email, webSite, address, gis, shortName, description, dateAdded);
		this.trainers = trainers;
		this.employees = employees;
	}

	public Long getId() {
		return id;
	}

	public List<Trainer> getTrainers() {
		return trainers;
	}

	public List<PartnerEmployee> getEmployees() {
		return employees;
	}
	
	public static PartnerDTOBuilder newBuilder() {
		return new PartnerDTOBuilder();
	}
	
	public static class PartnerDTOBuilder extends CorporateUserDTOBuilder {
		private Long id;
		private List<Trainer> trainers;
		private List<PartnerEmployee> employees;
		
		public PartnerDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public PartnerDTOBuilder setTrainers(List<Trainer> trainers) {
			this.trainers = trainers;
			return this;
		}
		public PartnerDTOBuilder setEmployees(List<PartnerEmployee> employees) {
			this.employees = employees;
			return this;
		}
		public PartnerDTO createPartnerDTO() {
			return new PartnerDTO(id, name, contactPhone, rcNumber, incDate, mobileNumber, email, webSite, address, gis, shortName, description, dateAdded, trainers, employees);
		}
		
	}

}
