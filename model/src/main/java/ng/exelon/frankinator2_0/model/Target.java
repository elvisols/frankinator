/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.TargetQuarter;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_target")
@NamedQueries( {
	@NamedQuery(name = "Target.findAll", query = "select c from Target c order by c.id"),
	@NamedQuery(name = "Target.findByPartner", query = "select c from Target c where c.partner.id = :partner order by c.id")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Target implements Serializable {
	
	private static final Long serialVersionUID = 1L;

	@SequenceGenerator(name="target_id_gen",  sequenceName="target_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="target_id_gen")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="partner", nullable=false)
	private Partner partner;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="course", nullable=false)
	private Course course;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="country", nullable=false)
	private Country country;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="training_tier", nullable=true)
	private TraineeTier tier;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="training_group", nullable=true)
	private TraineeGroup group;
	
	@Column(length = 90, name = "name", unique = false, nullable=true)
	private String name; 
	
	@Column(length = 120, name = "description")
	private String description;
	
	@Column(name="year")
	private Integer targetYear;
	
	@Column(name="target_value", nullable=false)
	private Long targetValue;
	
	@Enumerated(EnumType.STRING)
	@Column(name="quarter", length=2)
	private TargetQuarter quarter;
	
	@Temporal(TemporalType.DATE)
	@Column(name="start_date", nullable = true)
	private Date startDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="end_date", nullable = true)
	private Date endDate;

	public Target() { }
	
	public Target(Partner partner, Course course, Country country, TraineeTier tier, TraineeGroup group, String name, String description, Integer targetYear, Long targetValue, TargetQuarter quarter, Date startDate, Date endDate) {
		this.partner = partner;
		this.course = course;
		this.country = country;
		this.tier = tier;
		this.group = group;
		this.name = name;
		this.description = description;
		this.targetYear = targetYear;
		this.targetValue = targetValue;
		this.quarter = quarter;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getTargetYear() {
		return targetYear;
	}

	public void setTargetYear(Integer targetYear) {
		this.targetYear = targetYear;
	}

	public TargetQuarter getQuarter() {
		return quarter;
	}

	public void setQuarter(TargetQuarter quarter) {
		this.quarter = quarter;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public TraineeTier getTier() {
		return tier;
	}

	public void setTier(TraineeTier tier) {
		this.tier = tier;
	}

	public TraineeGroup getGroup() {
		return group;
	}

	public void setGroup(TraineeGroup group) {
		this.group = group;
	}

	public Long getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(Long targetValue) {
		this.targetValue = targetValue;
	}
	
	

}
