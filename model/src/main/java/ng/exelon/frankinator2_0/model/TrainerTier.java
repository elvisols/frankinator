/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 * 
 * --Trainer tiers		
--Tier 1	Expert knowledge		Can train any tier of participants
--Tier 2	Medium knowledge		Can train tier 2 and 3 only
--Tier 3 	Sufficient knowledge	Can train tier 1 only - new to web
		

 *
 */
@Entity
@Table(name="ds_trainer_tier")
@NamedQueries( {
	@NamedQuery(name = "TrainerTier.findAll", query = "select c from TrainerTier c order by c.code"),
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainerTier implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length=10)
	private String code;
	
	@Column(length=30, nullable = false, unique = true)
	private String name;
	
	@Column(length=100, nullable = false)
	private String description;

	public TrainerTier() {
	}
	
	public TrainerTier(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
