/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;


/**
 * @author ukaegbu
 *
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public abstract class AbstractEmbeddable {

	public AbstractEmbeddable() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Embeddable
	public class Address {
		
		@Column(length = 4, name = "country", unique = false)
		protected String residentCountry;
		
		@Column(length = 200, name = "address", unique = false, nullable = false)
		protected String address;
		
		@Column(length = 30, name = "city", unique = false)
		protected String city;
		
		@Column(length = 50, name = "state", unique = false)
		protected String state;
		
		public Address() {
			super();
		}
		
		public Address(String residentCountry, String address, String city, String state) {
			this.residentCountry = residentCountry;
			this.address = address;
			this.city = city;
			this.state = state;
		}

		public String getResidentCountry() {
			return residentCountry;
		}

		public void setResidentCountry(String residentCountry) {
			this.residentCountry = residentCountry;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}
	}
}

