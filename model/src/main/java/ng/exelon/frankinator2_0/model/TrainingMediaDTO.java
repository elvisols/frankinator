/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MediaType;


/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingMediaDTO implements Serializable {
	
	private static final Long serialVersionUID = 1L;
	
	private Long id;
	
	private TrainingUpload training;

	private MediaType mediaType;
	
	private String filename;
	
	private Date uploadDate;
	
	private Date uploadTime;
	
	public TrainingMediaDTO() { }
	
	public TrainingMediaDTO(Long id, TrainingUpload training, MediaType mediaType, String filename, Date uploadDate, Date uploadTime) {
		this.id = id;
		this.training = training;
		this.mediaType = mediaType;
		this.filename = filename;
		this.uploadDate = uploadDate;
		this.uploadTime = uploadTime;
	}

	public Long getId() {
		return id;
	}

	public TrainingUpload getTraining() {
		return training;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public String getFilename() {
		return filename;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public Date getUploadTime() {
		return uploadTime;
	}
	
	public static TrainingMediaDTOBuilder newBuilder() {
		return new TrainingMediaDTOBuilder();
	}

	public static class TrainingMediaDTOBuilder {
		private Long id;
		private TrainingUpload training;
		private MediaType mediaType;
		private String filename;
		private Date uploadDate;
		private Date uploadTime;
		
		public TrainingMediaDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public TrainingMediaDTOBuilder setTraining(TrainingUpload training) {
			this.training = training;
			return this;
		}
		public TrainingMediaDTOBuilder setMediaType(MediaType mediaType) {
			this.mediaType = mediaType;
			return this;
		}
		public TrainingMediaDTOBuilder setFilename(String filename) {
			this.filename = filename;
			return this;
		}
		public TrainingMediaDTOBuilder setUploadDate(Date uploadDate) {
			this.uploadDate = uploadDate;
			return this;
		}
		public TrainingMediaDTOBuilder setUploadTime(Date uploadTime) {
			this.uploadTime = uploadTime;
			return this;
		}
		public TrainingMediaDTO createTrainingMediaDTO() {
			return new TrainingMediaDTO(id, training, mediaType, filename, uploadDate, uploadTime);
		}
	}

}
