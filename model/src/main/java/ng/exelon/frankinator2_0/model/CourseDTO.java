/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.TierEnum;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CourseDTO implements Serializable {
	private static final Long serialVersionUID = 1L;

	private Long id;
	
	@NotNull(message = "Course name must not be null.")
	private String name; 
	
	@NotNull(message = "Course description must not be null.")
	private String description;
	
	private TierEnum tier;
	
	
	public TierEnum getTier() {
		return tier;
	}

	public void setTier(TierEnum tier) {
		this.tier = tier;
	}

	private ZonedDateTime dateAdded = ZonedDateTime.now();
	
	public CourseDTO() {
	}
	
	public CourseDTO(Long id, String name, String description, TierEnum tier) {
		this.name = name;
		this.description = description;
		this.tier = tier;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public ZonedDateTime getDateAdded() {
		return this.dateAdded;
	}

	public static CourseDTOBuilder newBuilder() {
		return new CourseDTOBuilder();
	}
	
	public static class CourseDTOBuilder {
		
		private Long id;
		private String name; 
		private String description;
		private TierEnum tier;
		
		public CourseDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		
		public CourseDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		
		public CourseDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		
		public CourseDTOBuilder setTier(TierEnum tier) {
			this.tier = tier;
			return this;
		}
		
		public CourseDTO createCourseDTO() {
			return new CourseDTO(id, name, description, tier);
		}
		
	}

}
