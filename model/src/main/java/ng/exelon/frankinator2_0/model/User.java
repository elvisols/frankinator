package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import ng.exelon.frankinator2_0.model.value.Sex;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;


/**
 * The persistent class for the ds_user database table.
 * 
 */
@Entity
@Table(name="ds_user")
@NamedQuery(name="User.findAll", query="SELECT d FROM User d")
@Cacheable
@SQLDelete(sql="update user set enabled=false where id=?")
@Where(clause="enabled = true")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User implements Serializable {

	@Id
	@SequenceGenerator(name="DS_USER_ID_GENERATOR", sequenceName="USERID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DS_USER_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(name="created_at", nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime createdAt = ZonedDateTime.now();

	@NotNull
	private Boolean enabled;

	@NotNull(message = "Email can not be null!")
	private String email;

	@Column(name="first_name", length=30)
	@NotNull(message = "First name can not be null!")
	private String firstName;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "profile_start_date")
	private Date profileStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "profile_end_date")
	private Date profileEndDate;
	
	@Enumerated(EnumType.STRING)
	@Column(name="sex", length=10)
	private Sex sex;

	@Temporal(TemporalType.DATE)
	@Column(name="last_log_date")
	private Date lastLogDate;

	@Column(name="last_log_time")
	private Time lastLogTime;

	@Column(name="last_name", length=30)
	@NotNull(message = "Last name can not be null!")
	private String lastName;

	@Column(name="logged_in_ip", length=30)
	private String loggedInIp;

	@Column(name="mobile_number", length=30)
	@NotNull(message = "Mobile number can not be null!")
	private String mobileNumber;

	@Column(length=160)
	@NotNull(message = "Password can not be null!")
	private String password;

	@Column(name="updated_at")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime updatedAt = ZonedDateTime.now();
//	private Timestamp updatedAt;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy="user", fetch=FetchType.LAZY)
	@Cascade({CascadeType.ALL})
	@OrderBy("role ASC")
	private List<Role> roles;

	public User() {
	}
	
	public User(String email, String password, String mobileNumber, String firstname, String lastname, Sex sex, Date profileStartDate, Date profileEndDate) {
		this.email = email;
        this.password = password;
        this.enabled = true;
        this.mobileNumber = mobileNumber;
        this.firstName = firstname;
        this.lastName = lastname;
        this.sex = sex;
        this.profileStartDate = profileStartDate;
        this.profileEndDate = profileEndDate;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getLastLogDate() {
		return this.lastLogDate;
	}

	public void setLastLogDate(Date lastLogDate) {
		this.lastLogDate = lastLogDate;
	}

	public Time getLastLogTime() {
		return this.lastLogTime;
	}

	public void setLastLogTime(Time lastLogTime) {
		this.lastLogTime = lastLogTime;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLoggedInIp() {
		return this.loggedInIp;
	}

	public void setLoggedInIp(String loggedInIp) {
		this.loggedInIp = loggedInIp;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ZonedDateTime getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(ZonedDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		role.setUser(this);
		getRoles().add(role);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setUser(null);
		return role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", createdAt=" + createdAt + ", enabled=" + enabled + ", email=" + email
				+ ", firstName=" + firstName + ", lastLogDate=" + lastLogDate + ", lastLogTime=" + lastLogTime
				+ ", lastName=" + lastName + ", loggedInIp=" + loggedInIp + ", mobileNumber=" + mobileNumber
				+ ", password=" + password + ", updatedAt=" + updatedAt + ", roles=" + roles.size() + "]";
	}

	public Date getProfileStartDate() {
		return profileStartDate;
	}

	public void setProfileStartDate(Date profileStartDate) {
		this.profileStartDate = profileStartDate;
	}

	public Date getProfileEndDate() {
		return profileEndDate;
	}

	public void setProfileEndDate(Date profileEndDate) {
		this.profileEndDate = profileEndDate;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

}