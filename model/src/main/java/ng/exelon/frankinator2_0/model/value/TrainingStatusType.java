package ng.exelon.frankinator2_0.model.value;

public enum TrainingStatusType {

	P("Pending"),
	RJ("Rejected"),
	RA("ReAssigned"),
	CX("Cancelled"),
	C("Closed");
	
	private final String type;
	
	TrainingStatusType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return this.type;
	}
	
}
