/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;


/**
 * @author ukaegbu
 *
 */
@MappedSuperclass
public abstract class AbstractPerson implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="middle_name", length=50)
	protected String middleName;
	
	@Column(name="first_name", length=50, nullable=false)
	protected String firstName;
	
	@Column(name="last_name", length=50, nullable=false)
	protected String lastName;
	
	@Enumerated(EnumType.STRING)
	@Column(name="sex", length=2, nullable=false)
	protected Sex sex;
	
	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth", nullable=true)
	protected Date dateOfBirth;
	
	@Enumerated(EnumType.STRING)
	@Column(name="marital_status", length=10, nullable=true)
	protected MaritalStatus maritalStatus;
	
	@Column(name="mobile_number", length=30, nullable=false)
	protected String mobileNumber;
	
	@Column(name="email", length=80, nullable=false)
	protected String email;
	
	@Column(name="birth_country", length=2, nullable=true)
	protected String birthCountry;
	
	@Column(name="resident_country", length=2, nullable=true)
	protected String residentCountry;
	
	public AbstractPerson() { }

	public AbstractPerson(String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry) { 
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.sex = sex;
		this.dateOfBirth = dateOfBirth;
		this.maritalStatus = maritalStatus;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.birthCountry = birthCountry;
		this.residentCountry = residentCountry;
	}
	
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the sex
	 */
	public Sex getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(Sex sex) {
		this.sex = sex;
	}
	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	/**
	 * @return the maritalStatus
	 */
	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}
	/**
	 * @param maritalStatus the maritalStatus to set
	 */
	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the birthCountry
	 */
	public String getBirthCountry() {
		return birthCountry;
	}
	/**
	 * @param birthCountry the birthCountry to set
	 */
	public void setBirthCountry(String birthCountry) {
		this.birthCountry = birthCountry;
	}
	/**
	 * @return the residentCountry
	 */
	public String getResidentCountry() {
		return residentCountry;
	}
	/**
	 * @param residentCountry the residentCountry to set
	 */
	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}
	
	
}
