package ng.exelon.frankinator2_0.model.value;

public enum MaritalStatus {
		
	SINGLE("Single"),
	MARRIED("Married"),
	DIVORCED("Divorced"),
	WIDOWED("Widowed");
	
	private String description;
	
	private MaritalStatus(String sex){
		this.description = sex;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}
