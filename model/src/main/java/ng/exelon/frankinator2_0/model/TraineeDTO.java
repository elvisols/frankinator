/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraineeDTO extends AbstractPersonDTO {

	private Long id;

	private TrainingUpload trainingUpload;

	public TraineeDTO() {
		super();
	}
	
	public TraineeDTO(Long id, String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry, TrainingUpload trainingUpload) {
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		this.trainingUpload = trainingUpload;
	}
	
	public Long getId() {
		return id;
	}

	public TrainingUpload getTrainingUpload() {
		return trainingUpload;
	}

	public static TraineeDTOBuilder newBuilder() {
		return new TraineeDTOBuilder();
	}
	
	public static class TraineeDTOBuilder extends AbstractPersonDTOBuilder {
		private Long id;
		private TrainingUpload trainingUpload;
		
		public TraineeDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public TraineeDTOBuilder setTrainingUpload(TrainingUpload trainingUpload) {
			this.trainingUpload = trainingUpload;
			return this;
		}
		public TraineeDTO createTraineeDTO() {
			return new TraineeDTO(id, firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry, trainingUpload);
		}
	}
	
}
