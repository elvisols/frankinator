/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private Trainer trainer;

	private Partner partner;

	private TraineeTier tier;

	private TraineeGroup audience;
	
//	private TrainingTypeDeleted typeOfTraining;
	
	private Course course;
	
	private User scheduledBy;
	
	private User approvedBy;
	
	private Date requestDate;
	
	private Date requestTime;
	
	private Date approvedDate;
	
	private Date approvedTime;
	
	private Country country;
	
	private Float longitude;
	
	private Float latitude;
	
	private Language language;
	
	private Date scheduledDate;
	
	private Date scheduledTime;
	
	private Integer durationInMins;
	
	private Integer expectedAttendees;
	
	private String address;
	
	public TrainingDTO() { }
	
	public TrainingDTO(Long id, Trainer trainer, Partner partner, TraineeTier tier, TraineeGroup audience, Course course, User scheduledBy, User approvedBy, Date requestDate, Date requestTime, Date approvedDate, Date approvedTime, Country country, Float longitude, Float latitude, Language language, Date scheduledDate, Date scheduledTime, Integer durationInMins, Integer expectedAttendees, String address) {
		this.id = id;
		this.trainer = trainer;
		this.partner = partner;
		this.tier = tier;
		this.audience = audience;
		this.course = course;
		this.scheduledBy = scheduledBy;
		this.approvedBy = approvedBy;
		this.requestDate = requestDate;
		this.requestTime = requestTime;
		this.approvedDate = approvedDate;
		this.approvedTime = approvedTime;
		this.country = country;
		this.longitude = longitude;
		this.latitude = latitude;
		this.language = language;
		this.scheduledDate = scheduledDate;
		this.scheduledTime = scheduledTime;
		this.durationInMins = durationInMins;
		this.expectedAttendees = expectedAttendees;
		this.address = address;
	}
	
	public Long getId() {
		return id;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public TraineeTier getTier() {
		return tier;
	}

	public TraineeGroup getAudience() {
		return audience;
	}

	public Course getCourse() {
		return course;
	}

	public Partner getPartner() {
		return partner;
	}

	public User getScheduledBy() {
		return scheduledBy;
	}

	public User getApprovedBy() {
		return approvedBy;
	}

	public Date getScheduledDate() {
		return scheduledDate;
	}

	public Date getScheduledTime() {
		return scheduledTime;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public Date getApprovedTime() {
		return approvedTime;
	}

	public Country getCountry() {
		return country;
	}

	public Language getLanguage() {
		return language;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public Integer getDurationInMins() {
		return durationInMins;
	}

	public Integer getExpectedAttendees() {
		return expectedAttendees;
	}

	public String getAddress() {
		return address;
	}

	public Float getLongitude() {
		return longitude;
	}

	public Float getLatitude() {
		return latitude;
	}

	public static TrainingDTOBuilder newBuilder() {
		return new TrainingDTOBuilder();
	}

	public static class TrainingDTOBuilder {
		private Long id;
		private Trainer trainer;
		private Partner partner;
		private TraineeTier tier;
		private TraineeGroup audience;
//		private TrainingTypeDeleted typeOfTraining;
		private Course course;
		private User scheduledBy;
		private User approvedBy;
		private Date requestDate;
		private Date requestTime;
		private Date approvedDate;
		private Date approvedTime;
		private Country country;
		private Float longitude;
		private Float latitude;
		private Language language;
		private Date scheduledDate;
		private Date scheduledTime;
		private Integer durationInMins;
		private Integer expectedAttendees;
		private String address;
		
		public TrainingDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public TrainingDTOBuilder setTrainer(Trainer trainer) {
			this.trainer = trainer;
			return this;
		}
		public TrainingDTOBuilder setPartner(Partner partner) {
			this.partner = partner;
			return this;
		}
		public TrainingDTOBuilder setTier(TraineeTier tier) {
			this.tier = tier;
			return this;
		}
		public TrainingDTOBuilder setAudience(TraineeGroup audience) {
			this.audience = audience;
			return this;
		}
		public TrainingDTOBuilder setCourse(Course course) {
			this.course = course;
			return this;
		}
		public TrainingDTOBuilder setScheduledBy(User scheduledBy) {
			this.scheduledBy = scheduledBy;
			return this;
		}
		public TrainingDTOBuilder setApprovedBy(User approvedBy) {
			this.approvedBy = approvedBy;
			return this;
		}
		public TrainingDTOBuilder setRequestDate(Date requestDate) {
			this.requestDate = requestDate;
			return this;
		}
		public TrainingDTOBuilder setRequestTime(Date requestTime) {
			this.requestTime = requestTime;
			return this;
		}
		public TrainingDTOBuilder setApprovedDate(Date approvedDate) {
			this.approvedDate = approvedDate;
			return this;
		}
		public TrainingDTOBuilder setApprovedTime(Date approvedTime) {
			this.approvedTime = approvedTime;
			return this;
		}
		public TrainingDTOBuilder setCountry(Country country) {
			this.country = country;
			return this;
		}
		public TrainingDTOBuilder setLongitude(Float longitude) {
			this.longitude = longitude;
			return this;
		}
		public TrainingDTOBuilder setLatitude(Float latitude) {
			this.latitude = latitude;
			return this;
		}
		public TrainingDTOBuilder setLanguage(Language language) {
			this.language = language;
			return this;
		}
		public TrainingDTOBuilder setScheduledDate(Date scheduledDate) {
			this.scheduledDate = scheduledDate;
			return this;
		}
		public TrainingDTOBuilder setScheduledTime(Date scheduledTime) {
			this.scheduledTime = scheduledTime;
			return this;
		}
		public TrainingDTOBuilder setDurationInMins(Integer durationInMins) {
			this.durationInMins = durationInMins;
			return this;
		}
		public TrainingDTOBuilder setExpectedAttendees(Integer expectedAttendees) {
			this.expectedAttendees = expectedAttendees;
			return this;
		}
		public TrainingDTOBuilder setAddress(String address) {
			this.address = address;
			return this;
		}
		public TrainingDTO createTrainingDTO() {
			return new TrainingDTO(id, trainer, partner, tier, audience, course, scheduledBy, approvedBy, requestDate, requestTime, approvedDate,  approvedTime, country, longitude, latitude, language, scheduledDate, scheduledTime, durationInMins, expectedAttendees, address);
		}
	}

}
