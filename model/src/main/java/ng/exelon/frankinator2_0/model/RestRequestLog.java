/**
 * 
 */
package ng.exelon.frankinator2_0.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_rest_log")
public class RestRequestLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@SequenceGenerator(name="rest_request_id_gen",  sequenceName="rest_request_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="rest_request_id_gen")
	private long id;
	
	@Column(length = 40,name="ip_address")
	private String ipAddress;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "request_time")
	private Date requestTime;
	
	/**
	 * 
	 */
	public RestRequestLog() {
	}


	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}


	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Date getRequestTime() {
		return requestTime;
	}


	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}


}
