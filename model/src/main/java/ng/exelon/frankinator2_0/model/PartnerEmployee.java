/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_partner_employee")
@NamedQueries( {
	@NamedQuery(name = "PartnerEmployee.findAll", 	
		query = "select c from PartnerEmployee c order by c.id "),
	@NamedQuery(name = "PEmp.findByMobileNumber", 	
	query = "select c from Trainer c where c.mobileNumber = :phone order by c.id "),
	@NamedQuery(name = "PEmp.findByEmail", 	
		query = "select c from Trainer c where lower(c.email) = :email order by c.id ")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerEmployee extends AbstractPerson {
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="partemp_id_gen",  sequenceName="partemp_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="partemp_id_gen")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="partner", nullable=false)
	private Partner partner;

	public PartnerEmployee() {
	}
	
	public PartnerEmployee(String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry, Partner partner) {
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		this.partner = partner;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	
	
	

}
