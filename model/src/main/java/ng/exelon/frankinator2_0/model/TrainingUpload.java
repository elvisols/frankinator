/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_training_upload")
@NamedQueries( {
	
	@NamedQuery(name = "TrainingUpload.findAll", 	
		query = "select c from TrainingUpload c order by c.id "),
	@NamedQuery(name = "TrainingUpload.findByTrainer", 	
	query = "select c from TrainingUpload c where c.trainer = :trainer order by c.id ")
	
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingUpload implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="upload_id_gen",  sequenceName="upload_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="upload_id_gen")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="trainer", nullable=false)
	private Trainer trainer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="partner", nullable=false)
	private Partner partner;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "training")
	private List<Trainee> trainees;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "training")
	private List<TrainingMedia> media;
	
	// summary of the training
	@Column(name="attendee")
	private Integer actualAttendees;

	public TrainingUpload() {
		// TODO Auto-generated constructor stub
	}

	public TrainingUpload(Trainer trainer, Partner partner, List<Trainee> trainees, List<TrainingMedia> media,
			Integer actualAttendees) {
		this.trainer = trainer;
		this.partner = partner;
		this.trainees = trainees;
		this.media = media;
		this.actualAttendees = actualAttendees;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public List<Trainee> getTrainees() {
		return trainees;
	}

	public void setTrainees(List<Trainee> trainees) {
		this.trainees = trainees;
	}

	public List<TrainingMedia> getMedia() {
		return media;
	}

	public void setMedia(List<TrainingMedia> media) {
		this.media = media;
	}

	public Integer getActualAttendees() {
		return actualAttendees;
	}

	public void setActualAttendees(Integer actualAttendees) {
		this.actualAttendees = actualAttendees;
	}
	
	
	
}
