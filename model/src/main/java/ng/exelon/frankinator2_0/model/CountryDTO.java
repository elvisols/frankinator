/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CountryDTO {
	
	private String code;
	
	private String name;
	
	public CountryDTO() { }
	
	public CountryDTO(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static CountryDTOBuilder newBuilder() {
		return new CountryDTOBuilder();
	}
	
	public static class CountryDTOBuilder {
		
		private String code;
		private String name;
		
		public CountryDTOBuilder setCode(String code) {
			this.code = code;
			return this;
		}
		public CountryDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		public CountryDTO createCountryDTO() {
			return new CountryDTO(code, name);
		}
		
	}
	

}
