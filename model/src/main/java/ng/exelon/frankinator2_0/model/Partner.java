/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name="ds_partner")
@NamedQueries( {
	@NamedQuery(name = "Partner.findAll", query = "select c from Partner c order by c.name")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Partner extends CorporateUser {
	
	@SequenceGenerator(name="partner_id_gen",  sequenceName="partner_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="partner_id_gen")
	private Long id;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "partner")
	private List<Trainer> trainers;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "partner")
	private List<PartnerEmployee> employees;
	
	public Partner() {	
		super();
	}
	
	public Partner(String name, String contactPhone, String rcNumber, Date incDate, String mobileNumber, String email, String webSite, Address address, String gis, String shortName, String description, Date dateAdded, List<Trainer> trainers, List<PartnerEmployee> employees) {	
		super(name, contactPhone, rcNumber, incDate, mobileNumber, email, webSite, address, gis, shortName, description, dateAdded);
		this.trainers = trainers;
		this.employees = employees;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Trainer> getTrainers() {
		return trainers;
	}

	public void setTrainers(List<Trainer> trainers) {
		this.trainers = trainers;
	}

	public List<PartnerEmployee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<PartnerEmployee> employees) {
		this.employees = employees;
	}

}
