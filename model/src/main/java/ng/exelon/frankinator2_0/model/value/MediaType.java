package ng.exelon.frankinator2_0.model.value;

public enum MediaType {

	IMAGE,
	AUDIO,
	VIDEO
	
}
