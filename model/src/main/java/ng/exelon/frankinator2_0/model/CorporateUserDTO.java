/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ng.exelon.frankinator2_0.model.AbstractPersonDTO.AbstractPersonDTOBuilder;

/**
 * @author ukaegbu
 *
 */
public abstract class CorporateUserDTO extends AbstractEmbeddable {

	protected String name;
	
	protected String contactPhone;
	
	protected String rcNumber;
	
	protected Date incDate;
	
	protected String mobileNumber;
	
	protected String email;
	
	protected Address address;
	
	protected String webSite;
	
	protected String gis;
	
	protected String shortName; 
	
	protected String description; 

	protected Date dateAdded;

	public CorporateUserDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public CorporateUserDTO(String name, String contactPhone, String rcNumber, Date incDate, String mobileNumber, String email, String webSite, Address address, String gis, String shortName, String description, Date dateAdded) {
		this.name = name;
		this.contactPhone = contactPhone;
		this.rcNumber = rcNumber;
		this.incDate = incDate;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.webSite = webSite;
		this.address = address;
		this.gis = gis;
		this.shortName = shortName;
		this.description = description;
		this.dateAdded = dateAdded;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the contactPhone
	 */
	public String getContactPhone() {
		return contactPhone;
	}

	/**
	 * @return the rcNumber
	 */
	public String getRcNumber() {
		return rcNumber;
	}

	/**
	 * @return the incDate
	 */
	public Date getIncDate() {
		return incDate;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the webSite
	 */
	public String getWebSite() {
		return webSite;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @return the gis
	 */
	public String getGis() {
		return gis;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the dateAdded
	 */
	public Date getDateAdded() {
		return dateAdded;
	}

	public static CorporateUserDTOBuilder newCorporateUserDTOBuilder() {
		return new CorporateUserDTOBuilder();
	}
	
	@SuppressWarnings("unchecked")
	public static class CorporateUserDTOBuilder {
		protected String name;
		protected String contactPhone;
		protected String rcNumber;
		protected Date incDate;
		protected String mobileNumber;
		protected String email;
		protected Address address;
		protected String webSite;
		protected String gis;
		protected String shortName; 
		protected String description; 
		protected Date dateAdded;
		
		public <T extends CorporateUserDTOBuilder> T setName(String name) {
			this.name = name;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setContactPhone(String contactPhone) {
			this.contactPhone = contactPhone;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setRcNumber(String rcNumber) {
			this.rcNumber = rcNumber;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setIncDate(Date incDate) {
			this.incDate = incDate;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setEmail(String email) {
			this.email = email;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setAddress(Address address) {
			this.address = address;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setWebSite(String webSite) {
			this.webSite = webSite;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setGis(String gis) {
			this.gis = gis;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setShortName(String shortName) {
			this.shortName = shortName;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setDescription(String description) {
			this.description = description;
			return (T) this;
		}
		public <T extends CorporateUserDTOBuilder> T setDateAdded(Date dateAdded) {
			this.dateAdded = dateAdded;
			return (T) this;
		}
		
	}

}
