/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class KpiCategoryDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String name; 
	
	private String description;

	private List<Kpi> kpis;
	
	public KpiCategoryDTO() {	}

	public KpiCategoryDTO(String id, String name, String description, List<Kpi> kpis) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.kpis = kpis;
	}
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public List<Kpi> getKpis() {
		return kpis;
	}

	public void addKpis(Kpi kpi) {
		this.kpis.add(kpi);
	}
	
	public static KpiCategoryDTOBuilder newBuilder() {
		return new KpiCategoryDTOBuilder();
	}
	
	public static class KpiCategoryDTOBuilder {
		private String id;
		private String name; 
		private String description;
		private List<Kpi> kpis;
		
		public KpiCategoryDTOBuilder setId(String id) {
			this.id = id;
			return this;
		}
		public KpiCategoryDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		public KpiCategoryDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		public KpiCategoryDTOBuilder setKpis(List<Kpi> kpis) {
			this.kpis = kpis;
			return this;
		}
		public KpiCategoryDTO createKpiCategoryDTO() {
			return new KpiCategoryDTO(id, name, description, kpis);
		}
		
	}
	
	
	

}
