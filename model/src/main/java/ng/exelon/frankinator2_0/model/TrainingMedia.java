/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MediaType;


/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_training_media")
@NamedQueries( {
	
	@NamedQuery(name = "TrainingMedia.findAll", 	
		query = "select c from TrainingMedia c order by c.id "),
	@NamedQuery(name = "TrainingMedia.findByTrainer", 	
	query = "select c from TrainingMedia c where c.training = :training order by c.id ")
	
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingMedia implements Serializable {
	
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="media_id_gen",  sequenceName="media_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="media_id_gen")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="training", nullable=false)
	private TrainingUpload training;

	
	@Enumerated(EnumType.STRING)
	@Column(name="media_type", nullable = false, length = 5)
	private MediaType mediaType;
	
	@Column(length=70, nullable=false)
	private String filename;
	
	@Temporal(TemporalType.DATE)
	@Column(name="upload_date", nullable = false)
	private Date uploadDate;
	
	@Temporal(TemporalType.TIME)
	@Column(name="upload_time", nullable = false)
	private Date uploadTime;
	
	public TrainingMedia() { }
	
	public TrainingMedia(TrainingUpload training, MediaType mediaType, String filename, Date uploadDate, Date uploadTime) {
		this.training = training;
		this.mediaType = mediaType;
		this.filename = filename;
		this.uploadDate = uploadDate;
		this.uploadTime = uploadTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TrainingUpload getTraining() {
		return training;
	}

	public void setTraining(TrainingUpload training) {
		this.training = training;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public Date getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	} 
	
	

}
