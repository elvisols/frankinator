/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.KpiInputType;


/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KpiDTO {
	
	private Long id;
	
	private String name; 
	
	private String description;
	
	private KpiInputType inputType;
	
	private KpiCategory category;

	public KpiDTO() {  }
	
	public KpiDTO(Long id, String name, String description, KpiInputType inputType, KpiCategory category) {
		this.name = name;
		this.description = description;
		this.inputType = inputType;
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}


	public String getDescription() {
		return description;
	}

	public KpiCategory getCategory() {
		return category;
	}

	public KpiInputType getInputType() {
		return inputType;
	}

	public static KpiDTOBuilder newBuilder() {
		return new KpiDTOBuilder();
	}

	public static class KpiDTOBuilder {
		private Long id;
		private String name; 
		private String description;
		private KpiInputType inputType;
		private KpiCategory category;
		
		public KpiDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public KpiDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		public KpiDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		public KpiDTOBuilder setInputType(KpiInputType inputType) {
			this.inputType = inputType;
			return this;
		}
		public KpiDTOBuilder setCategory(KpiCategory category) {
			this.category = category;
			return this;
		}
		public KpiDTO createKpiDTO() {
			return new KpiDTO(id, name, description, inputType, category);
		}
	}

}
