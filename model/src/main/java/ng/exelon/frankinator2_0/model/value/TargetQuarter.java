package ng.exelon.frankinator2_0.model.value;

public enum TargetQuarter {

	Q1("First Quarter"),
	Q2("Second Quarter"),
	Q3("Third Quarter"),
	Q4("Fourth Quarter");
	
	private final String type;
	
	TargetQuarter(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return this.type;
	}
	
}
