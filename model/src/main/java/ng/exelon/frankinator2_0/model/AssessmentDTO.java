/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * @author ukaegbu
 *
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssessmentDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private Assessor assessor;

	private Trainer trainer;

	private Training training;

	private User createdBy;
	
//	private Integer kpi;
//	
//	private Float kpiValue;
	
	private List<AssessmentKpi> kpis;
	
	public AssessmentDTO() { }
	
	public AssessmentDTO(Long id, Assessor assessor, Trainer trainer, Training training, User createdBy, List<AssessmentKpi> kpis) { 
		this.id = id;
		this.assessor = assessor;
		this.trainer = trainer;
		this.training = training;
		this.createdBy = createdBy;
//		this.kpi = kpi;
//		this.kpiValue = kpiValue;
		this.kpis = kpis;
	}
	
	public Long getId() {
		return id;
	}

	public Assessor getAssessor() {
		return assessor;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public Training getTraining() {
		return training;
	}

	public User getCreatedBy() {
		return createdBy;
	}

//	public Integer getKpi() {
//		return kpi;
//	}
//
//	public Float getKpiValue() {
//		return kpiValue;
//	}

	public List<AssessmentKpi> getKpis() {
		return kpis;
	}
	
	public static AssessmentDTOBuilder newBuilder() {
		return new AssessmentDTOBuilder();
	}

	public static class AssessmentDTOBuilder {
		private Long id;
		private Assessor assessor;
		private Trainer trainer;
		private Training training;
		private User createdBy;
//		private Integer kpi;
//		private Float kpiValue;
		private List<AssessmentKpi> kpis;
		
		public AssessmentDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}

		public AssessmentDTOBuilder setAssessor(Assessor assessor) {
			this.assessor = assessor;
			return this;
		}

		public AssessmentDTOBuilder setTrainer(Trainer trainer) {
			this.trainer = trainer;
			return this;
		}

		public AssessmentDTOBuilder setTraining(Training training) {
			this.training = training;
			return this;
		}

		public AssessmentDTOBuilder setCreatedBy(User createdBy) {
			this.createdBy = createdBy;
			return this;
		}

//		public AssessmentDTOBuilder setKpi(Integer kpi) {
//			this.kpi = kpi;
//			return this;
//		}
//
//		public AssessmentDTOBuilder setKpiValue(Float kpiValue) {
//			this.kpiValue = kpiValue;
//			return this;
//		}

		public AssessmentDTOBuilder setKpis(List<AssessmentKpi> kpis) {
			this.kpis = kpis;
			return this;
		}
		
		public AssessmentDTO createAssessmentDTO() {
			return new AssessmentDTO(this.id,this.assessor, this.trainer, this.training, this.createdBy, this.kpis);
		}

		@Override
		public String toString() {
			return "AssessmentDTOBuilder [id=" + id + ", assessor=" + assessor + ", trainer=" + trainer + ", training="
					+ training + ", createdBy=" + createdBy + ", kpis=" + kpis + "]";
		}

	}

}
