package ng.exelon.frankinator2_0.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import ng.exelon.frankinator2_0.model.value.RoleEnum;
import ng.exelon.frankinator2_0.model.value.Sex;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO implements Serializable {

//	@JsonIgnore
	private Long id;

	@JsonIgnore
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime createdAt = ZonedDateTime.now();

	private Boolean enabled;

	@NotNull(message = "Email can not be null!")
	private String email;

	@NotNull(message = "First name can not be null!")
	private String firstName;

	private Date lastLogDate;

//	@Column(name="last_log_time")
	private Time lastLogTime;

//	@Column(name="last_name", length=30)
	private String lastName;

//	@Column(name="logged_in_ip", length=30)
	private String loggedInIp;

	@NotNull(message = "Mobile number can not be null!")
	private String mobileNumber;
	
	@Enumerated(EnumType.STRING)
    private RoleEnum role;

	@NotNull(message = "Password must not be null")
//	@JsonIgnore
	private String password;
	
	@Temporal(TemporalType.DATE)
	private Date profileStartDate;
	
	@Temporal(TemporalType.DATE)
	private Date profileEndDate;
	
	@Enumerated(EnumType.STRING)
	private Sex sex;

	@JsonIgnore
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime updatedAt = ZonedDateTime.now();
//	private Timestamp updatedAt;
	
//	@JsonIgnore
	private List<Map<String, Object>> roles;

	public UserDTO() {
	}
	
	public UserDTO(Long id, String email, String password, Boolean enabled, String firstname, String lastname, String mobileNumber, List<Map<String, Object>> roles) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.enabled = enabled;
		this.firstName = firstname;
		this.lastName = lastname;
		this.mobileNumber = mobileNumber;
		this.roles = roles;
	}

//	@JsonProperty
	public Long getId() {
		return this.id;
	}

	public ZonedDateTime getCreatedAt() {
		return this.createdAt;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public Date getLastLogDate() {
		return this.lastLogDate;
	}

	public Time getLastLogTime() {
		return this.lastLogTime;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getLoggedInIp() {
		return this.loggedInIp;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public String getPassword() {
		return this.password;
	}

	public ZonedDateTime getUpdatedAt() {
		return this.updatedAt;
	}

	public List<Map<String, Object>> getRoles() {
		return this.roles;
	}

	public String getEmail() {
		return email;
	}
	
	public RoleEnum getRole() {
		return this.role;
	}
	
	public Date getProfileStartDate() {
		return profileStartDate;
	}

	public Date getProfileEndDate() {
		return profileEndDate;
	}

	public Sex getSex() {
		return sex;
	}

	public static UserDTOBuilder newBuilder() {
		return new UserDTOBuilder();
	}
	
	public static class UserDTOBuilder {
		
		private Long id;
		private Boolean enabled;
		private String email;
		private String firstName;
		private String lastName;
		@Temporal(TemporalType.DATE)
		private Date profileStartDate;
		@Temporal(TemporalType.DATE)
		private Date profileEndDate;
		@Enumerated(EnumType.STRING)
		private Sex sex;
		private String mobileNumber;
		private String password;
		private List<Map<String, Object>> roles;
		
		public UserDTOBuilder() {
			this.roles = new ArrayList<>();
		}
		
		public UserDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}

		public UserDTOBuilder setEnabled(Boolean enabled) {
			this.enabled = enabled;
			return this;
		}

		public UserDTOBuilder setEmail(String email) {
			this.email = email;
			return this;
		}

		public UserDTOBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public UserDTOBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public UserDTOBuilder setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
			return this;
		}

		public UserDTOBuilder setPassword(String password) {
			this.password = password;
			return this;
		}
		
		public UserDTOBuilder setProfileStartDate(Date profileStartDate) {
			this.profileStartDate = profileStartDate;
			return this;
		}

		public UserDTOBuilder setProfileEndDate(Date profileEndDate) {
			this.profileEndDate = profileEndDate;
			return this;
		}

		public UserDTOBuilder setSex(Sex sex) {
			this.sex = sex;
			return this;
		}

		public UserDTOBuilder addRole(Map<String, Object> role) {
			this.roles.add(role);
			return this;
		}
		
		public UserDTO createUserDTO() {
			return new UserDTO(id, email, password, enabled, firstName, lastName, mobileNumber, roles);
		}
		
	}

	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", createdAt=" + createdAt + ", enabled="
				+ enabled + ", email=" + email + ", firstName=" + firstName
				+ ", lastLogDate=" + lastLogDate + ", lastLogTime="
				+ lastLogTime + ", lastName=" + lastName + ", loggedInIp="
				+ loggedInIp + ", mobileNumber=" + mobileNumber + ", role="
				+ role + ", password=" + password + ", updatedAt=" + updatedAt
				+ ", roles=" + roles + "]";
	}
	
}