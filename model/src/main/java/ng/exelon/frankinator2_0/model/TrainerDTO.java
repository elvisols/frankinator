/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainerDTO extends AbstractPersonDTO {

	private Long id;

	private Integer partner;

	public TrainerDTO() {
		super();
	}
	
	public TrainerDTO(Long id, String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry, Integer partner) {
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		this.partner = partner;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	public Integer getPartner() {
		return partner;
	}

	public static TrainerDTOBuilder newBuilder() {
		return new TrainerDTOBuilder();
	}
	
	public static class TrainerDTOBuilder extends AbstractPersonDTOBuilder {
		private Long id;
		private Integer partner;
		
		public TrainerDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public TrainerDTOBuilder setPartner(Integer partner) {
			this.partner = partner;
			return this;
		}
		public TrainerDTO createTrainer() {
			return new TrainerDTO(id, firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry, partner);
		}
	}
	
}
