/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author ukaegbu
 * 
 * PRV 	Private
 * PUB	Public
 *
 */
@Entity
@Table(name = "ds_ref_training_type")
@NamedQueries( {
	@NamedQuery(name = "TrainingType.findAll", 	query = "select c from TrainingTypeDeleted c  order by c.code ")
})
public class TrainingTypeDeleted implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 5, name = "code", unique = false, nullable = false)
	@Id
	private String code;
	
	@Column(length = 40, name = "name", unique = false, nullable = false)
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
