/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.MaritalStatus;
import ng.exelon.frankinator2_0.model.value.Sex;

/**
 * @author ukaegbu
 *
 */
@Entity
@Table(name = "ds_trainee")
@NamedQueries( {
	
	@NamedQuery(name = "Trainee.findAll", 	
		query = "select t from Trainee t order by t.id "),
	@NamedQuery(name = "Trainee.findByMobileNumber", 	
	query = "select c from Trainee c where c.mobileNumber = :phone order by c.id "),
	@NamedQuery(name = "Trainee.findTraineeByEmail", 	
		query = "select c from Trainee c where lower(c.email) = :email order by c.id ")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Trainee extends AbstractPerson {
	private static final Long serialVersionUID = 1L;
	
	@SequenceGenerator(name="trainee_id_gen",  sequenceName="trainee_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="trainee_id_gen")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="training", nullable=false)
	private TrainingUpload training;

	public Trainee() {
		super();
	}
	
	public Trainee(String firstName, String middleName, String lastName, Sex sex, Date dateOfBirth, MaritalStatus maritalStatus, String mobileNumber, String email, String birthCountry, String residentCountry, TrainingUpload training) {
		super(firstName, middleName, lastName, sex, dateOfBirth, maritalStatus, mobileNumber, email, birthCountry, residentCountry);
		this.training = training;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TrainingUpload getTraining() {
		return training;
	}

	public void setTraining(TrainingUpload trainer) {
		this.training = trainer;
	}

	

}
