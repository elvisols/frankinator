/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ng.exelon.frankinator2_0.model.value.TierEnum;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraineeTierDTO implements Serializable {
	private static final Long serialVersionUID = 1L;

	private String code;
	
	@NotNull(message = "TraineeTier name must not be null.")
	private String name; 
	
	@NotNull(message = "TraineeTier description must not be null.")
	private String description;
	
	public TraineeTierDTO() {
	}
	
	public TraineeTierDTO(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public static TraineeTierDTOBuilder newBuilder() {
		return new TraineeTierDTOBuilder();
	}
	
	public static class TraineeTierDTOBuilder {
		
		private String code;
		private String name; 
		private String description;
		
		public TraineeTierDTOBuilder setCode(String code) {
			this.code = code;
			return this;
		}
		
		public TraineeTierDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		
		public TraineeTierDTOBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		
		public TraineeTierDTO createTraineeTierDTO() {
			return new TraineeTierDTO(code, name, description);
		}
		
	}

}
