/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LanguageDTO {
	
	private String code;
	
	private String name;
	
	public LanguageDTO() { }
	
	public LanguageDTO(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static LanguageDTOBuilder newBuilder() {
		return new LanguageDTOBuilder();
	}
	
	public static class LanguageDTOBuilder {
		
		private String code;
		private String name;
		
		public LanguageDTOBuilder setCode(String code) {
			this.code = code;
			return this;
		}
		public LanguageDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		public LanguageDTO createLanguageDTO() {
			return new LanguageDTO(code, name);
		}
		
	}
	

}
