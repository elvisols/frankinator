/**
 * 
 */
package ng.exelon.frankinator2_0.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author ukaegbu
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StateDTO implements Serializable {

	private Long id;

	private Country country;

	private String code;
	
	private String name;
	
	private String capital;

	public StateDTO() { }
	
	public StateDTO(Long id, Country country, String code, String name, String capital) {
		this.id = id;
		this.country = country;
		this.code = code;
		this.name = name;
		this.capital = capital;
	}
	
	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getCapital() {
		return capital;
	}

	public Country getCountry() {
		return country;
	}

	public static StateDTOBuilder newBuilder() {
		return new StateDTOBuilder();
	}
	
	public static class StateDTOBuilder {
		private Long id;
		private Country country;
		private String code;
		private String name;
		private String capital;
		
		public StateDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public StateDTOBuilder setCountry(Country country) {
			this.country = country;
			return this;
		}
		public StateDTOBuilder setCode(String code) {
			this.code = code;
			return this;
		}
		public StateDTOBuilder setName(String name) {
			this.name = name;
			return this;
		}
		public StateDTOBuilder setCapital(String capital) {
			this.capital = capital;
			return this;
		}
		public StateDTO createStateDTO() {
			return new StateDTO(id, country, code, name, capital);
		}
	}
	
}
